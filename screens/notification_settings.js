import React, {useEffect, useState} from 'react';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '../AsyncStorageCustom'

import { View, Image, Dimensions, StyleSheet, ScrollView, TouchableOpacity, SafeAreaView } from 'react-native'
import { Layout, Text, Card, Icon, Button, Toggle, ListItem } from '@ui-kitten/components';
import { ActivityIndicator, useTheme } from 'react-native-paper';
import { PreferencesContext } from '../context';
import { colors } from '../utils/constants';

const ForwardIcon = (props) => (
    <Icon {...props} name='arrow-ios-forward-outline' fill={'grey'}/>
);

const GiftIcon = (props) => (
    <Icon {...props} name='gift-outline' style={{width: 32, height: 32}}/>
);

const BackIcon = (props) => (
  <Icon {...props} name='chevron-left-outline'/>
);

const NotificationSettings = ({route, navigation}) =>{

    const [user, setUser] = useState()

    useEffect(()=>{

      (async()=>{
        let account = await AsyncStorage.getItem('user')
        setUser(JSON.parse(account))
      })()

    }, [])

    return (
        <Layout style={{ flex: 1}}>
            <View style={{backgroundColor: colors.primary, width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/3, position: 'absolute', top: 0 }}/>
            
            <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='control' accessoryLeft={BackIcon}/>
                <Text category={'h4'} style={{color: 'white', margin: 15}}>Nofication Settings</Text>
                </View>

                { user ?
                <View shouldRasterizeIOS showsVerticalScrollIndicator={false}>
                <Card disabled style={{margin: 15}}>
                    
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <View>
                            <Text category={'s1'}>Incoming messages</Text>
                            <Text category={'s2'} appearance={'hint'}>When doctors message you.</Text>
                        </View>

                        <View>
                            <Toggle checked={true} onChange={()=>{}}>
                            </Toggle>
                        </View>
                    </View>

                    <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 25, justifyContent: 'space-between'}}>
                        <View>
                            <Text category={'s1'}>Booking Response</Text>
                            <Text category={'s2'} appearance={'hint'}>When doctors respond to your {'\n'}booking requests.</Text>
                        </View>

                        <View>
                            <Toggle checked={true} onChange={()=>{}}>
                            </Toggle>
                        </View>

                    </View>

                    <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 25, justifyContent: 'space-between'}}>
                        <View>
                            <Text category={'s1'}>Pharmarcy</Text>
                            <Text category={'s2'} appearance={'hint'}>When your pharmacy order {'\n'}is confirmed.</Text>
                        </View>

                        <View>
                            <Toggle checked={true} onChange={()=>{}}>
                            </Toggle>
                        </View>

                    </View>

                    <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 25, justifyContent: 'space-between'}}>
                        <View>
                            <Text category={'s1'}>Reminders</Text>
                            <Text category={'s2'} appearance={'hint'}>When your consultations are{'\n'}due or near.</Text>
                        </View>

                        <View>
                            <Toggle checked={true} onChange={()=>{}}>
                            </Toggle>
                        </View>

                    </View>
                    
                </Card>
                </View>
                :
                <ActivityIndicator size={'large'}/> }
            </SafeAreaView>
        </Layout>
    );
}

export default NotificationSettings

const style = StyleSheet.create({
  container: {
    // maxHeight: 192,
  },
});