import React, {useEffect, useState} from 'react';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '../AsyncStorageCustom'

import { View, Image, Dimensions, StyleSheet, ScrollView, TouchableOpacity, SafeAreaView } from 'react-native'
import { Layout, Text, Icon, Button, Input, Card, Spinner, Modal } from '@ui-kitten/components';
// import * as ImagePicker from 'expo-image-picker';
import { colors } from '../utils/constants';
import { update, updatePassword } from '../api/auth';

const BackIcon = (props) => (
  <Icon {...props} name='arrow-ios-back-outline'/>
);

const UserIcon = (props) => (
  <Icon {...props} name='lock-outline'/>
);

const PhoneIcon = (props) => (
  <Icon {...props} name='phone-outline'/>
);

const EmailIcon = (props) => (
  <Icon {...props} name='email-outline'/>
);

const GiftIcon = (props) => (
    <Icon {...props} name='gift-outline'/>
);

const Password = ({route, navigation}) =>{

  const [user, setUser] = useState()
  const [error, seterror] = useState()
  const [password, setpassword] = useState('')
  const [newpassword, setnewpassword] = useState('')
  const [confirmpassword, setconfirmpassword] = useState('')
  const [loading, setloading] = useState(false)
  const [response, setresponse] = useState({
    error: false,
    message: null
  })
  const [account, setaccount] = useState({
    firstname: '', 
    lastname: '', 
    age: '', 
    email: '', 
    avatar: '', 
    address: '', 
    gender: '',
    phoneNumber: ''
  })

  const savePassword = async ()=>{
    // get old password
    var oldpas = await AsyncStorage.getItem('password');
    if(oldpas == null){

    }else{
      if(password != oldpas){
        return setresponse({
          error: true,
          message: 'Old Password doesn\'t match'
        })
      }
    }

    if(password.length == 0){
      return setresponse({
        error: true,
        message: 'Please enter your current password'
      })
    }
    

    if(newpassword.trim().length == 0){
      return setresponse({
        error: true,
        message: 'Please enter your new password'
      })
    }

    if(confirmpassword.trim().length == 0){
      return setresponse({
        error: true,
        message: 'Please confirm your new password'
      })
    }

    if(newpassword == confirmpassword){
      try{
        setloading(true)
        let data = await updatePassword({password: newpassword})

        setloading(false)

        if(data.success){
          await AsyncStorage.setItem('user', JSON.stringify({...account, ...{ phone: account.phoneNumber }}));
          setresponse({
            error: false,
            message: 'Your password was successfully updated!'
          })

          await AsyncStorage.setItem('password', newpassword)
          // console.log(await AsyncStorage.getAllKeys());
        }else{
          setresponse({
            error: true,
            message: data.message
          })
        }

      }catch(e){
        console.log(e)
        setloading(false)
        setresponse({
          error: true,
          message: e.response.data.message
        })
      }
    }else{
      setresponse({
        error: true,
        message: 'Your passwords do not match'
      })
    }
  }

  useEffect(()=>{

    (async()=>{
      let account = await AsyncStorage.getItem('user')
      setUser(JSON.parse(account))
      setaccount({
        ...JSON.parse(account),
        ...{
          gender: JSON.parse(account).sex,
          phoneNumber: JSON.parse(account).phone
        }
      })
    })()

  }, [])

  return (
      <Layout style={{ flex: 1, justifyContent: 'center'}}>

        {/* Modal for response */}
        <Modal
          visible={response.message}
          backdropStyle={{
            backgroundColor: 'rgba(0, 0, 0, 0.3)'
          }}>
          <Card disabled status={response.error ? 'danger' : 'primary'} style={{width: Dimensions.get('screen').width-60}}>
            <Text category="h6" style={{textAlign: 'center', margin: 15}} status={response.error ? 'danger' : 'primary'}>{response.error ? 'Error!' : 'Saved!'}</Text>
            <Text style={{textAlign: 'center'}}>{response.message}</Text>
            <Button onPress={()=>{setresponse({error: false, message: null})}} appearance={'outline'}  status={response.error ? 'basic' : 'primary'} style={{marginTop: 15}}>{response.error ? 'Try Again' : 'Alright'}</Button>
          </Card>
        </Modal>

          <View style={{backgroundColor: colors.primary, width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/3, position: 'absolute', top: 0 }}/>
          
          <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, top: 0, position: 'absolute', left: 0, right: 0 }}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='control' accessoryLeft={BackIcon}/>
              <Text category={'h4'} status={'control'} style={{margin: 15}}>Change Password</Text>
            </View>

            { user ?
            <ScrollView shouldRasterizeIOS showsVerticalScrollIndicator={false}>
                <Card disabled style={{margin: 15}}>
                  <Input value={password} onChangeText={setpassword}
                    size={'large'}
                    accessoryLeft={UserIcon}
                    keyboardType={'default'}
                    autoCompleteType={'password'}
                    placeholder={'Current Password'}/>

                  <Input value={newpassword} onChangeText={setnewpassword}
                    size={'large'}
                    style={{
                      marginVertical: 15
                    }}
                    accessoryLeft={UserIcon}
                    keyboardType={'default'}
                    autoCompleteType={'password'}
                    placeholder={'New Password'}/>

                  <Input value={confirmpassword} onChangeText={setconfirmpassword}
                    size={'large'}
                    accessoryLeft={UserIcon}
                    keyboardType={'default'}
                    autoCompleteType={'password'}
                    placeholder={'Confirm New Password'}/>

                  <Button
                    disabled={loading}
                    onPress={savePassword}
                    style={{
                      marginTop: 15
                    }}>
                      { loading ? 'Saving...' : 'Save Changes' }
                  </Button>
                  
                </Card>                
            </ScrollView>
            :
            <Spinner size={'large'}/> }
        </SafeAreaView>
      </Layout>
  );
}

export default Password

const style = StyleSheet.create({
  container: {
    // maxHeight: 192,
  },
});