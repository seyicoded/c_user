import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  PermissionsAndroid,
  StatusBar,
  StyleSheet,
  ImageBackground,
  View,
  Image,
  useColorScheme,
  Platform,
} from 'react-native';
import RtcEngine, {
  ChannelProfile,
  ClientRole,
  RtcEngineContext,
} from 'react-native-agora';
import { Layout, Text, Icon, Button } from '@ui-kitten/components';
import firebase from 'firebase';
import axios from 'axios';

const CancelIcon = (props) => (
  <Icon {...props} name='close-outline'/>
);

const SpeakerIcon = (props) => (
  <Icon {...props} name='volume-up-outline'/>
);

const HeadsetIcon = (props) => (
  <Icon {...props} name='volume-up-outline'/>
);

const MuteIcon = (props) => (
  <Icon {...props} name='mic-off-outline'/>
);

const MutedIcon = (props) => (
  <Icon {...props} name='mic-outline'/>
);

let _engine

const Call = ({navigation, route}) => {

  const isDarkMode = useColorScheme() === 'dark';
  const [joined, setjoined] = useState(false)
  const [picked, setpicked] = useState(false)
  const [muted, setmuted] = useState(false)
  const [speaker, setspeaker] = useState(false)
  const [duration, setduration] = useState('0:00')

  const backgroundStyle = {
    backgroundColor: isDarkMode ? '#141414' : '#FFFFFF',
  };

  const _init = async (channel, token) => {
    _engine = await RtcEngine.createWithContext(
      new RtcEngineContext('0742c8affa02429b9622956bac0d67d0')
    )

    _addListeners()

    if(Platform.OS == 'android'){
      requestCameraAndAudioPermission()
    }

    await _engine.enableAudio()
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting)
    await _engine.setClientRole(ClientRole.Audience)
    try{
      console.info('joining')
      await _engine.joinChannel(token,
      channel, null, 0)
    }catch(e){
      console.log('Init error: ', e)
    }
    try{
      await _joinChannel()
    }catch(e){
      console.log('Join error: ', e)
    }
  }

  const requestCameraAndAudioPermission = async () =>{
    try {
        const granted = await PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        ])
        if (
            granted['android.permission.RECORD_AUDIO'] === PermissionsAndroid.RESULTS.GRANTED
        ) {
            console.log('You can use the mic')
        } else {
            console.log('Permission denied')
        }
    } catch (err) {
        console.warn(err)
    }
  }

  const _addListeners = () => {
    _engine.addListener('JoinChannelSuccess', (channel, uid, elapsed) => {
      console.info('JoinChannelSuccess', channel, uid, elapsed)
      setjoined(true)
    })

    _engine.addListener('LeaveChannel', (stats) => {
      console.info('LeaveChannel', stats)
      setjoined(false)
    })

    _engine.addListener('UserJoined', (channel, uid, elapsed)=>{
      setpicked(true)
    })

    _engine.addListener('UserOffline', (channel, uid, elapsed)=>{
      _leaveChannel
    })

    _engine.addListener('Error', (e)=>{
      console.log('Main Error: ', e)
    })
  }

  const _joinChannel = async () => {
    console.log('Connection state: ', await _engine.getConnectionState())
  };

  const _onChangeRecordingVolume = (value) => {
    _engine.adjustRecordingSignalVolume(value * 400)
  }

  const _onChangePlaybackVolume = (value) => {
    _engine.adjustPlaybackSignalVolume(value * 400)
  }

  const _toggleInEarMonitoring = (isEnabled) => {
    _engine.enableInEarMonitoring(isEnabled)
  }

  const _onChangeInEarMonitoringVolume = (value) => {
    _engine.setInEarMonitoringVolume(value * 400)
  }

  const _leaveChannel = async () => {
    try{
      //await _engine.leaveChannel();
    }catch(e){
      console.log('Leave error: ', e)
    }
    navigation.goBack()
  }

  const _switchMicrophone = () => {
    _engine
      ?.enableLocalAudio(!muted)
      .then(() => {
        setmuted(!muted);
      })
      .catch((err) => {
        console.warn('enableLocalAudio', err);
      });
  };

  const _switchSpeakerphone = () => {
    _engine
      ?.setEnableSpeakerphone(!speaker)
      .then(() => {
        setspeaker(!speaker)
      })
      .catch((err) => {
        console.warn('setEnableSpeakerphone', err);
      });
  }

  useEffect(() => {
    _init(route.params.channel, route.params.token)
    return () => {
      _engine.destroy()
    }
  }, [])

  return (
    <Layout style={{ flex: 1}}>
      <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0 }}>
        <View style={{marginTop: 100, alignItems: 'center', justifyContent: 'center'}}>
          <Text category='s1' appearance='hint'>{ picked ? 'Connected' : 'Joining...' } </Text>
          <Text category='h4' style={{marginTop: 10}}>{route.params.doctor}</Text>
          <Image source={require('../assets/images/urgent_care.png')} resizeMode={'cover'} style={{height: 150, width: 150, borderRadius: 150, marginTop: 50}}/>
        </View>

        <View style={{flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', bottom: 50, position: 'absolute'}}>
          <Button onPress={_leaveChannel} style={{width: 70, height: 70, alignSelf: 'flex-end', marginEnd: 25}} appearance='outline' status='danger' accessoryLeft={CancelIcon}/>
          <Button onPress={_switchSpeakerphone} style={{width: 70, height: 70, alignSelf: 'flex-end', marginEnd: 25}} appearance={speaker ? 'filled' : 'outline'} status='basic' accessoryLeft={SpeakerIcon}/>
          <Button onPress={_switchMicrophone} style={{width: 70, height: 70, alignSelf: 'flex-end'}} appearance={muted ? 'filled' : 'outline'} status='basic' accessoryLeft={MuteIcon}/>
        </View>
      </SafeAreaView>
    </Layout>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default Call;