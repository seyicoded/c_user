import React, {useEffect, useState} from 'react';
import { View, TouchableOpacity, Platform, StyleSheet, Image, Alert, ScrollView } from 'react-native';
import { LoginManager, Profile } from "react-native-fbsdk-next";
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';  
import { styles } from '../utils/style';
import { login, register, sociallogin } from '../api/auth';
import { Layout, Input, Button, Text, RadioGroup, Radio } from '@ui-kitten/components';
import { colors } from '../utils/constants';

GoogleSignin.configure({
    scopes: ['https://www.googleapis.com/auth/user.gender.read'], // [Android] what API you want to access on behalf of the user, default is email and profile
    webClientId: '77071010064-u7nlds7si6bh93bmvs2q5dkb8er0qkaa.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
    offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
    forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
    iosClientId: '77071010064-ejb2e2j3v5s2gmj3o3vgf1ai838un9fs.apps.googleusercontent.com', // [iOS] if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    profileImageSize: 120, // [iOS] The desired height (and width) of the profile image. Defaults to 120px
});  

function Register({navigation}) {

    const [name, setname] = useState('')
    const [surname, setsurname] = useState('')
    const [address, setaddress] = useState('')
    const [phone, setphone] = useState('')
    const [age, setage] = useState('')
    const [gender, setgender] = useState(0)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState()
    const [emailError, setEmailError] = useState(false)
    const [passwordError, setPasswordError] = useState(false)
    const [loading, setLoading] = useState(false)

    const authenticate = async ()=>{

        setError()
        setEmailError(false)
        setPasswordError(false)

        if(name.length == 0){
            setError('Please provide a valid first name.')
            return
        }

        if(surname.length == 0){
            setError('Please provide a valid last name.')
            return
        }

        if(address.length == 0){
            setError('Please provide a valid physical address.')
            return
        }

        if(phone.length == 0){
            setError('Please provide a valid phone number.')
            return
        }

        if(age.length == 0){
            setError('Please provide a your age.')
            return
        }

        if(email.length < 6){
            setEmailError(true)
            setError('Please provide a valid email address.')
            return
        }

        if(password.length < 6){
            setPasswordError(true)
            setError('Password minimum length is 6 characters.')
            return
        }

        setLoading(true)

        try{
            const response = await register({ 
                firstname: name, 
                lastname: surname, 
                email, 
                password, 
                phone, 
                age, 
                avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/220px-User_icon_2.svg.png', 
                address, 
                sex: gender == 0 ? 'M' : 'F'
            })

            if(response.success){
                setLoading(false)
                navigation.replace('Main');
            }else{
                setError(response.message)
                setLoading(false)
            }
        }catch(e){
            setError(e.message)
            setLoading(false)
        }

    }

    const google = async () => {
        try {
            setLoading(true)
            await GoogleSignin.hasPlayServices();
            const user = (await GoogleSignin.signIn()).user;
            
            const response = await register({ 
                firstname: user.givenName, 
                lastname: user.familyName, 
                email: user.email, 
                password: user.id, 
                phone: 'N/A', 
                age: 'N/A', 
                avatar: user.photo, 
                address: 'N/A', 
                sex: 'N/A'
            })

            if(response.success){
                setLoading(false)
                navigation.replace('Main');
            }else{
                setError(response.message)
                setLoading(false)
            }
        } catch (error) {
            setLoading(false)
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
                Alert.alert('Unavailable', 'Your device does not have play services or play services have been disabled')
            } else {
                // some other error happened
                setError(error)
            }
        }
    };

    const facebook = async ()=>{

        setLoading(true)
        try{
        LoginManager.logInWithPermissions(["public_profile", "email"]).then(
            async function(result) {
                setLoading(false)
                if (result.isCancelled) {
                    console.log("Login cancelled");
                } else {
                    console.log(
                    "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );

                    let user = await Profile.getCurrentProfile()

                    if(user!=null){
                        try{
                            const response = await register({ 
                                firstname: user.firstName, 
                                lastname: user.lastName, 
                                email: user.email, 
                                password: user.userID, 
                                phone: 'N/A', 
                                age: 'N/A', 
                                avatar: user.imageURL, 
                                address: 'N/A', 
                                sex: 'N/A'
                            })

                            if(response.success){
                                setLoading(false)
                                navigation.replace('Main');
                            }else{
                                setError(response.message)
                                setLoading(false)
                            }
                        }catch(e){
                            setLoading(false)
                            setError(e.message)
                        }
                    }
                    
                }
            },
            function(error) {
                setLoading(true)
                setError(error)
            }
          );
        }catch(e){
            setError(e.message)
            setLoading(false)
        }

    }

    return (
        <Layout style={{ flex: 1, justifyContent: 'center' }}>

            <ScrollView>

                <Image source={require('../assets/images/logo.png')} shouldRasterizeIOS style={{width: 150, height: 100, alignSelf: 'center', marginTop: 20}} resizeMode={'contain'} /> 

                <Text category="h6" style={{ textAlign: 'center', marginHorizontal: 25, fontFamily: 'Nunito'}}>Sign Up with</Text>

                <View style={{flexDirection: 'row', marginTop: 15}}>

                    <TouchableOpacity 
                        onPress={google}
                        style={
                            [
                                styles.button, 
                                style.button,
                                {
                                    marginBottom: 15,
                                    flex: 1
                                }
                            ]
                        }>

                        <>
                            <Image source={require('../assets/images/google.png')} shouldRasterizeIOS style={{width: 25, height:25, marginEnd:15}} resizeMode={'contain'} />

                            <Text 
                                style={{color: '#141414'}}>
                                Google
                            </Text>
                        </>
                        
                    </TouchableOpacity>

                    <TouchableOpacity 
                        onPress={facebook}
                        style={
                            [
                                styles.button, 
                                style.button,
                                {
                                    backgroundColor: '#3B5998',
                                    marginBottom: 15,
                                    marginStart: 0,
                                    flex: 1
                                }
                            ]
                        }>

                        <>
                        <Image source={require('../assets/images/facebook.png')} shouldRasterizeIOS style={{width: 25, height:25, marginEnd:15}} resizeMode={'contain'} />

                        <Text 
                            style={styles.button_text}>
                            Facebook
                        </Text>
                        </>
                        
                    </TouchableOpacity>
                </View>

                <Text category="s1" style={{ textAlign: 'center', marginHorizontal: 25, fontFamily: 'Nunito'}}>Or manually...</Text>

                <Text status={'danger'} style={{ textAlign: 'center'}}>{error ? error : ''}</Text>

                <Input 
                    mode={'outlined'}
                    appearance={'default'}
                    size={'large'}
                    style={{marginHorizontal: 15}} 
                    placeholder={'First name'} 
                    label={'First name'} 
                    value={name}
                    onChangeText={text=>{
                        setEmailError(false)
                        setError()
                        setname(text)
                    }}
                    autoCapitalize={'none'} 
                    shouldRasterizeIOS
                    error={error}
                    textContentType={'givenName'} 
                    autoCompleteType={'name'}/>
                
                <Input 
                    mode={'outlined'}
                    appearance={'default'}
                    size={'large'}
                    style={{margin: 15}} 
                    placeholder={'Last name'} 
                    label={'Last name'} 
                    value={surname}
                    onChangeText={text=>{
                        setEmailError(false)
                        setError()
                        setsurname(text)
                    }}
                    autoCapitalize={'none'} 
                    shouldRasterizeIOS
                    error={error}
                    textContentType={'familyName'} 
                    autoCompleteType={'name'}/>

                <Input 
                    mode={'outlined'}
                    appearance={'default'}
                    size={'large'}
                    style={{marginHorizontal: 15}} 
                    placeholder={'Email address'} 
                    label={'Email address'} 
                    value={email}
                    onChangeText={text=>{
                        setEmailError(false)
                        setError()
                        setEmail(text)
                    }}
                    keyboardType={'email-address'} 
                    autoCapitalize={'none'} 
                    shouldRasterizeIOS
                    error={emailError}
                    textContentType={'emailAddress'} 
                    autoCompleteType={'email'}/>

                <Input 
                    mode={'outlined'}
                    appearance={'default'}
                    size={'large'}
                    style={{margin: 15}} 
                    placeholder={'Phone number'} 
                    label={'Phone number'} 
                    value={phone}
                    onChangeText={text=>{
                        setError()
                        setphone(text)
                    }}
                    keyboardType={'phone-pad'} 
                    autoCapitalize={'none'} 
                    shouldRasterizeIOS
                    error={error}
                    textContentType={'telephoneNumber'} 
                    autoCompleteType={'tel'}/>

                <Input 
                    mode={'outlined'}
                    appearance={'default'}
                    size={'large'}
                    style={{marginHorizontal: 15}} 
                    placeholder={'Physical address'} 
                    label={'Physical address'} 
                    value={address}
                    onChangeText={text=>{
                        setError()
                        setaddress(text)
                    }}
                    autoCapitalize={'none'} 
                    shouldRasterizeIOS
                    error={error}
                    textContentType={'addressCity'} 
                    autoCompleteType={'street-address'}/>

                <Input 
                    mode={'outlined'}
                    appearance={'default'}
                    size={'large'}
                    style={{marginHorizontal: 15}} 
                    placeholder={'Age'} 
                    label={'Age'} 
                    value={age}
                    onChangeText={text=>{
                        setError()
                        setage(text)
                    }}
                    autoCapitalize={'none'} 
                    shouldRasterizeIOS
                    error={error}
                    keyboardType="number-pad"
                    autoCompleteType={'off'}/>

                <Input 
                    mode={'outlined'} 
                    style={{marginHorizontal: 15, marginTop: 15}} 
                    placeholder={'Password'} 
                    label={'Password'} 
                    appearance={'default'}
                    size={'large'}
                    value={password}
                    error={passwordError}
                    onChangeText={text=>{
                        setPasswordError(false)
                        setError()
                        setPassword(text)
                    }}
                    secureTextEntry={true} keyboardType={'default'} 
                    shouldRasterizeIOS 
                    textContentType={'password'}/>

                <RadioGroup style={{margin: 15}} onChange={setgender} selectedIndex={gender}>
                    <Radio>I am a man</Radio>
                    <Radio>I am a woman</Radio>
                </RadioGroup>

                <Text status={'danger'} style={{ textAlign: 'center'}}>{error ? error : ''}</Text>

                <Button disabled={loading} onPress={authenticate} style={{marginHorizontal: 15}}>{ loading ? 'Please wait...' : 'Create Account'}</Button>
                
                <TouchableOpacity onPress={()=>navigation.replace('Login')} style={{
                    margin: 15
                    }}>

                    <Text style={{
                        color: colors.primary,
                        textAlign: 'center',
                        fontSize: 16
                    }}>
                        I already have an account
                    </Text>
                </TouchableOpacity>

            </ScrollView>

        </Layout>
    );
}

export default Register

const style = StyleSheet.create({
    button: {
        backgroundColor: '#ffffff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3,
        elevation: 3,
        flexDirection: 'row'
    }
})