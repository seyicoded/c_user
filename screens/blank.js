import React, { useState } from 'react';
import { Layout, Text, Icon, Button, Divider } from '@ui-kitten/components';
import { NativeModules, View, Image, SafeAreaView } from 'react-native';

const BackIcon = (props) => (
    <Icon {...props} name='arrow-ios-back-outline'/>
);

const Blank = ({navigation, route}) => {


  return (
    <Layout style={{flex: 1}}>
      <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0 }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
          <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} accessoryLeft={BackIcon}/>
          <Text category={'h4'} style={{margin: 15}}>Blank</Text>
        </View>
        <Divider/>
      </SafeAreaView>
    </Layout>
  );
}

export default Blank