import React, { useState } from 'react';
import * as Const from '../utils/constants'
import * as API from '../api/doctors'
import { Layout, Text, Icon, Button, IndexPath, Select, SelectItem, Divider, Input, Datepicker, RadioGroup, Radio, Modal, Card } from '@ui-kitten/components';
import { ScrollView, Dimensions, View, Image, SafeAreaView, Alert } from 'react-native';
import uuid from 'react-native-uuid';

const BackIcon = (props) => (
    <Icon {...props} name='arrow-ios-back-outline'/>
);

const who = [
  'Self',
  'Parent/Guardian',
  'Family',
  'Friend',
  'Other'
]

const payments = [
  'E-Cash',
  'Insurance'
]

const mediums = ['Chat', 'Voice Call', 'Video Call']

const OnDemand = ({navigation, route}) => {

  const [specialist, setspecialist] = useState(new IndexPath(0))
  const [medium, setmedium] = useState(0)
  const [reason, setreason] = useState('')
  const [other, setother] = useState()
  const [error, seterror] = useState()
  const [time, settime] = useState('')
  const [when, setwhen] = useState(new Date())
  const [insurance, setinsurance] = useState({
    amount: 50, 
    currency: 'GHS',
    names: '', 
    policyProvider: '', 
    policyNumber: '',
    policyOption: '', 
    serviceId: uuid.v4(), 
    service: 'On Demand Booking', 
    policyExpiryDate: new Date()
  })
  const [modal, setmodal] = useState(false)
  const [success, setsuccess] = useState(false)
  const [loading, setloading] = useState(false)
  const [provider, setprovider] = useState(new IndexPath(0))
  const [payment, setpayment] = useState(new IndexPath(0))
  const [patient, setpatient] = useState(new IndexPath(0))

  const selectwho = (p)=>{
    setpatient(p)
    if(p.row == who.length-1){
      setother('')
    }else{
      setother(null)
    }
  }

  const selectpayment = (p)=>{
    setpayment(p)

    if(p.row == payments.length-1){
      setmodal(true)
    }else{
      setmodal(false)
    }
  }

  const saveinsurance = ()=>{
    if(insurance.names.trim().length == 0){
      return seterror('name')
    }

    if(insurance.policyNumber.trim().length == 0){
      return seterror('number')
    }

    if(insurance.policyOption.trim().length == 0){
      return seterror('policy')
    }

    setmodal(false)
  }

  const submit = async ()=>{

    if(reason.trim().length == 0){
      return seterror('reason')
    }

    if(time.trim().replace(/\s/g, '').length != 5){
      return seterror('time')
    }

    if(!time.trim().includes(':')){
      return seterror('time')
    }

    if(time.split(':').length > 2){
      return seterror(time)
    }

    let date = when
    date.setHours(time.split(':')[0])
    date.setMinutes(time.split(':')[1])

    try{
      let data = {
        specialist: Const.specialists[specialist-1], 
        consult_medium: mediums[medium], 
        reason: reason, 
        consult_date: date.toISOString(), 
        payment_option: payments[payment-1], 
        if_insurance: insurance, 
        appointment_for: who[patient-1] == 'Other' ? other : who[patient-1]
      }

      setloading(true)

      let booked = await API.onDemandBooking(data)

      setloading(false)

      if(booked){
        setsuccess(true)
        setTimeout(()=>{
          setsuccess(false)
          navigation.goBack()
        }, 3000)
      }else{
        Alert.alert('Error', 'There was an error sending your booking, please try again')
      }

    }catch(e){
      setloading(false)
      alert(e.message)
    }

  }

  return (
    <Layout style={{flex: 1}}>

      <Modal 
        visible={modal}
        backdropStyle={{
          backgroundColor: 'rgba(0, 0, 0, 0.3)'
        }}>
        <Card disabled  style={{width: Dimensions.get('screen').width-30}}>
          <Text category={'h6'} style={{textAlign: 'center'}}>Insurance Details</Text>
          <Input status={error == 'name' ? 'danger' : 'basic'} value={insurance.names} onChangeText={txt=>setinsurance({...insurance, ...{names: txt}})} placeholder="Full Name" style={{marginTop: 15}}/>
          <Input status={error == 'number' ? 'danger' : 'basic'} value={insurance.policyNumber} onChangeText={txt=>setinsurance({...insurance, ...{policyNumber: txt}})} placeholder="Policy No" style={{marginTop: 15}}/>
          <Input status={error == 'policy' ? 'danger' : 'basic'} value={insurance.policyOption} onChangeText={txt=>setinsurance({...insurance, ...{policyOption: txt}})} placeholder="Policy Type" style={{marginTop: 15}}/>
          <Datepicker value={insurance.policyExpiryDate} onSelect={date=>setinsurance({...insurance, ...{policyExpiryDate: date}})} label="Policy Expiry" style={{marginVertical: 15}}/>
          <Button onPress={saveinsurance}>Proceed</Button>
          <Button appearance={'ghost'} status="basic" onPress={()=>{setpayment(new IndexPath(0)); setmodal(false)}}>Cancel</Button>
        </Card>
      </Modal>

      <Modal 
        visible={success}
        backdropStyle={{
          backgroundColor: 'rgba(0, 0, 0, 0.3)'
        }}>
        <Card disabled  style={{width: Dimensions.get('screen').width-30}}>
          <Text category={'h6'} status={'primary'} style={{textAlign: 'center', margin: 15}}>Booking Successful!</Text>
          <Text category={'s1'} appearance={'hint'} style={{textAlign: 'center', marginBottom: 15}}>We have received your request, it will be processed soon and you will be updated</Text>
        </Card>
      </Modal>

      <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
          <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} accessoryLeft={BackIcon}/>
          <Text category={'h4'} style={{margin: 15}}>On Demand Booking</Text>
        </View>
        <Divider/>
        
        <ScrollView showsVerticalScrollIndicator={false} style={{padding: 15, flex: 1}}>
          <Text category="s1" style={{marginBottom: 10}}>Choose Service</Text>
          <Select
            selectedIndex={specialist}
            value={Const.specialists[specialist-1]}
            onSelect={setspecialist}>
            {Const.specialists.map((s, i)=><SelectItem key={i} title={s}/>)}
          </Select>

          <Text category="s1" style={{marginBottom: 10, marginTop: 15}}>Who is the appointment for?</Text>
          <Select
            selectedIndex={patient}
            value={who[patient-1]}
            onSelect={selectwho}>
            {who.map((w, i)=><SelectItem key={i} title={w}/>)}
          </Select>

          { other != null ?
          <Input value={other} onChangeText={setother} placeholder={'Specify your relationship with the person'} label={'Relationship'} style={{marginTop: 15}} />
          : <></> }

          <Text category="s1" style={{marginBottom: 10, marginTop: 15}}>How will payment be made?</Text>
          <Select
            selectedIndex={payment}
            value={payments[payment-1]}
            onSelect={selectpayment}>
            {payments.map((p, i)=><SelectItem key={i} title={p}/>)}
          </Select>

          <Input value={reason} onChangeText={txt=>{setreason(txt); seterror(null)}} status={error == 'reason' ? 'danger' : 'basic'} placeholder={'Specify reason for appointment'} label={'Appointment Reason'} multiline numberOfLines={3} style={{marginTop: 15}} />
          
          <Text category="s1" style={{marginBottom: 10, marginTop: 15}}>When should the appointment be?</Text>
          <Datepicker
            date={when}
            onSelect={setwhen}
            />

          <Text category="s1" style={{marginBottom: 10, marginTop: 15}}>What time?</Text>
          <Input value={time} onChangeText={txt=>{settime(txt); seterror(null)}} status={error == 'time' ? 'danger' : 'basic'} placeholder={'10:00'} />

          <Text category="s1" style={{marginBottom: 10, marginTop: 15}}>What medium should be used?</Text>
          <RadioGroup
            selectedIndex={medium}
            onChange={setmedium}>
            {mediums.map(m=><Radio>{m}</Radio>)}
          </RadioGroup>

          <Button disabled={loading} onPress={submit} style={{marginTop: 15, marginBottom: 35}} size={'large'}>{ loading ? 'Please wait...' : 'Submit Booking' }</Button>
        
        </ScrollView>

      </SafeAreaView>
    </Layout>
  );
}

export default OnDemand