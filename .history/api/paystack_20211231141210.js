import AsyncStorage from '@react-native-async-storage/async-storage';
const axios = require('axios');

const sk_key = "sk_test_b99f2f6df4585689836cad3d63d8b1d145789934";

const getUser = async()=>{
    let account = await AsyncStorage.getItem('user')
    return JSON.parse(account)
}

export const initPayment = async(price, phone, network)=>{
    let user = await getUser()

    let response

    try{
        let res = await axios.default.post('https://api.paystack.co/charge', {
                "amount": price*100, 
                "email": `${user.email}`,
                "firstname": `${user.firstname}`,
                "lastname": `${user.lastname}`,
                "currency": "GHS",
                "plan": 'PLN_2lw4dvg8vkv2aro',
                "mobile_money": {
                    "phone" : `${phone}`,
                    "provider" : `${network.toUpperCase()}`
                }
            },
            {
                headers: {
                    'Authorization': `Bearer ${sk_key}`
                }
            }
        )
        response = res.data
    }catch(e){
        try{
            response = e.response.data
        }catch(e){
            response = null
        }
    }

    return response
}

export const verOtp = async(tnx_ref, otp)=>{
    let user = await getUser()

    let response

    try{
        let res = await axios.default.post('https://api.paystack.co/charge/submit_otp', {
                "reference": tnx_ref, 
                "otp": `${otp}`,
            },
            {
                headers: {
                    'Authorization': `Bearer ${sk_key}`
                }
            }
        )
        response = res.data
    }catch(e){
        try{
            response = e.response.data
        }catch(e){
            response = null
        }
    }

    return response
}

export const cardPayment = async(card, amount)=>{
    let user = await getUser()
    let email = user.email;

    // console.log(card)
    // console.log(amount)
    // console.log(email)

    let response

    try{
        let res = await axios.default.post('https://api.paystack.co/charge', {
                "amount": amount*100, 
                "email": `${email}`,
                "pin": `${card.pin}`,
                "card": {
                    "cvv": card.cvv,
                    "number": card.number,
                    "expiry_month": card.month,
                    "expiry_year": card.year,
                },
                "custom_fields": [
                    {
                    "value": "Payment:"+(Math.floor(Math.random() * 100) + 1)+(Math.floor(Math.random() * 100) + 1)+(Math.floor(Math.random() * 100) + 1),
                    "display_name": "Payment entry --"+(Math.floor(Math.random() * 100) + 1),
                    "variable_name": "Payment entry _for"+(Math.floor(Math.random() * 100) + 1)
                    }
                ]
            },
            {
                headers: {
                    'Authorization': `Bearer ${sk_key}`
                }
            }
        )
        response = res.data
    }catch(e){
        try{
            response = e.response.data
        }catch(e){
            response = null
        }
    }

    return response
}