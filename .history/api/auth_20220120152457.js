// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '../AsyncStorageCustom'

const axios = require('axios');

export const login = async (email, password) => {

    const key = await apiKey()

    try{
        const response = await axios({
            method: 'POST',
            url: 'https://api.clarondoc.com/login',
            data: {
                email,
                password
            },
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': key
            },
            options: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        
        await AsyncStorage.setItem('access-token', response.data.accessToken)
        await AsyncStorage.setItem('login-expiry', response.data.tokenExpiryUTC)
        await AsyncStorage.setItem('email', email)
        await AsyncStorage.setItem('password', password)
        await userDetails(email, key, response.data.accessToken)

        return response.data
    }catch(e){
        return {
            message: e.message,//'The credentials you provided are not associated with any account',
            success: false
        }
    }
    
}

export const resetpassword = async(email)=>{
    const key = await apiKey()

    try{
        const response = await axios({
            method: 'GET',
            url: 'https://api.clarondoc.com/otp/get/'+email,
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': key
            },
            options: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        
        
        return response.data
    }catch(e){
        return {
            message: e.message,//'The credentials you provided are not associated with any account',
            success: false
        }
    }
}

export const checkotp = async(reset_code)=>{
    const key = await apiKey()

    try{
        const response = await axios({
            method: 'GET',
            url: 'https://api.clarondoc.com/otp/confirm/'+reset_code,
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': key
            },
            options: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        
        
        return response.data
    }catch(e){
        return {
            message: e.message,//'The credentials you provided are not associated with any account',
            success: false
        }
    }
}

export const changePasswordd = async (data, auth) => {
    const key = await apiKey()
    // const auth = await AsyncStorage.getItem('access-token');
    const response = await axios.default.put('https://api.clarondoc.com/users/update/password',
        data,
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${auth}`,
                'x-api-key': key
            },
            options: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }
    )

    console.log(response.data)
    return response.data
}

export const sociallogin = async (email) => {

    const key = await apiKey()

    try{
        const response = await axios({
            method: 'POST',
            url: 'https://api.clarondoc.com/social/login',
            data: {
                email
            },
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': key
            },
            options: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        
        await AsyncStorage.setItem('access-token', response.data.accessToken)
        await AsyncStorage.setItem('login-expiry', response.data.tokenExpiryUTC)
        await AsyncStorage.setItem('email', email)
        await userDetails(email, key, response.data.accessToken)

        return response.data
    }catch(e){
        return {
            message: e.message,//'The credentials you provided are not associated with any account',
            success: false
        }
    }
    
}

export const update = async (data) => {
    const key = await apiKey()
    const auth = await AsyncStorage.getItem('access-token');
    const response = await axios.default.put('https://api.clarondoc.com/users/update/profile',
        data,
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${auth}`,
                'x-api-key': key
            },
            options: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }
    )

    return response.data
}

export const updatePassword = async (data) => {
    const key = await apiKey()
    const auth = await AsyncStorage.getItem('access-token');
    const response = await axios.default.put('https://api.clarondoc.com/users/update/password',
        data,
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${auth}`,
                'x-api-key': key
            },
            options: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }
    )

    console.log(response.data)
    return response.data
}

export const register = async (data) => {
    
    const key = await apiKey()

    try{
        const response = await axios({
            method: 'POST',
            url: 'https://api.clarondoc.com/register',
            data,
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': key
            },
            options: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        
        await AsyncStorage.setItem('access-token', response.data.accessToken)
        await AsyncStorage.setItem('login-expiry', response.data.tokenExpiryUTC)
        await AsyncStorage.setItem('email', data.email)
        await AsyncStorage.setItem('password', data.password)
        await userDetails(data.email, key, response.data.accessToken)

        return response.data
    }catch(e){
        return {
            message: e.message,//'The credentials you provided are not associated with any account',
            success: false
        }
    }

}

export const userDetails = async (email, key, auth) => {
    const response = await axios({
        method: 'GET',
        url: 'https://api.clarondoc.com/users/'+email,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${auth}`,
            'x-api-key': key
        },
        options: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })

    if(response.data.success){
        await AsyncStorage.setItem('user', JSON.stringify(response.data.userDetails))
        return response.data.userDetails
    }
}

export const downgrade = async () => {
    const key = await apiKey()
    const auth = await AsyncStorage.getItem('access-token');

    try{
        const response = await axios.default.put('https://api.clarondoc.com/subscriptions/downgrade', {},
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${auth}`,
                    'x-api-key': key
                },
                options: {
                    'Content-Type': 'application/json'
                }
            }
        )

        return response.data
    }catch(e){
        console.info(e.response.data)
        return null
    }
}

export const apiKey = async (data) => {
    let key = await AsyncStorage.getItem('api-key');

    if(key != null){
        return key
    }else{
        const response = await axios({
            method: 'POST',
            url: 'https://api.clarondoc.com/getAPIKey',
            data: {
                email: 'developer@clarondoc.com',
                password: 'Basket012Ball'
            },
            headers: {
                'Content-Type': 'application/json',
            }
        })

        key = response.data.apiKey

        await AsyncStorage.setItem('api-key', key)

        return key
    }
}