import AsyncStorage from '@react-native-async-storage/async-storage';
const axios = require('axios');

const getUser = async()=>{
    let account = await AsyncStorage.getItem('user')
    return JSON.parse(account)
}

export const initPayment = async(price, phone, network)=>{
    let user = await getUser()

    let response

    try{
        let res = await axios.default.post('https://api.paystack.co/charge', {
                "amount": price*100, 
                "email": `${user.email}`,
                "firstname": `${user.firstname}`,
                "lastname": `${user.lastname}`,
                "currency": "GHS",
                "plan": 'PLN_2lw4dvg8vkv2aro',
                "mobile_money": {
                    "phone" : `${phone}`,
                    "provider" : `${network.toUpperCase()}`
                }
            },
            {
                headers: {
                    'Authorization': `Bearer sk_test_b99f2f6df4585689836cad3d63d8b1d145789934`
                }
            }
        )
        response = res.data
    }catch(e){
        try{
            response = e.response.data
        }catch(e){
            response = null
        }
    }

    return response
}

export const verOtp = async(tnx_ref, otp)=>{
    let user = await getUser()

    let response

    try{
        let res = await axios.default.post('https://api.paystack.co/charge/submit_otp', {
                "reference": tnx_ref, 
                "otp": `${otp}`,
            },
            {
                headers: {
                    'Authorization': `Bearer sk_test_b99f2f6df4585689836cad3d63d8b1d145789934`
                }
            }
        )
        response = res.data
    }catch(e){
        try{
            response = e.response.data
        }catch(e){
            response = null
        }
    }

    return response
}

export const cardPayment = async(card, amount)=>{
    let user = await getUser()
    let email = user.email;

    console.log(card)
    console.log(amount)
    console.log(email)
}