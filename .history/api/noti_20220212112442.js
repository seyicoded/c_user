import React from 'react'
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '../AsyncStorageCustom'
import axios from 'axios'

export const updateFCMToken = async (fcm)=>{

    try{
        let token = await AsyncStorage.getItem('access-token');

        const key = await AsyncStorage.getItem('api-key')
        let base_url = 'https://api.clarondoc.com'
        // const auth = await AsyncStorage.getItem('access-token');

        let res = await axios.post(`${base_url}/notifications/subscribe`, {
            token: fcm
        }, {
            headers: {
                'x-api-key': key,
                'Authorization': `Bearer ${token}`
            }
        })
        console.log(res.data)
        data = res.data
    }catch(e){
        console.log(e)
        data = null
    }

    try{
        AsyncStorage.setItem('device_fcm_token', fcm)
    }catch(e){}

    return data

}