// console.log('app start -1')
import 'react-native-gesture-handler';
import {AppRegistry, Alert} from 'react-native';
import {name as appName} from './app.json';
import App from './App';
import RNCallKeep from 'react-native-callkeep';
import messaging from '@react-native-firebase/messaging';

console.log('app start 0');

// Register background handler
messaging().setBackgroundMessageHandler(async message => {
    console.log('Message handled in the background!', message);
    if(message.data.hasOwnProperty('call')){
        try{
            console.log('notification: ', message.data.call)
            let calls = await RNCallKeep.getCalls()
            if(calls.length == 0){
                RNCallKeep.displayIncomingCall(call_id, 'Urgent Care', localizedCallerName = message.data.call.name, handleType = 'generic')
            }
            return;
        }catch(e){
            console.log('display incoming call error: ', e)
        }
    }
});

console.log('app start 1');

const HeadlessCheck = ({isHeadless}) =>{
    if(isHeadless){
        return null
    }

    return <App />;
}

AppRegistry.registerComponent(appName, () => HeadlessCheck);