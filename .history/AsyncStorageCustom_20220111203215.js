import React from 'react'
import { MMKV } from 'react-native-mmkv'

const storage = new MMKV()

export const getItem = async (key)=>{
    // return 'a';
    const val = storage.getString(key)
    return ((val != undefined) ? val: null)
}

export const setItem = async (key, value)=>{
    storage.set(key, value)
    return true;
}

export const getAllKeys = async ()=>{
    return (storage.getAllKeys()).toString()
}

export const clear = async ()=>{
    storage.clearAll()
    return true
}

const removeItem = async (key)=>{
    storage.delete(key)
    return true
}
