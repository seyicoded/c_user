import React, {useEffect, useState, useRef} from 'react';
import {
  PermissionsAndroid,
  StyleSheet,
  View,
  Image,
  useColorScheme,
  Platform,
  Alert,
} from 'react-native';
import RtcEngine, {
  ChannelProfile,
  ClientRole,
  RtcEngineContext,
} from 'react-native-agora';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '../AsyncStorageCustom'
import { Layout, Text, Icon, Button } from '@ui-kitten/components';
import firebase from 'firebase';
import axios from 'axios';

const CancelIcon = (props) => (
  <Icon {...props} name='close-outline'/>
);

const SpeakerIcon = (props) => (
  <Icon {...props} name='volume-up-outline'/>
);

const HeadsetIcon = (props) => (
  <Icon {...props} name='volume-up-outline'/>
);

const MuteIcon = (props) => (
  <Icon {...props} name='mic-off-outline'/>
);

const MutedIcon = (props) => (
  <Icon {...props} name='mic-outline'/>
);

let _engine
let call_id
let urgent
let countt = 0

const UrgentCare = ({navigation, route}) => {
  
  const _engine_ref = useRef(null);
  let countt_r = useRef(0)
  countt_r.current = 0;
  const email_r = useRef('')

  // try{
  //   console.log(_joinChannel)
  // }catch(e){}

  let email = 'samuel.anakwa@claronhealth.com';

  const isDarkMode = useColorScheme() === 'dark';
  const [joined, setjoined] = useState(false)
  const [engine, setengine] = useState()
  const [picked, setpicked] = useState(false)
  const [muted, setmuted] = useState(false)
  const [speaker, setspeaker] = useState(false)
  const [duration, setduration] = useState('0:00')
  const [recp, setrecp] = useState(email)

  const backgroundStyle = {
    backgroundColor: isDarkMode ? '#141414' : '#FFFFFF',
  };

  const _init = async (channel, token) => {
    _engine = await RtcEngine.createWithContext(
      new RtcEngineContext('0742c8affa02429b9622956bac0d67d0')
    )

    setengine(_engine)
    _engine_ref.current = _engine;

    _addListeners()

    if(Platform.OS == 'android'){
      requestCameraAndAudioPermission()
    }

    await _engine.enableAudio()
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting)
    await _engine.setClientRole(ClientRole.Broadcaster)
    try{
      console.info('joining')
      await _engine.joinChannel(token,
      channel, null, 0)
    }catch(e){
      console.log('Init error: ', e)
    }
    
    try{
      await _joinChannel()
    }catch(e){
      console.log('Join error: ', e)
    }
  }

  const requestCameraAndAudioPermission = async () =>{
    try {
        const granted = await PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        ])
        if (
            granted['android.permission.RECORD_AUDIO'] === PermissionsAndroid.RESULTS.GRANTED
        ) {
            console.log('You can use the mic')
        } else {
            console.log('Permission denied')
        }
    } catch (err) {
        console.warn(err)
    }
  }

  const _addListeners = () => {
    _engine.addListener('JoinChannelSuccess', (channel, uid, elapsed) => {
      console.info('JoinChannelSuccess', channel, uid, elapsed)
      setjoined(true)

      try {
        // _engine_ref.current.enableLocalAudio(true)
        _switchMicrophone()
      } catch (error) {
        
      }
    })

    _engine.addListener('LeaveChannel', (stats) => {
      console.info('LeaveChannel', stats)
      if(picked){
        firebase.firestore().collection('calls').doc(recp).set({data: {
          duration: stats.duration,
          status: 'ended',
        }},{merge: true})
      }

      try{
        setjoined(false)
      }catch(e){}
      
      (async()=>{
        try{
          await _leaveChannel();
        }catch(e){}
      })()
      
    })

    _engine.addListener('UserJoined', (channel, uid, elapsed)=>{
      console.log('user joined')
      firebase.firestore().collection('calls').doc(recp).set({data: {
        status: 'ongoing'
      }}, {merge: true})
      setpicked(true)
    })

    _engine.addListener('UserOffline', (channel, uid, elapsed)=>{
      
      (async()=>{
        try{
          await _leaveChannel();

          await firebase.firestore().collection('calls').doc(recp).set({data: {
            status: 'ended'
          }}, {merge: true})
        }catch(e){}
      })()
    })

    _engine.addListener('Error', (e)=>{
      console.log('Main Error: ', e)
    })
  }

  const _joinChannel = async () => {
    console.log('Connection state: ', await _engine.getConnectionState())
  };

  const _onChangeRecordingVolume = (value) => {
    _engine.adjustRecordingSignalVolume(value * 400)
  }

  const _onChangePlaybackVolume = (value) => {
    _engine.adjustPlaybackSignalVolume(value * 400)
  }

  const _toggleInEarMonitoring = (isEnabled) => {
    _engine.enableInEarMonitoring(isEnabled)
  }

  const _onChangeInEarMonitoringVolume = (value) => {
    _engine.setInEarMonitoringVolume(value * 400)
  }

  const _leaveChannel = async () => {
    try{
      if(_engine !=null){
        await _engine.leaveChannel()
      }
    }catch(e){
      console.log('Leave error: ', e)
    }
    navigation.goBack()
  }

  const _switchMicrophone = () => {
    try {
      engine.enableLocalAudio(!muted)
      .then(() => {
        setmuted(!muted);
        console.log('local audio:', !muted)
      })
      .catch((err) => {
        console.warn('enableLocalAudio', err);
      });

      console.log('local audio clicked')  
    } catch (error) {
      console.log(_engine_ref.current)
    }
    
  };

  const _switchSpeakerphone = () => {
    try {
      engine.setEnableSpeakerphone(!speaker)
      .then(() => {
        setspeaker(!speaker)
        console.log('local speaker:', !speaker)
      })
      .catch((err) => {
        console.warn('setEnableSpeakerphone', err);
      });  
    } catch (error) {
      
    }
    
  }

  const startUrgent = async()=>{
    try{
      let email = await AsyncStorage.getItem('email')
      let res = await axios.get('https://api.clarondoc.com/urgent/token')
      // console.log(res.data)
      _init(res.data.RTCChannel, res.data.RTCAccessToken)
      let doc = await firebase.firestore().collection('calls').doc(email_r.current).set({data: {
        time: new Date(),
        recipient: email_r.current,
        caller: email,
        status: 'started',
        channel: res.data.RTCChannel,
        token: res.data.RTCAccessToken
      }})
      call_id = (email_r.current)

      console.log('started')

      // send require to other device if in background
      // start
      firebase.firestore().collection('calls').doc(email_r.current).get().then(snapshot=>{
        console.log('Docs: ', snapshot)
        // if(snapshot.docs.length > 0){
          
        
    })
      // end

    }catch(e){
      Alert.alert('Unable to start call', e.message)
      setTimeout(()=>{
        navigation.goBack()
      }, 3000)
    }
  }

  const start_now = async(email)=>{
    // start
    setrecp(email)
    email_r.current = email;
    
    await startUrgent();

      // Alert.ale

      firebase.firestore().collection('calls').doc(email).onSnapshot(async snapshot=>{
          // console.log('Docs: ', snapshot.docs.length)
          // if(snapshot.docs.length > 0){
            try {
              if(true){
                // urgent = snapshot.docs[0]
                urgent = snapshot.data().data;
                // let call = await AsyncStorage.removeItem('_call')
                // // console.log(urgent.data().channel)
  
                console.log('count:'+ countt_r.current)
  
                if(urgent.status == 'ongoing'){
                  setpicked(true);
                  console.log('picked')
                }
  
                // if(countt_r.current % 3 == 0 ){
                if(false){
                    // console.log('reached')
                    // console.log(urgent.status)
  
                    
                    if((urgent.status != "ended" && urgent.status != "ongoing") ){
                        
                    }else{
                      console.log('reached end call'+urgent.status)
                        try{
                          if(_engine !=null){
                            // await _engine.leaveChannel()
                          }
                        }catch(e){
                          console.log('Leave error: ', e)
                        }
                        // navigation.goBack()
                    }
                    
                    
                }
  
                countt_r.current+=1;
            }              
            } catch (error) {
              
            }
          
      }, e => {
          console.log('Firebase Error: ', e)
      })
    // stop
  }

  useEffect(() => {

    (async()=>{

      try {
        Alert.alert(
          "Stand-By Line",
          "Please Select A Line to Route call to",
          [
            {
              text: "Line For Dr. Seth",
              onPress: () => start_now('krebscycle07@yahoo.com'),
              style: "default",
            },
            {
              text: "Line For Dr. Joseph",
              onPress: () => start_now('oppongjoseph69@gmail.com'),
              style: "default",
            },
            {
              text: "Line For Dr. Samuel",
              onPress: () => start_now('samuel.anakwa@claronhealth.com'),
              style: "default",
            },
          ],
          {
            cancelable: false,
          }
        );        
      } catch (error) {
        console.log('a')
        console.log(error)
      }
      
      
    })()
    
  
    return () => {
      if(call_id != null){
        console.log('leaving call ooo'+email_r.current)
        firebase.firestore().collection('calls').doc(email_r.current).set({data: {
          status: 'ended'
        }}, {merge: true})
      }
      _engine.destroy();
      countt_r.current = 0
    }
  }, [])

  return (
    <Layout style={{ flex: 1}}>
        <View style={{marginTop: 100, alignItems: 'center', justifyContent: 'center'}}>
          <Text category='s1' appearance='hint'>{ picked ? 'Connected' : 'Calling...' } </Text>
          <Text category='h4' style={{marginTop: 10}}>Urgent Care</Text>
          <Image source={require('../assets/icon.png')} resizeMode={'contain'} style={{height: 150, width: 150, borderRadius: 150, marginTop: 50}}/>
        </View>

        <View style={{flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', bottom: 50, position: 'absolute'}}>
          <Button onPress={_switchMicrophone} style={{width: 70, height: 70, borderRadius: 70, alignSelf: 'flex-end', marginEnd: 25}} appearance={!muted ? 'filled' : 'outline'} status='basic' accessoryLeft={MuteIcon}/>
          <Button onPress={_leaveChannel} style={{width: 70, height: 70, borderRadius: 70, alignSelf: 'flex-end', marginEnd: 25}} status='danger' accessoryLeft={CancelIcon}/>
          <Button onPress={_switchSpeakerphone} style={{width: 70, height: 70, borderRadius: 70, alignSelf: 'flex-end'}} appearance={speaker ? 'filled' : 'outline'} status='basic' accessoryLeft={SpeakerIcon}/>
        </View>
    </Layout>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default UrgentCare