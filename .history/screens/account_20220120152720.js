import React, {useEffect, useState} from 'react';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '../AsyncStorageCustom'

import { View, Image, Dimensions, StyleSheet, ScrollView, TouchableOpacity, SafeAreaView, Alert, ActivityIndicator } from 'react-native'
import { Layout, Text, Icon, Button, Input, Card, Spinner, Modal } from '@ui-kitten/components';
// import * as ImagePicker from 'expo-image-picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker'
import { colors } from '../utils/constants';
import { update } from '../api/auth';
import firebase from 'firebase';

const BackIcon = (props) => (
  <Icon {...props} name='arrow-ios-back-outline'/>
);

const UserIcon = (props) => (
  <Icon {...props} name='person-outline'/>
);

const PhoneIcon = (props) => (
  <Icon {...props} name='phone-outline'/>
);

const EmailIcon = (props) => (
  <Icon {...props} name='email-outline'/>
);

const GiftIcon = (props) => (
    <Icon {...props} name='gift-outline'/>
);

const Account = ({route, navigation}) =>{

  const [user, setUser] = useState()
  const [error, seterror] = useState()
  const [loading, setloading] = useState(false)
  const [imgloading, setimgloading] = useState(false)
  const [response, setresponse] = useState({
    error: false,
    message: null
  })
  const [account, setaccount] = useState({
    firstname: '', 
    lastname: '', 
    age: '', 
    email: '', 
    avatar: '', 
    address: '', 
    gender: '',
    phoneNumber: ''
  })

  // console.log(account)

  // added methods
  const getFileName = (name, path)=> {
    if (name != null) { return name; }

    if (Platform.OS === "ios") {
        path = "~" + path.substring(path.indexOf("/Documents"));
    }
    return path.split("/").pop();
}

const getPlatformPath = ({ path, uri }) => {
    return Platform.select({
        android: { "value": uri },
        ios: { "value": uri }
    })
}

const getPlatformURI = (imagePath) => {
    let imgSource = imagePath;
    if (isNaN(imagePath)) {
        imgSource = { uri: this.state.imagePath };
        if (Platform.OS == 'android') {
            imgSource.uri = "file:///" + imgSource.uri;
        }
    }
    return imgSource
}

const uriToBlob = (uri) => {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function() {
        // return the blob
        resolve(xhr.response);
      };
      
      xhr.onerror = function() {
        // something went wrong
        reject(new Error('uriToBlob failed'));
      };
      // this helps us get a blob
      xhr.responseType = 'blob';
      xhr.open('GET', uri, true);
      
      xhr.send();
    });
  }

  const pickImage = async () => {
    // let result = await ImagePicker.launchImageLibraryAsync({
      setimgloading(true)
    
    try{
      let result = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
        allowMultiSelection: false,
    })

      if (!result.cancelled) {
        result = (result[0]);
        if(result.size/1024 > 500){
          seterror(`${attached.name} cannot be attached. Exceed the maximum upload size (500KB).`)
          return false
      }
        var path = getPlatformPath(result).value;
        var name = getFileName(result.name, path);
        var blob = await uriToBlob(result.uri);
        let type = result.type;
        let attached = await firebase.storage().ref(`new-photo/${name}`).put(blob, {contentType: type})
        let url = await firebase.storage().ref(`new-photo`).child(name).getDownloadURL()
        // console.log(url);
        // setimgloading(false)
        // return ;
        setaccount({
          ...account,
          ...{
            avatar: url
          }
        });
      }
    }catch(e){}

    setimgloading(false)
  }

  const saveAccount = async ()=>{
    if(account.firstname.trim().length == 0){
      return seterror('name')
    }

    if(account.lastname.trim().length == 0){
      return seterror('lastname')
    }

    if(account.email.trim().length == 0){
      return seterror('email')
    }

    if(account.phoneNumber.trim().length == 0){
      return seterror('phone')
    }

    if(user.firstname != account.firstname ||
      user.lastname != account.lastname ||
      user.email != account.email ||
      user.phone != account.phoneNumber ||
      true){
        try{
          setloading(true)
          let data = await update(account)

          setloading(false)

          if(data.success){
            await AsyncStorage.setItem('user', JSON.stringify({...account, ...{ phone: account.phoneNumber, avatar: account.avatar }}))
            setresponse({
              error: false,
              message: 'Your profile was successfully updated! image would be reflect on app restart'
            })
          }else{
            setresponse({
              error: true,
              message: data.message
            })
          }

        }catch(e){
          console.log(e)
          setloading(false)
          setresponse({
            error: true,
            message: e.response.data.message
          })
        }
      }
  }

  useEffect(()=>{

    (async()=>{
      let account = await AsyncStorage.getItem('user')
      setUser(JSON.parse(account))
      setaccount({
        ...JSON.parse(account),
        ...{
          gender: JSON.parse(account).sex,
          phoneNumber: JSON.parse(account).phone
        }
      })
    })()

  }, [])

  return (
      <Layout style={{ flex: 1, justifyContent: 'center'}}>

        {/* Modal for response */}
        <Modal
          visible={response.message}
          backdropStyle={{
            backgroundColor: 'rgba(0, 0, 0, 0.3)'
          }}>
          <Card disabled status={response.error ? 'danger' : 'primary'} style={{width: Dimensions.get('screen').width-60}}>
            <Text category="h6" style={{textAlign: 'center', margin: 15}} status={response.error ? 'danger' : 'primary'}>{response.error ? 'Error!' : 'Saved!'}</Text>
            <Text style={{textAlign: 'center'}}>{response.message}</Text>
            <Button onPress={()=>{setresponse({error: false, message: null})}} appearance={'outline'}  status={response.error ? 'basic' : 'primary'} style={{marginTop: 15}}>{response.error ? 'Try Again' : 'Alright'}</Button>
          </Card>
        </Modal>

          <View style={{backgroundColor: colors.primary, width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/3, position: 'absolute', top: 0 }}/>
          
          <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, top: 0, position: 'absolute', left: 0, right: 0 }}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='control' accessoryLeft={BackIcon}/>
              <Text category={'h4'} status={'control'} style={{margin: 15}}>Edit Account</Text>
            </View>

            { user ?
            <ScrollView shouldRasterizeIOS showsVerticalScrollIndicator={false}>
                <Card disabled style={{margin: 15}}>
                  <TouchableOpacity onPress={pickImage}>
                    <View style={{justifyContent: 'center', alignItems: 'center'}}>
                      {
                        (imgloading) ?
                        <ActivityIndicator />
                        :
                        <Image source={{uri: account.avatar}} style={{width: 70, height: 70, borderRadius: 70, marginBottom: 5}} resizeMode={'cover'}/>
                      }
                      
                      <Text category={'s1'} appearance={'hint'} style={{marginTop:5}}>Change Photo</Text>
                    </View>
                  </TouchableOpacity>

                  <Input value={account.firstname} onChangeText={txt=>{
                      setaccount({
                        ... account,
                        ... {
                          firstname: txt
                        }
                      })
                    }}
                    style={{marginTop: 15}}
                    size={'large'}
                    accessoryLeft={UserIcon}
                    keyboardType={'default'}
                    autoCompleteType={'name'}
                    placeholder={'First Name'}/>

                  <Input value={account.lastname} onChangeText={txt=>{
                      setaccount({
                        ... account,
                        ... {
                          lastname: txt
                        }
                      })
                    }}
                    size={'large'}
                    accessoryLeft={UserIcon}
                    keyboardType={'default'}
                    autoCompleteType={'name'}
                    placeholder={'Last Name'}
                    style={{
                      marginTop: 15
                    }}/>

                  <Input value={account.email} onChangeText={txt=>{
                      setaccount({
                        ... account,
                        ... {
                          email: txt
                        }
                      })
                    }}
                    size={'large'}
                    accessoryLeft={EmailIcon}
                    keyboardType={'email-address'}
                    autoCompleteType={'email'}
                    placeholder={'Email Address'}
                    style={{
                      marginTop: 15
                    }}/>

                  <Input value={account.phoneNumber} onChangeText={txt=>{
                      setaccount({
                        ... account,
                        ... {
                          phone: txt
                        }
                      })
                    }}
                    size={'large'}
                    accessoryLeft={PhoneIcon}
                    keyboardType={'phone-pad'}
                    autoCompleteType={'tel'}
                    placeholder={'Phone Number'}
                    style={{
                      marginTop: 15
                    }}/>

                  <Button
                    disabled={loading}
                    onPress={saveAccount}
                    style={{
                      marginTop: 15
                    }}>
                      { loading ? 'Saving...' : 'Save Changes' }
                  </Button>

                  <Button
                    onPress={()=>navigation.navigate('Password')}
                    appearance={'outline'}
                    style={{
                      marginTop: 15
                    }}>
                      Change Password
                  </Button>
                  
                </Card>                
            </ScrollView>
            :
            <Spinner size={'large'}/> }
        </SafeAreaView>
      </Layout>
  );
}

export default Account

const style = StyleSheet.create({
  container: {
    // maxHeight: 192,
  },
});