import React from 'react'
import { View, StyleSheet, ScrollView } from 'react-native'
import { Layout, Text, Button, Icon, Card, Input } from '@ui-kitten/components';
import { FlatList } from 'react-native-gesture-handler';

export default function Search({navigation}) {
    return (
        <View style={styles.container}>
            <View style={{flexDirection: 'row'}}>
                <Icon name='arrow-ios-back-outline' onPress={()=>{navigation.goBack()}} size={24} style={{backgroundColor: 'red', flex: 1}}/>
                {/* <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='basic' accessoryLeft={BackIcon}/> */}
                <Input style={{flex: 1}} autoFocus={true} placeholder="type to start searching" returnKeyType="search" />
            </View>
            <ScrollView style={styles.container}>

            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
