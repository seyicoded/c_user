import React, { useState, useEffect, useRef } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { PermissionsAndroid, SafeAreaView, Alert } from 'react-native'
import RNCallKeep from 'react-native-callkeep';
import firebase from 'firebase';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '../AsyncStorageCustom'

import useAppState from 'react-native-appstate-hook'

import messaging from '@react-native-firebase/messaging';
import { BottomNavigation, BottomNavigationTab, Icon, Layout } from '@ui-kitten/components';
import Home from './tabs/home'
import History from './tabs/history'
import Blog from './tabs/blog'
import uuid from 'react-native-uuid';
import Profile from './tabs/profile'

const { Navigator, Screen } = createBottomTabNavigator();

let urgent;

const call_options = {
    ios: {
      appName: 'ClarondocDoctors',
      imageName: 'https://imageuploads01.s3.amazonaws.com/1607082935102-icon_inv.jpg',
      includesCallsInRecents: true
    },
    android: {
      alertTitle: 'Permissions required',
      alertDescription: 'This application needs to access your phone accounts',
      cancelButton: 'Cancel',
      okButton: 'ok',
      imageName: 'phone_account_icon',
      additionalPermissions: [PermissionsAndroid.PERMISSIONS.READ_CONTACTS],
      // Required to get audio in background when using Android 11
      foregroundService: {
        channelId: 'default',
        channelName: 'Foreground service for my app',
        notificationTitle: 'Incoming urgent connect',
        notificationIcon: 'https://imageuploads01.s3.amazonaws.com/1607082935102-icon_inv.jpg',
      }, 
    }
};

RNCallKeep.setup(call_options).then(accepted=>{})
let oncall = false
let call_id

//icons
const HomeIcon = (props) => (
    <Icon {...props} name='home-outline'/>
);

const HistoryIcon = (props) => (
<Icon {...props} name='clipboard-outline'/>
);

const BlogIcon = (props) => (
<Icon {...props} name='book-open-outline'/>
);

const ProfileIcon = (props) => (
    <Icon {...props} name='person-outline'/>
);

const BottomTabBar = ({ navigation, state }) => (
  <BottomNavigation
    selectedIndex={state.index}
    onSelect={index => navigation.navigate(state.routeNames[index])}>
    <BottomNavigationTab title='Home' icon={HomeIcon}/>
    <BottomNavigationTab title='History' icon={HistoryIcon}/>
    <BottomNavigationTab title='Learn' icon={BlogIcon}/>
    <BottomNavigationTab title='Profile' icon={ProfileIcon}/>
  </BottomNavigation>
);

const TabNavigator = () => (
    <Navigator screenOptions={{headerShown: false}} tabBar={props => <Layout><SafeAreaView><BottomTabBar {...props} /></SafeAreaView></Layout>}>
        <Screen name='Home' component={Home}/>
        <Screen name='Account History' component={History}/>
        <Screen name='Blog' component={Blog}/>
        <Screen name='Profile' component={Profile}/>
    </Navigator>
);

export const UserMain = ({navigation}) =>{

    const typeOfCall = useRef(null);
    typeOfCall.current = 'normal';
    
    const listensForCalls = async ()=>{
        let email = await AsyncStorage.getItem('email')
        console.log(email)
        // old logic for urgent call... from doctor tho,
        firebase.firestore().collection('calls').where('recipient', '==', email).where('status', '==', 'started').onSnapshot(snapshot=>{
            if(snapshot.docs.length > 0){
                let doc = snapshot.docs[0]
                RNCallKeep.displayIncomingCall(call_id, 'Urgent Care', localizedCallerName = doc.data().caller, handleType = 'generic')
            }
        }, e => {
            console.log('Firebase error: ', e)
        })

        // for normal call
        firebase.firestore().collection('normal_calls').doc(email).onSnapshot(async snapshot=>{
            // console.log('Docs: ', snapshot.docs.length)
            if(snapshot.data() != null){
            // if(true){
                // urgent = snapshot.docs[0]
                console.log(snapshot.data())
                urgent = snapshot.data().data;

                // urgent = (urgent != undefined) ? urgent : {status: 'ended'};

                let call = await AsyncStorage.removeItem('_call')
                // console.log(urgent.data().channel)

                if(call == null){
                    // console.log('reached')
                    console.log(urgent.status)
                    if((urgent.status != "ended") && (urgent.status != "ongoing")){
                        await AsyncStorage.setItem('_call', urgent.channel)
                        try{
                            console.log('call reached')
                            RNCallKeep.displayIncomingCall(urgent.channel, urgent.caller, localizedCallerName = urgent.caller, handleType = 'generic', true)
                        }catch(e){
                            // console.log(e)
                        }
                    }else if(urgent.status == "ongoing"){

                    }else{
                        console.log('ending nw')
                        RNCallKeep.endAllCalls()
                    }
                    
                    
                }
            }
        }, e => {
            console.log('Firebase Error: ', e)
        })
    }

    useEffect(() => {
        call_id = uuid.v4()
        RNCallKeep.removeEventListener('answerCall');
        
        RNCallKeep.addEventListener('answerCall', ({ callUUID }) => {
            // Do your normal `Answering` actions here.
            RNCallKeep.endAllCalls();
            navigation.navigate('Video', {urgent: urgent});
        });

        RNCallKeep.addEventListener('endCall', ({ callUUID }) => {
            // Do your normal `Answering` actions here.
            oncall = false;
            // RNCallKeep.endAllCalls()
            (async()=>{
                let email = await AsyncStorage.getItem('email')
                firebase.firestore().collection('normal_calls').doc(email).set({data: {
                    status: 'ended'
                }}, {merge: true})
        
                RNCallKeep.endAllCalls();
                console.log('rnck: ended call')
            })()
        });

        RNCallKeep.addEventListener('didPerformSetMutedCallAction', (data)=>{
            console.log(data.muted)
        });

        listensForCalls()

        // listen for foreground notifications
        messaging().onMessage(async message => {

            if(message.data.hasOwnProperty('call')){
                try{
                    if(!oncall){
                        oncall = true
                        RNCallKeep.displayIncomingCall(call_id, 'Urgent Care', localizedCallerName = `${message.notification.title}`, handleType = 'generic')
                    }
                    return;
                }catch(e){
                    console.log('display incoming call error: ', e)
                    return
                }
            }

            console.log(message)

            Alert.alert(message.notification.title, message.notification.body, [
            {
                text: 'View',
                style: 'default',
                onPress: ()=>{
                    if(message.data.title.includes('New message')){
                        let sender = message.data.extraData1
                        let name = message.data.title.replace('New message from ', '');
                        navigation.navigate('Conversation', { user: sender, to: sender, name: name, doctor: {firstname: name, email: sender, availability: 'Online'}});
                        
                    }
                }
            },
            {
                text: 'Dismiss',
                style: 'cancel',
                onPress: ()=>{}
            }
            ]);
        })
        // end listen for foreground notifications

        // do something on notification open
        messaging().onNotificationOpenedApp(message=>{
            console.log('Notification opened: ', message)
        })

        return () => {
            
        }
    }, [])

    const bg_picker = ()=>{
        console.log('state changed')
        (async()=>{
            try{
                const call_d = await AsyncStorage.getItem('call_d')
                if(call_d != null){
                    const urgent = JSON.parse(call_d);
                    navigation.navigate('Video', {urgent: urgent});
                    AsyncStorage.removeItem('call_d')
                }
            }catch(e){}
            
        })()
    }

    useAppState({
        onForeground: ()=>{
            bg_picker();
        }
    })

    return (
        <TabNavigator/>
    );
}