import React, {useState, useEffect} from 'react';
import { Dimensions, Image, SafeAreaView, StyleSheet, View, ActivityIndicator } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Layout, Text, Calendar, Button, Icon, Modal, Card, Input } from '@ui-kitten/components';
import { ScrollView } from 'react-native-gesture-handler';
import { makeBooking } from '../api/doctors';
import firebase from 'firebase';

const BackIcon = (props) => (
    <Icon {...props} name='chevron-left-outline'/>
);

const VideoIcon = (props) => (
    <Icon {...props} name='video-outline'/>
);

const CallIcon = (props) => (
    <Icon {...props} name='phone-outline'/>
);

const Availability = ({navigation, route}) => {

    const [date, setDate] = useState(new Date())
    const [time, setTime] = useState('10:00')
    const [booking, setBooking] = useState(false)
    const [submitted, setSubmitted] = useState(false)
    const [reason, setReason] = useState('')
    const [error, setError] = useState()
    const [user, setUser] = useState()
    const [consultation, setConsultation] = useState('')
    const [loading, setloading] = useState(true)
    const [time_selected, settime_selected] = useState([])

    const book = async()=>{
        if(reason.length == 0){
            setError('Please provide reason for the booking')
            return
        }

        try{
            const res = await makeBooking({
                physicianId: route.params.doctor.id,
                schedule: date.toISOString().substring(0, 10) +' '+time,
                symptoms: [reason]
            })

            console.log(res)
            
            if(res.success){
                setSubmitted(true)
                setTimeout(()=>{
                    setBooking(false)
                    navigation.goBack()
                }, 5000)
            }else{
                setError(res.message)
            }

        }catch(e){
            setError(e)
        }
    }

    useEffect(()=>{
        (async()=>{
            let account = await AsyncStorage.getItem('user')
            setUser(JSON.parse(account))

            const email = (route.params.doctor.email);
                // firebase.firestore().collection('newMyAvail').doc(email).onSnapshot(snapshot=>{
                //     console.log(snapshot.data().date_entry);
            
                //     settime_selected(snapshot.data().date_entry);
                    
                //   }, error=>{
                //     console.log(error)
                //   });
        })()

        setloading(false)
    }, [])

    return (
    <Layout style={{ flex: 1 }}>
        <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0 }}>

            {
                loading ? 
                <View style={{position: 'absolute', zIndex: 99, width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator color="green" />
                </View>:<></>
            }
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='basic' accessoryLeft={BackIcon}/>
                <Text category={'h4'} style={{margin: 15}}>{route.params.doctor.firstname}'s Availability</Text>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Calendar
                    date={date}
                    min={new Date()}
                    style={{
                        marginTop: 15,
                        alignSelf: 'center'
                    }}
                    onSelect={setDate}
                />

                <View style={{margin: 15, borderRadius: 5, borderColor: '#E0FFFC', borderWidth: 1}}>
                    <View style={{padding: 15, backgroundColor: '#5AB9AF', marginBottom: 10, borderTopEndRadius: 5, borderTopStartRadius: 5}}>
                        <Text category={'h6'} status={'control'}>Availability Times</Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10}}>
                        <Button appearance={time == '10:00' ? 'filled' : 'outline'} status={time == '10:00' ? 'primary' : 'basic'} onPress={()=>setTime('10:00')} style={{flex : 1, marginEnd: 15}}>10:00</Button>
                        <Button appearance={time == '10:30' ? 'filled' : 'outline'} status={time == '10:30' ? 'primary' : 'basic'} onPress={()=>setTime('10:30')} style={{flex : 1, marginEnd: 15}}>10:30</Button>
                        <Button appearance={time == '11:00' ? 'filled' : 'outline'} status={time == '11:00' ? 'primary' : 'basic'} onPress={()=>setTime('11:00')} style={{flex : 1}}>11:00</Button>
                    </View>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10, marginTop: 15}}>
                        <Button appearance={time == '11:30' ? 'filled' : 'outline'} status={time == '11:30' ? 'primary' : 'basic'} onPress={()=>setTime('11:30')} style={{flex : 1, marginEnd: 15}}>11:30</Button>
                        <Button appearance={time == '14:00' ? 'filled' : 'outline'} status={time == '14:00' ? 'primary' : 'basic'} onPress={()=>setTime('14:00')} style={{flex : 1, marginEnd: 15}}>02:00</Button>
                        <Button appearance={time == '14:30' ? 'filled' : 'outline'} status={time == '14:30' ? 'primary' : 'basic'} onPress={()=>setTime('14:30')} style={{flex : 1 }}>02:30</Button>
                    </View>
                    
                    {/* <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10, marginTop: 15, marginBottom: 10}}>
                        <Button appearance={time == '15:00' ? 'filled' : 'outline'} status={time == '15:00' ? 'primary' : 'basic'} onPress={()=>setTime('15:00')} style={{flex : 1, marginEnd: 15}}>03:00</Button>
                        <Button appearance={time == '15:30' ? 'filled' : 'outline'} status={time == '15:30' ? 'primary' : 'basic'} onPress={()=>setTime('15:30')} style={{flex : 1, marginEnd: 15}}>03:30</Button>
                        <Button appearance={time == '16:00' ? 'filled' : 'outline'} status={time == '16:00' ? 'primary' : 'basic'} onPress={()=>setTime('16:00')} style={{flex : 1}}>04:00</Button>
                    </View> */}
                </View>

                <Button onPress={()=>{
                        setConsultation('call')
                        setBooking(true)
                    }} appearance='outline' style={{flex: 1, marginHorizontal: 15, marginTop: 5}} status='primary' accessoryLeft={CallIcon}>
                    Book for call consultation
                </Button>

                <Button onPress={()=>{
                        setConsultation('video')
                        setBooking(true)
                    }} appearance='outline' style={{flex: 1, margin: 15}} status='primary' accessoryLeft={VideoIcon}>
                    Book for video consultation
                </Button>
            </ScrollView>

            <Modal
                visible={booking}
                backdropStyle={styles.backdrop}
                onBackdropPress={() => setBooking(false)}>
                <Card disabled={true} style={{
                        width: Dimensions.get('screen').width-30
                    }}>
                        { submitted ?

                        <>
                            <Image source={{uri: route.params.doctor.avatar}} style={{width: 100, height: 100, borderRadius: 100, alignSelf: 'center'}} />
                            <Text category={'h5'} style={{textAlign: 'center', marginTop: 15}} status={'primary'}>{route.params.doctor.firstname} has received your {consultation} booking.</Text>
                            <Text appearance={'hint'} style={{textAlign: 'center', marginTop: 5}}>You will be notified when they respond.</Text>
                        </>
                        :
                        <>
                            <Text category={'h5'}>Confirm your {consultation} booking</Text>

                            <Layout level={'3'} style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 15, padding: 10, borderRadius: 5}}>
                                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                                    <Text category={'h6'}>Date</Text>
                                    <Text category={'s1'} appearance={'hint'}>{date.toString().substr(0, 11)}</Text>
                                </View>

                                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                                    <Text category={'h6'}>Time</Text>
                                    <Text category={'s1'} appearance={'hint'}>{time}</Text>
                                </View>
                            </Layout>

                            <Input value={reason} onChangeText={text=>{
                                    setError()
                                    setReason(text)
                                }}
                                style={{marginBottom: 15}} label={error} status={error ? 'danger' : 'basic'} multiline numberOfLines={3} placeholder={'Please provide reason for the consultation'} />

                            <Button onPress={book}>
                                CONFIRM BOOKING
                            </Button>

                            <Button style={{marginTop: 10}} status={'basic'} appearance={'outline'} onPress={() => setBooking(false)}>
                                DISMISS
                            </Button>
                        </>
                        }
                </Card>
            </Modal>
        </SafeAreaView>
    </Layout>
    );
}

export default Availability

const styles = StyleSheet.create({
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    }
})