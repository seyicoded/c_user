import React, { useEffect, useState } from 'react';
import { Layout, Text, Button, Icon, Input, Modal, Card, IndexPath, Select, SelectItem, Alert } from '@ui-kitten/components';
import { View, StyleSheet, FlatList, Dimensions, SafeAreaView } from 'react-native'
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '../AsyncStorageCustom'

import { fetchDoctors } from '../api/doctors';
import { Doctor } from '../utils/components';
import { colors } from '../utils/constants';
import { initPayment } from '../api/paystack';

const BackIcon = (props) => (
  <Icon {...props} name='arrow-ios-back-outline'/>
);

const phoneIcon = (props) => (
  <Icon {...props} name='smartphone-outline' fill={'grey'}/>
);

const cardIcon = (props) => (
  <Icon {...props} name='credit-card-outline' fill={'grey'}/>
);

const calendarIcon = (props) => (
  <Icon {...props} name='calendar-outline' fill={'grey'}/>
);

const clockIcon = (props) => (
  <Icon {...props} name='clock-outline'/>
);

const securityIcon = (props) => (
  <Icon {...props} name='shield-outline' fill={'grey'}/>
);

var methods = ['Mobile Money', 'Visa/Mastercard']
const mtn = ['024', '054', '055', '059']
const tgo = ['027', '057', '026', '056']
const vod = ['020', '050']

const Doctors = ({navigation}) => {

  const [modal, setmodal] = useState(false)
  const [doctors, setDoctors] = useState([])
  const [filtered, setFiltered] = useState([])
  const [step, setstep] = useState(0)
  const [temporaryaccess, settemporaryaccess] = useState(false)
  const [subscribed, setsubscribed] = useState(false)
  const [method, setmethod] = useState('Mobile Money')
  const [methodindex, setmethodindex] = useState(new IndexPath(1))
  const [phone, setphone] = useState('')
  const [phone_error, setphone_error] = useState()
  const [loading, setloading] = useState(false)
  const [sendotp, setsendotp] = useState(false)
  const [doctor, setdoctor] = useState()
  const [button, setbutton] = useState('Make Payment')
  const [card, setcard] = useState({
    number: '',
    month: '',
    year: '',
    cvv: ''
  })
  let network, reference
  const [card_error, setcard_error] = useState()

  const search = (query) => {
    setFiltered(doctors.filter(doctor=>
      doctor.firstname.toLowerCase().includes(query.toLowerCase()) ||
      doctor.lastname.toLowerCase().includes(query.toLowerCase()) ||
      doctor.department.toLowerCase().includes(query.toLowerCase()) ||
      // doctor.seniority.toLowerCase().includes(query.toLowerCase()) ||
      doctor.email.toLowerCase().includes(query.toLowerCase())
      ))
  }

  const initMomo = async()=>{

    try{
      if(phone.length != 10){
        return setphone_error('Phone number invalid')
      }

      if(mtn.includes(phone.substring(0, 3))){
        network = 'mtn'
      }else if(vod.includes(phone.substring(0, 3))){
        network = 'vod'
      }else if(tgo.includes(phone.substring(0, 3))){
        network = 'tgo'
      }

      if(network == null){
        return setphone_error('Not a local number, please correct')
      }

      setloading(true)
      setphone_error(null)
      let init = await initPayment(50, phone, network)

      if(init == null){
        setloading(false)
        return setphone_error('There was an error processing your payment request')
      }
      
      if(init.status){

        if(init.data.status == 'send_otp'){
          setloading(false)
          setsendotp(true)
          setbutton('Authorize Payment')
        }else if(init.data.status == 'pay_offline'){
          setloading(false)
          setsendotp(false)
          setbutton('Awaiting for payment confirmation...')
        }else if(init.data.status == 'success'){
          setloading(false)
          setstep(2)
          setbutton('Proceed')
          AsyncStorage.setItem('temp_access', new Date().toISOString())
          setTimeout(()=>{
            dismiss()
            navigation.navigate('Conversation', {to: doctor.email, name: doctor.firstname+' '+doctor.lastname, doctor})
          }, 5000)
        }else{
          setloading(false)
          setbutton(init.data.gateway_response)
        }

      }else{
        setloading(false)
        setbutton('Make Payment')
        try{
          setphone_error(init.data.message)
        }catch(e){
          setphone_error(e.message)
        }
        return
      }

    }catch(e){
      setloading(false)
      setphone_error(e.message)
    }

  }

  const triggermodal = (doctor)=>{
    setdoctor(doctor)
    setmodal(true)
  }

  const dismiss = ()=>{
    setmodal(false)
    setphone_error(null)
    setcard_error(null)
    setstep(0)
  }

  useEffect(()=>{
        
    (async()=>{
        let found = await fetchDoctors()
        setDoctors(found)
        setFiltered(found)
        try{
          let account = await AsyncStorage.getItem('user');
          let temp = await AsyncStorage.getItem('temp_access');

          if(temp != null){
            settemporaryaccess(true)
          }else{
            settemporaryaccess(false)
          }

          let subscription = JSON.parse(account).subscription;
          ['Family', 'Premium', 'Basic'].includes(subscription) ? setsubscribed(true) : setsubscribed(false)
        }catch(e){
          console.info(e)
        }
    })()

  }, [])

  return (
      <Layout style={{ flex: 1}}>
        <Modal 
          visible={modal}
          backdropStyle={{backgroundColor: 'rgba(0, 0, 0, 0.3)'}}
          onBackdropPress={dismiss}>

          <Card disabled style={{width: Dimensions.get('screen').width-30}}>

            { step == 0 ?
            <>
            <Text category={'h6'} style={{textAlign: 'center', marginBottom: 15}}>Pay As You GO</Text>
            <Text appearance={'hint'} style={{textAlign: 'center', marginBottom: 15}}>You are about to pay GHS 50 to consult with a doctor.</Text>
            
            <Select
                value={methods[methodindex-2]}
                size={'large'}
                style={{marginVertical: 10, flex: 1}}
                selectedIndex={methodindex}
                onSelect={setmethodindex}>
                <SelectItem disabled title='Select Payment Method'/>
                { methods.map(m=><SelectItem key={m} title={m}/>) }

            </Select>
            {methodindex-2 == 0 ?
            <View>
              <Input accessoryLeft={phoneIcon} value={phone} label={phone_error ? phone_error : ''} status={phone_error ? 'danger' : 'basic'} onChangeText={(text)=>{setphone_error(null); setphone(text)}} placeholder={'MoMo Number (e.g 0500000000)'} keyboardType={'phone-pad'} autoCompleteType={'tel'} maxLength={10} shouldRasterizeIOS returnKeyType={'done'} />
            </View> : 
            <View>
              <Input accessoryLeft={cardIcon} value={card.number} label={card_error ? card_error : ''} status={card_error ? 'danger' : 'basic'} onChangeText={(text)=>{setcard_error(null); setcard({...card, ...{number: text}})}} placeholder={'Card Number'} keyboardType={'number-pad'} autoCompleteType={'cc-number'} maxLength={16} shouldRasterizeIOS returnKeyType={'done'} />
              <View style={{flexDirection: 'row', marginVertical: 15}}>
                <Input accessoryLeft={calendarIcon} label={'Expiry Month'} value={card.month} status={card_error ? 'danger' : 'basic'} style={{flex: 1, marginEnd: 15}} onChangeText={(text)=>{setcard_error(null); setcard({...card, ...{month: text}})}} placeholder={'MM'} keyboardType={'number-pad'} autoCompleteType={'cc-exp-month'} maxLength={2} shouldRasterizeIOS returnKeyType={'done'} />
                <Input accessoryLeft={calendarIcon} label={'Expiry Year'} value={card.year} status={card_error ? 'danger' : 'basic'} style={{flex: 1}} onChangeText={(text)=>{setcard_error(null); setcard({...card, ...{year: text}})}} placeholder={'YY'} keyboardType={'number-pad'} autoCompleteType={'cc-exp-year'} maxLength={2} shouldRasterizeIOS returnKeyType={'done'} />
              </View>
              <Input accessoryLeft={securityIcon} value={card.cvv} status={card_error ? 'danger' : 'basic'} onChangeText={(text)=>{setcard_error(null); setcard({...card, ...{cvv: text}})}} placeholder={'CVV/CSC'} keyboardType={'number-pad'} autoCompleteType={'cc-csc'} maxLength={3} shouldRasterizeIOS returnKeyType={'done'} />
            </View>
            }
            <Button appearance="outline" onPress={ methodindex-2 == 0 ? initMomo : ()=>{} } style={{marginVertical: 10}}>{loading ? 'Please wait...' : button }</Button>
            <Button status={'primary'} appearance={'ghost'}>Save GHS 10 - Subscribe Now!</Button>
            </>
            : step == 1 ?
            <>
            </>
            : 
            <>
              <Text style={{textAlign: 'center', marginVertical: 15}} category="h5" status="primary">Payment Successful</Text>
              <Text style={{textAlign: 'center', marginBottom: 15}} status="primary">Your payment was successful! You have access to consultation until {new Date().toString().substring(0, 21)}.</Text>
            </>}
            { step != 2 ? <Button status="basic" onPress={dismiss} appearance={'ghost'} style={{marginTop: 10}}>Cancel</Button> : <></> }
          </Card>

        </Modal>

        <View style={[style.top, {backgroundColor: colors.primary}]}>
          <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0 }}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50}} status='control' accessoryLeft={BackIcon}/>
                <View style={{}}>
                <Text category={'h4'} style={{marginTop: 5, color: 'white'}}>Book for Consultation</Text>
              </View>
            </View>
            <Input 
              size="large"
              placeholder={'Search...'}
              placeholderTextColor={'grey'}
              onChangeText={search}
              style={{
                  marginTop: 25,
                  marginBottom: 15
              }}/>
          </SafeAreaView>
        </View>

        <FlatList
          data={filtered}
          renderItem={({item, index})=>{
            return <Doctor doctor={item}
              nav={navigation}
              alt={triggermodal}
              subscribed={subscribed || temporaryaccess}
              navigate={()=>{

                (async()=>{
                    const day = await AsyncStorage.getItem('subscription_exp_day');
                    const sub_type = await AsyncStorage.getItem('subscription');
                    if( (sub_type == 'Normal' || (sub_type == 'Normal' && day < 14))){
                        Alert.alert('Must be on a Subscription to access such features');
                        navigation.navigate('Subscription', {doctors: item});
                        return false;
                    }
                    navigation.navigate('Doctor', {doctor: item})
                })()

                
              }
              }/>
            }
          }
          keyExtractor={item=>item.email}
          showsVerticalScrollIndicator={false}
          />

        <Button size={'giant'} onPress={()=>navigation.navigate('Demand')} accessoryLeft={clockIcon} style={{borderRadius: 75, bottom: 15, right: 15, position: 'absolute'}}></Button>
      </Layout>
  );
}

export default Doctors

const style = StyleSheet.create({
  top: {
      // borderBottomLeftRadius: 15,
      // borderBottomRightRadius: 15,
      padding: 15
  }
})