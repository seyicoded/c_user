import React, { useState } from 'react';
import { Layout, Text, Icon, Button, Spinner } from '@ui-kitten/components';
import { NativeModules, View, Image, SafeAreaView, ActivityIndicator, Dimensions } from 'react-native';
import WebView from 'react-native-webview';
import {Terms, About} from '../utils/WebHolder'

const BackIcon = (props) => (
    <Icon {...props} name='arrow-ios-back-outline'/>
);

const Web = ({navigation, route}) => {

  const [loading, setloading] = useState(true);

  const url = route.params.url;
  var isArticle = false;
  try {
    const content = route.params.content;  
    if(content != undefined && content != null){
      isArticle = true;

      if(isArticle){
        // is article view
        return (
          <Layout style={{flex: 1}}>
            <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} accessoryLeft={BackIcon}/>
                <Text category={'h4'} style={{margin: 15}}>{route.params.title.substring(0, 20)}...</Text>
              </View>
              {
                loading ? 
                <View style={{flex: 1, position: 'absolute', zIndex: 999, justifyContent: 'center', alignItems: 'center', width: '100%', height: Dimensions.get('screen').height}}>
                  <ActivityIndicator color="green" size="large" />
                </View>
              :<></>
              }
              
              <WebView
                style={{flex: 1}}
                renderLoading={()=><Spinner size="medium" status="primary"/>}
                source={{uri: url}}
                javaScriptEnabled={true}
                cacheEnabled={true}
                onLoad={()=>{setloading(false)}}
                />
            </SafeAreaView>
          </Layout>
        );
      }
    }
  } catch (error) {
    
  }
  
  let html = (url == 'http://clarondoc.com/terms') ? Terms:About;
  // console.log(Terms)
  return (
    <Layout style={{flex: 1}}>
      <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
          <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} accessoryLeft={BackIcon}/>
          <Text category={'h4'} style={{margin: 15}}>{route.params.title.substring(0, 20)}...</Text>
        </View>
        {
          loading ? 
          <View style={{flex: 1, position: 'absolute', zIndex: 999, justifyContent: 'center', alignItems: 'center', width: '100%', height: Dimensions.get('screen').height}}>
            <ActivityIndicator color="green" size="large" />
          </View>
        :<></>
        }
        
        <WebView
          style={{flex: 1}}
          renderLoading={()=><Spinner size="medium" status="primary"/>}
          source={{html: html}}
          javaScriptEnabled={true}
          cacheEnabled={true}
          onLoad={()=>{setloading(false)}}
          />
      </SafeAreaView>
    </Layout>
  );
}

export default Web