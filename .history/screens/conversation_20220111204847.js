import React, { useEffect, useState } from 'react';
import { View, FlatList, Platform, Dimensions, TouchableOpacity, SafeAreaView, Alert } from 'react-native'
// import * as ImagePicker from 'expo-image-picker';
import { Layout, Text, Divider, Icon, Button, Input } from '@ui-kitten/components';
// import * as DocumentPicker from 'expo-document-picker';
import DocumentPicker from 'react-native-document-picker'
// import AsyncStorage from '@react-native-async-storage/async-storage';
import * as AsyncStorage from '../AsyncStorageCustom'

import { fetchConversation, sendMessage } from '../api/chats';
import { ChatBubble } from '../utils/components';
import firebase from 'firebase';
import {Image} from 'react-native-elements'
import { ActivityIndicator } from 'react-native-paper';

const BackIcon = (props) => (
    <Icon {...props} name='arrow-ios-back-outline'/>
);

const CloseIcon = (props) => (
    <Icon {...props} name='close-outline'/>
);

const SwapIcon = (props) => (
    <Icon {...props} name='swap-outline'/>
);

const ImageIcon = (props) => (
    <Icon {...props} name='image-outline'/>
);

const VideoIcon = (props) => (
    <Icon {...props} name='video-outline'/>
);

const CallIcon = (props) => (
    <Icon {...props} name='phone-outline'/>
);

let interval

const Conversation = ({route, navigation}) =>{

    const { doctor } = route.params
    const [chats, setChats] = useState([])
    const [me, setMe] = useState()
    const [message, setmessage] = useState('')
    const [attaching, setAttaching] = useState(false)
    const [attachment, setattachment] = useState()
    const [attachments, setattachments] = useState([])
    const [attachmenttype, setattachmenttype] = useState()
    const [attachmentsize, setattachmentsize] = useState()
    const [error, seterror] = useState()
    const [selected, setSelected] = useState()
    const [sendingNow, setsendingNow] = useState(false)
    const [sendingNowAA, setsendingNowAA] = useState(false)
    const [loadingChat, setloadingChat] = useState(false)

    const chat_code = (patient, doctor)=>{
        return patient+'-'+doctor;
    }

    const loadfirebasechat = async()=>{
        const from = await AsyncStorage.getItem('email');
        setMe(from)
        firebase.firestore().collection('newSMessages').doc(chat_code(from, doctor.email)).collection('messages').orderBy('timeStamp', 'desc').onSnapshot(snapshot=>{
            var r = snapshot.docs.map(doc =>{
                return (doc.data())
                  
                // return {
                //     id: doc.id,
                //     data: (doc.data())
                //   }
              // check if data is thesame as uid
            //   if(my_data.uid != doc.id){
                
            //   }
              
            });
    
            // Alert.alert(r.length)
            console.log(r)
            setChats(r)
            // setchat_record(r)
          }, error=>{
            console.log(error)
          });
    }

    const loadchat = async()=>{
        const from = await AsyncStorage.getItem('email');

        if(interval == null){
            clearInterval(interval)
        }

        let chat = await fetchConversation(from, doctor.email)
        setMe(from)
        setChats(chat)

        // console.log(chat[0])

        interval = setInterval(async()=>{
            let chat = await fetchConversation(from, doctor.email)
            setMe(from)
            setChats(chat)
        }, 10000)
        
    }

    useEffect(() => {
        // loadchat()
        (async()=>{
            try {
                await loadfirebasechat()    
            } catch (error) {
                console.log(error)
                Alert.alert(error.toString())
            }
        })()
        
        return () => {
            clearInterval(interval)
        }
    }, [])

    const attach = async () => {

        try{
            setsendingNowAA(true)
            let document = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
                allowMultiSelection: false,
            })

            let attached_files = []

            document.map(attached=>{
                if(attached.size/1024/1024 > 25){
                    seterror(`${attached.name} cannot be attached. Exceed the maximum upload size (25MB).`)
                    setsendingNowAA(false)
                }else{
                    attached_files.push(attached)
                }
            })

            setattachments(attached_files)    
            setattachmenttype(attached_files[0].type ? attached_files[0].type : '')
            setattachmentsize((attached_files[0].size/1024/1024).toFixed(2), 'MB')
            setattachment(attached_files[0])
            setsendingNowAA(false)
        }catch(e){
            console.log(e)
            setsendingNowAA(false)
        }
    }

    // added methods
    const getFileName = (name, path)=> {
        if (name != null) { return name; }

        if (Platform.OS === "ios") {
            path = "~" + path.substring(path.indexOf("/Documents"));
        }
        return path.split("/").pop();
    }

    const getPlatformPath = ({ path, uri }) => {
        return Platform.select({
            android: { "value": uri },
            ios: { "value": uri }
        })
    }

    const getPlatformURI = (imagePath) => {
        let imgSource = imagePath;
        if (isNaN(imagePath)) {
            imgSource = { uri: this.state.imagePath };
            if (Platform.OS == 'android') {
                imgSource.uri = "file:///" + imgSource.uri;
            }
        }
        return imgSource
    }

    const uriToBlob = (uri) => {
        return new Promise((resolve, reject) => {
          const xhr = new XMLHttpRequest();
          xhr.onload = function() {
            // return the blob
            resolve(xhr.response);
          };
          
          xhr.onerror = function() {
            // something went wrong
            reject(new Error('uriToBlob failed'));
          };
          // this helps us get a blob
          xhr.responseType = 'blob';
          xhr.open('GET', uri, true);
          
          xhr.send();
        });
      }

    const send = async () => {

        let url = ''; let type = '';

        var messag = message;
        if(message.length == 0){
            messag = 'Media Attachment';
        }

        setsendingNow(true)

        if(attachment){
            type = attachment.type

            // adding
            // console.log(attachment)
            var path = getPlatformPath(attachment).value;
            var name = getFileName(attachment.name, path);
            // console.log(name+'--'+path);
            // return false;
            try{
                var blob = await uriToBlob(attachment.uri);
                let attached = await firebase.storage().ref(`new-attaches/${name}`).put(blob, {contentType: type})
                url = await firebase.storage().ref(`new-attaches`).child(name).getDownloadURL()
                console.log(url)
                // return false;
            }catch(e){
                console.log('*****')
                console.log(e)
            }
        }

        try{

            let email = await AsyncStorage.getItem('email')

            let sen = {
                message: messag.trim(),
                recipient: doctor.email,
                attachment: url,
                file_type: type,
                sender: email,
                symptoms: [],
                createDate: (new Date()).toString(),
                timeStamp: Date.now()
            };
            // let sent = await sendMessage(sen)
            sendMessage(sen)

            // send to firebase
            await firebase.firestore().collection('newSMessages').doc(chat_code(email, doctor.email)).collection('messages').add(sen);

            // loadchat()

            if(true){
                setmessage('')
                setattachment(null)
                setloadingChat(true)
            }else{
                seterror('There was an error sending your message')
            }

        }catch(e){
            console.log('message sending failed: ', e)
        }

        setsendingNow(false)

    }

    const AttachIcon = (props) => (
        <TouchableOpacity onPress={attach}>
            <Icon {...props} name='attach-outline'/>
        </TouchableOpacity>
    );

    const SendIcon = (props) => (
        <TouchableOpacity onPress={send}>
            <Icon {...props} name='paper-plane-outline'/>
        </TouchableOpacity>
    );

    const LoadingIcon = (props) => (
        <ActivityIndicator {...props} color="green"></ActivityIndicator>
    );

    return (
        <Layout collapsable={false}  style={{ flex: 1, justifyContent: 'flex-end'}}>

            <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
            
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='basic' accessoryLeft={BackIcon}/>
                    <Text category={'h5'}>{doctor.firstname}</Text>
                    <TouchableOpacity onPress={()=>navigation.replace('Doctor', {doctor})}><Image source={{uri: doctor.avatar}} style={{width: 45, height: 45, borderRadius: 45, marginEnd: 15}}/></TouchableOpacity>
                </View>

                <Divider/>

                { doctor.availability != 'Online' ?
                <Layout level="4" style={{padding: 15}}>
                    <Text category={'s1'} appearance={'hint'} status={'danger'}>This doctor is currently offline and may take time to respond.</Text>
                    <TouchableOpacity onPress={()=>{ navigation.goBack(); navigation.replace('Doctors')}} style={{alignSelf: 'flex-end'}}><Text category={'s1'} status={'primary'}>Check Different Doctor</Text></TouchableOpacity>
                </Layout> : <></> }

                {   attaching ?
                <View style={{flex: 1, backgroundColor: 'black'}} >
                    <Button onPress={()=>setAttaching(false)} appearance='ghost' style={{ width: 50, alignSelf: 'flex-end', marginTop: 15, marginEnd: 15}} status='control' accessoryLeft={CloseIcon}/>
                    <Image source={{uri: selected}} style={{flex: 1}} resizeMode={'contain'}/>
                </View>
                :
                <FlatList
                    data={chats}
                    refreshing={loadingChat}
                    renderItem={({item, index})=>{
                        return <ChatBubble chat={item} me={me}/>
                    }}
                    shouldRasterizeIOS
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item)=>item.id}
                    inverted/>
                }
                
            <View style={{margin: 10}}>
                    { attachment ?
                        ['image/jpg', 'image/png', 'image/jpeg'].includes(attachmenttype.toLowerCase()) ?
                        <>
                        <TouchableOpacity style={{alignSelf: 'flex-end', padding: 10}} onPress={()=>setattachment(null)}><Icon name="close-outline" style={{height: 20, width: 20}} fill={'grey'}/></TouchableOpacity>
                        <Image PlaceholderContent={()=><ActivityIndicator size="large" />} source={{ uri: attachment.uri }} style={{height: 300, width: Dimensions.get('screen').width}} resizeMode={'contain'} />
                        </> : 
                        <Layout level="2" style={{padding: 15, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                            <Text status={'primary'}>Attached: {attachment.name}</Text>
                            <TouchableOpacity onPress={()=>setattachment(null)}><Icon name="trash-outline" style={{height: 15, width: 15}} fill={'red'}/></TouchableOpacity>
                        </Layout>
                        : null }
                    <Input
                        accessoryLeft={sendingNowAA ? LoadingIcon: AttachIcon}
                        accessoryRight={sendingNow ? LoadingIcon: SendIcon}
                        multiline mode='outlined'
                        value={message} 
                        onChangeText={setmessage}
                        size={'large'}
                        placeholder={'Type message...'}/>

                    {/* <Button onPress={attachImage} appearance='outline' style={{ marginEnd: 5}} status='primary' accessoryLeft={attaching ? SwapIcon : ImageIcon}/>
                    { attaching ? null : <Button onPress={attachFile} appearance='outline' style={{ marginEnd: 5}} status='primary' accessoryLeft={AttachIcon}/> }
                    <Button appearance='outline' status='primary' accessoryLeft={SendIcon}/> */}
                </View>
            </SafeAreaView>
        </Layout>
    );
}

export default Conversation