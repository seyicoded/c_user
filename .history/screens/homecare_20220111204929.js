import React, {useState, useEffect} from 'react';
import { View, ScrollView, Dimensions, SafeAreaView, TouchableOpacity, Alert, ActivityIndicator, Platform} from 'react-native';
import { Layout, TabBar, Tab, Text, Modal, Card, Autocomplete, AutocompleteItem, IndexPath, Select, SelectItem, Button, RadioGroup, Radio, Datepicker, Icon, Input, Calendar, ListItem, List, Divider, CheckBox } from '@ui-kitten/components';
import { fetchDoctors } from '../api/doctors';
import { isToday } from 'date-fns';
import moment from 'moment'
import DateTimePicker from '@react-native-community/datetimepicker';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import * as AsyncStorage from '../AsyncStorageCustom'

import { getSymptoms } from '../api/homecare';

const BackIcon = (props) => (
    <Icon {...props} name='chevron-left-outline'/>
);

const CalendarIcon = (props) => (
    <Icon {...props} name='calendar-outline'/>
);

let testnames = []
let genders = ['Male', 'Female']
let who = ['Self', 'Others']
let reasons = [
  'Routine Doctors Consultation', 
  'Nurse Home Visit', 
  'Routine Medical Check-up', 
  'Emergency Visit', 
  'Chronic Home Care Disease Managment'
]

const HomeCare = ({navigation}) => {

    
    const [step, setStep] = useState(0)
    const [tests, settests] = useState([])
    const [labtestnames, setlabtestnames] = useState([])
    const [filterednames, setfilterednames] = useState(testnames)
    const [isfacility, setisfacility] = useState(false)
    const [facilitymodal, setfacilitymodal] = useState(false)
    const [name, setName] = useState('')
    const [user, setuser] = useState()
    const [person, setperson] = useState(new IndexPath(0))
    const [labtests, setlabtests] = useState([])
    const [showdob, setshowdob] = useState(false)
    const [dob, setDob] = useState(moment().subtract(16, 'years').toDate())
    const [history, sethistory] = useState('')
    const [doctors, setdoctors] = useState([])
    const [reason, setreason] = useState(new IndexPath(0))
    const [phone, setPhone] = useState('')
    const [sex, setSex] = useState(0)
    const [total, settotal] = useState(0)
    const [doc, setdoc] = useState('')
    const [facility, setfacility] = useState({
        name: '',
        location: '',
        department: '',
        diagnosis: ''
    })
    const [address, setAddress] = useState('')
    const [error, seterror] = useState()
    const [date, setDate] = useState(new Date())
    const [showtime, setshowtime] = useState(false)
    const [time, setTime] = useState(new Date())
    const [loading, setloading] = useState(true)

    const next = ()=>{
        if(step == 0){
            if(isfacility){
                if(facilitymodal){
                    if(facility.name.trim().length == 0){
                        return seterror('facility.name')
                    }

                    if(facility.location.trim().length == 0){
                        return seterror('facility.location')
                    }

                    if(facility.diagnosis.trim().length == 0){
                        return seterror('facility.diagnosis')
                    }

                    setfacilitymodal(false)
                    setStep(step+1)

                }else{
                    setfacilitymodal(true)
                }
            }else{

                if(name.trim().length == 0 ){
                    return seterror('name')
                }

                if(address.trim().length == 0 ){
                    return seterror('address')
                }

                if(phone.trim().length != 10 ){
                    return seterror('phone')
                }

                setStep(step+1)
            }
        }else if(step == 1){
            if(labtests.length == 0){
                return Alert.alert('Symptoms Missing!', 'Please select symptoms are experiencing')
            }
            setStep(step+1)
        }else if(step == 2){
            setStep(step+1)
        }else{

            date.setHours(time.getHours())
            date.setMinutes(time.getMinutes())
            date.setSeconds(time.getSeconds())
            date.setMilliseconds(time.getMilliseconds())

            navigation.navigate('Payment', {
                item: {
                    netTotal: ['Basic', 'Premium', 'Family'].includes(user.subscription) ? (350-(350*0.15)) : 350+5,
                    total: ['Basic', 'Premium', 'Family'].includes(user.subscription) ? 350 : 350+5,
                    totalDiscount:  ['Basic', 'Premium', 'Family'].includes(user.subscription) ? 350*0.15 : 0,
                },
                data: {
                  schedule: date.toISOString(), 
                  address: address, 
                  names: name, 
                  gender: genders[sex], 
                  dob: dob, 
                  history: history, 
                  previousPhysicianName: doc, 
                  symptoms: labtestnames, 
                  reasons: reasons[reason-1], 
                  requestingFor: who[person-1], 
                  bestContact: phone, 
                  signature: true,
                  serviceId: 'homecarerequests'
                },
                type: 'homecare'
            })
        }
    }

    const saveTime = (e, time)=>{
        setTime(time)
    }

    const tocart = (test)=>{
        if(labtests.includes(test.id)){ 
            setlabtests(labtests.filter(t=>t != test.id)); 
            setlabtestnames(labtestnames.filter(t=>t != test.body)); 
        }else{ 
            setlabtests([ ...labtests, ...[test.id]]); 
            setlabtestnames([ ...labtestnames, ...[test.body]]); 
        }
    }

    const renderItem = ({ item, index }) => (
        <ListItem
            title={`${item.title}`}
            description={`${item.description}`}
            // onPress={()=>navigation.navigate(item.title)}
            accessoryLeft={ props=><Icon name={item.icon} {...props} /> }
        />
    );

    useEffect(() => {

        (async()=>{
            try{
                setloading(true);
                let account = await AsyncStorage.getItem('user')
                setuser(JSON.parse(account))
                let data = await getSymptoms()
                settests(data)
                data.map(s=>{
                    testnames.push(`${s.name}`)
                })
            }catch(e){
                Alert.alert('Error', e.message)
            }

            setloading(false);
        })()

    }, [])

    let lab_render = tests.map(test => (
        <TouchableOpacity onPress={()=>tocart(test)}>
            <Layout level={labtests.includes(test.id) ? '3' : '1' } style={{padding: 15}}>
                <Text>{test.body}</Text>
            </Layout>
        </TouchableOpacity>
    ))

    // return (<View></View>)

    return (
        <Layout style={{ flex: 1}}>
            <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>


                {/* Facility Details modal */}
                <Modal
                    visible={facilitymodal}
                    backdropStyle={{
                        backgroundColor: 'rgba(0, 0, 0, 0.3)'
                    }}>
                    <Card disabled style={{width: Dimensions.get('screen').width-30}}>
                        <Text category="h6" style={{textAlign: 'center', margin: 15}}>Facility Details</Text>
                        <Input label={ error == 'facility.name' ? "Please enter valid facility name" : "Facility Name"} status={ error == 'facility.name' ? 'danger' : 'basic' } value={facility.name} onChangeText={txt=>{ seterror(null); setfacility({...facility, ...{name: txt}}) }} placeholder="Facility Name" style={{marginBottom: 15}}/>
                        <Input label={ error == 'facility.location' ? "Please enter valid facility location" : "Facility Location"} status={ error == 'facility.location' ? 'danger' : 'basic' } value={facility.location} onChangeText={txt=>{ seterror(null); setfacility({...facility, ...{location: txt}}) }} placeholder="Facility Location" style={{marginBottom: 15}}/>
                        <Input label={"Referring Department"} value={facility.department} placeholder="Referring Department" onChangeText={txt=>{ seterror(null); setfacility({...facility, ...{department: txt}}) }} style={{marginBottom: 15}}/>
                        <Input label={ error == 'facility.diagnosis' ? "Please enter valid diagnosis" : "Diagnosis"} status={ error == 'facility.diagnosis' ? 'danger' : 'basic' } value={facility.diagnosis} onChangeText={txt=>{ seterror(null); setfacility({...facility, ...{diagnosis: txt}}) }} placeholder="Diagnosis" style={{marginBottom: 15}}/>
                        <Button onPress={next}>Proceed</Button>
                        <Button onPress={()=>setfacilitymodal(false)} appearance={'ghost'} status="basic">Cancel</Button>
                    </Card>
                </Modal>

                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Button onPress={()=> step == 0 ? navigation.goBack() : setStep(step-1)} appearance='outline' style={{ width: 50, margin: 15}} status='basic' accessoryLeft={BackIcon}/>
                    <Text category={'h4'} style={{marginEnd: 15}}>Home Care</Text>
                </View>

                <TabBar
                    style={{marginTop: 10}}
                    selectedIndex={step}
                    appearance={'default'}
                    shouldRasterizeIOS
                    // onSelect={index => setSelectedIndex(index)}
                    >
                    <Tab title='Details'/>
                    <Tab title={`Symptoms`}/>
                    <Tab title='Time'/>
                    <Tab title='Finish'/>
                </TabBar>

                <Divider/>

                { step == 1 ?
                <>
                    <Autocomplete placeholder="Search for test" style={{margin: 15}} onChangeText={q=>{ setfilterednames(testnames.filter(n=>n.toLowerCase().includes(q.toLowerCase()))) }} onSelect={i=>console.log(i)}>
                        {filterednames.map((name, i)=><AutocompleteItem title={name} key={i}/> )}
                    </Autocomplete>
                </>
                :
                <></> }

                <Layout style={{flex: 1}}>
                { step == 0 ?
                    <ScrollView shouldRasterizeIOS showsVerticalScrollIndicator={false}>
                        <Input 
                            mode='outlined'
                            style={{
                            marginHorizontal: 15,
                            marginTop: 15
                            }}
                            size={'large'}
                            value={name}
                            status={error == 'name' ? 'danger' : 'basic'}
                            label={error == 'name' ? 'Please enter name' : ''}
                            onChangeText={text=>{ seterror(null); setName(text)}}
                            placeholder={'Patient Name'}/>

                        <Select
                          style={{
                              marginHorizontal: 15,
                              marginTop: 15
                          }}
                          label={'Please select a reason'}
                          label={'Reason for service'}
                          value={reasons[reason-1]}
                          selectedIndex={reason}
                          onSelect={setreason}>
                          {reasons.map((r, i)=><SelectItem key={i} title={r}/>)}
                        </Select>

                        <Select
                          style={{
                              marginHorizontal: 15,
                              marginTop: 15
                          }}
                          label={'Please select who this if for'}
                          label={'Making request for'}
                          value={who[person-1]}
                          selectedIndex={person}
                          onSelect={setperson}>
                          {who.map((r, i)=><SelectItem key={i} title={r}/>)}
                        </Select>

                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 15, marginHorizontal: 15}}>
                            <Text>Date of Birth</Text>
                            {
                                (Platform.OS == 'android') ?
                                <View>
                                    <TouchableOpacity onPress={()=> setshowdob(!showdob)}>
                                        <Text style={{color: 'green'}}>Click to Change {dob.toLocaleDateString()}</Text>
                                    </TouchableOpacity>
                                    {
                                        showdob ? <DateTimePicker
                                        value={dob}
                                        mode={'date'}
                                        maximumDate={moment().subtract(16, 'years').toDate()}
                                        style={{
                                            flex: 1
                                        }}
                                        display={(Platform.OS == 'ios') ? 'compact':'default'}
                                        onChange={(e, d)=>{setshowdob(!showdob); setDob(d);}}
                                        />: <></>
                                    }
                                </View>
                                :
                                <DateTimePicker
                                value={dob}
                                mode={'date'}
                                maximumDate={moment().subtract(16, 'years').toDate()}
                                style={{
                                    flex: 1
                                }}
                                display={(Platform.OS == 'ios') ? 'compact':'default'}
                                onChange={(e, d)=>{setDob(d)}}
                                />
                            }
                            
                        </View>

                        <Text category={'label'} appearance={'hint'} style={{marginTop: 15, marginHorizontal: 15}}>Gender</Text>
                        <RadioGroup
                            shouldRasterizeIOS
                            selectedIndex={sex}
                            style={{
                            marginTop: 5,
                            marginHorizontal: 15,
                            flexDirection: 'row'
                            }}
                            onChange={index => setSex(index)}>
                            <Radio>Male</Radio>
                            <Radio>Female</Radio>
                        </RadioGroup>

                        <Input 
                            mode='outlined'
                            style={{
                            marginHorizontal: 15,
                            marginTop: 15
                            }} 
                            value={address}
                            status={error == 'address' ? 'danger' : 'basic'}
                            label={error == 'address' ? 'Please enter address' : ''}
                            onChangeText={text=>{ seterror(null); setAddress(text)}}
                            size={'large'}
                            placeholder={'Physical Address'}/>

                        <Input 
                            mode='outlined'
                            style={{
                            marginHorizontal: 15,
                            marginTop: 15
                            }} 
                            value={phone}
                            status={error == 'phone' ? 'danger' : 'basic'}
                            label={error == 'phone' ? 'Please enter a valid phone number' : ''}
                            onChangeText={text=>{ seterror(null); setPhone(text)}}
                            size={'large'}
                            placeholder={'Phone Number'}/>
                      
                        <Input
                            style={{
                                marginHorizontal: 15,
                                marginTop: 15
                            }}
                            status={error == 'doc' ? 'danger' : 'basic'}
                            label={error == 'doc' ? 'Please enter a doctor\'s name' : ''}
                            onChangeText={text=>{ seterror(null); setdoc(text)}}
                            placeholder={'Private Doctor (if none, enter none)'}/> 

                        <Input 
                            mode='outlined'
                            style={{
                            marginHorizontal: 15,
                            marginTop: 15
                            }} 
                            value={history}
                            multiline
                            numberOfLines={3}
                            onChangeText={sethistory}
                            size={'large'}
                            placeholder={'Medical History (if none, enter none)'}/>

                    </ScrollView>
                :
                step == 1 ?
                    <ScrollView shouldRasterizeIOS showsVerticalScrollIndicator={false}>
                        {lab_render}
                    </ScrollView>
                : step == 2 ?
                    <ScrollView shouldRasterizeIOS showsVerticalScrollIndicator={false}>
                        <Calendar
                            date={date}
                            style={{
                            marginTop: 15,
                            alignSelf: 'center'
                            }}
                            min={new Date()}
                            size={'large'}
                            onSelect={nextDate => { setDate(nextDate); isToday(nextDate) ? setTime(new Date()) : setTime(nextDate) }}
                            />

                        <View style={{flexDirection: 'row', alignItems: 'center', margin: 15, justifyContent: 'space-between'}}>
                            <Text category={'h6'}>Select Time</Text>
                            {
                                (Platform.OS == 'android') ?
                                <View>
                                    <TouchableOpacity onPress={()=> setshowtime(!showtime)}>
                                    <Text style={{color: 'green'}}>Click to Change {time.toLocaleTimeString()}</Text>
                                    </TouchableOpacity>
                                    {
                                        showtime ? <DateTimePicker
                                        value={time}
                                        mode={'time'}
                                        minimumDate={isToday(date) ? new Date() : date }
                                        style={{flex: 1}}
                                        is24Hour={true}
                                        display={(Platform.OS == 'ios') ? 'compact':'default'}
                                        onChange={(e, d)=>{setshowtime(!showtime); setTime(d);}}
                                        />: <></>
                                    }
                                </View>
                                :
                                <DateTimePicker
                                value={time}
                                mode={'time'}
                                minimumDate={isToday(date) ? new Date() : date }
                                style={{flex: 1}}
                                is24Hour={true}
                                display={(Platform.OS == 'ios') ? 'compact':'default'}
                                onChange={(e, d)=>{setTime(d)}}
                                />
                            }
                            {/* <DateTimePicker
                                value={time}
                                mode={'time'}
                                minimumDate={isToday(date) ? new Date() : date }
                                style={{flex: 1}}
                                is24Hour={true}
                                display={(Platform.OS == 'ios') ? 'compact':'default'}
                                onChange={(e, d)=>setTime(d)}
                                />  */}
                        </View>
                    </ScrollView>
                :

                    <View style={{flex: 1}}>
                        <List
                            data={[
                                {
                                    title: name,
                                    icon: 'person-outline',
                                    description: 'Patient Name',
                                },
                                {
                                    title: phone,
                                    icon: 'phone-outline',
                                    description: 'Phone Number',
                                },
                                {
                                    title: genders[sex],
                                    icon: 'person-outline',
                                    description: 'Gender',
                                },
                                {
                                    title: dob.toString().substring(0, 16),
                                    icon: 'calendar-outline',
                                    description: 'Date of Birth',
                                },
                                {
                                    title: address,
                                    icon: 'pin-outline',
                                    description: 'Physical Address',
                                },
                                {
                                    title: labtestnames.toString().replace(/,/g, ', '),
                                    icon: 'clipboard-outline',
                                    description: 'Symptoms',
                                },
                                {
                                    title: date.toString().substring(0, 16)+time.toString().substring(16, 21),
                                    icon: 'calendar-outline',
                                    description: 'Schedule Date',
                                },
                                {
                                    title: ['Basic', 'Premium', 'Family'].includes(user.subscription) ? `Free` : `GHS 5.00`,
                                    icon: 'pricetags-outline',
                                    description: 'Service Fee',
                                },
                                {
                                    title: ['Basic', 'Premium', 'Family'].includes(user.subscription) ? `GHS ${(350-(350*0.15)).toFixed(2)}` : `GHS ${(350+5).toFixed(2)}`,
                                    icon: 'pricetags-outline',
                                    description: 'Total Price',
                                }
                            ]}
                            renderItem={renderItem}
                        />
                    </View>

                }
                
                </Layout>

                <Button
                    onPress={next}
                    style={{
                    marginHorizontal: 15,
                    marginBottom: 15
                    }} 
                    >
                    Proceed
                </Button>

            </SafeAreaView>
        </Layout>
    );
}

export default HomeCare