import React from 'react'
import { View, StyleSheet, ScrollView } from 'react-native'
import { Layout, Text, Button, Icon, Card, Input } from '@ui-kitten/components';
import { FlatList } from 'react-native-gesture-handler';

const BackIcon = (props) => (
    <Icon {...props} onPress={()=>navigation.goBack()} name='arrow-ios-back-outline'/>
);

const ButtonIcon = (p) => {

}

export default function Search({navigation}) {
    return (
        <View style={styles.container}>
            <View style={{flexDirection: 'row'}}>
                <Input accessoryLeft={BackIcon} style={{flex: 1}} autoFocus={true} placeholder="type to start searching" returnKeyType="search" />
            </View>
            <ScrollView style={styles.container}>

            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
