import React from 'react';
import { View, Image, Dimensions, ScrollView, SafeAreaView, ActivityIndicator } from 'react-native'
import { Layout, Text, Card, Icon, Button } from '@ui-kitten/components';
import { useTheme } from 'react-native-paper';
import { useEffect, useState } from 'react';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '../AsyncStorageCustom'

import firebase from 'firebase'
import { colors } from '../utils/constants';
// import {Image} from 'react-native-elements'

const BackIcon = (props) => (
    <Icon {...props} name='arrow-ios-back-outline'/>
);

const ChatIcon = (props) => (
    <Icon {...props} name='message-circle-outline'/>
);

const SaveIcon = (props) => (
    <Icon {...props} name='heart-outline'/>
);

const VideoIcon = (props) => (
    <Icon {...props} name='video-outline'/>
);

const CallIcon = (props) => (
    <Icon {...props} name='phone-outline'/>
);

const CalendarIcon = (props) => (
    <Icon {...props} name='calendar-outline'/>
);

const DoctorProfile = ({route, navigation}) =>{

    const { doctor } = route.params
    const [loading, setloading] = useState(false)
    const [favorites, setfavorites] = useState([])
    const [loaded, setloaded] = useState(false)

    const favorite = async()=>{

        setloading(true)
        if(favorites.includes(doctor.email)){
            setfavorites(favorites.filter(fav=>fav!=doctor.email))
            await AsyncStorage.setItem('saved', favorites.filter(fav=>fav!=doctor.email).toString())
        }else{
            setfavorites([...favorites, ...[doctor.email]])
            await AsyncStorage.setItem('saved', [...favorites, ...[doctor.email]].toString())
        }
        setloading(false)
    }

    useEffect(()=>{

        (async()=>{

            if(!loaded){
                let saved = await AsyncStorage.getItem('saved')
                if(saved == null){
                    return
                }
                console.log(saved)
                setfavorites(saved.split(','))
                setloaded(true)
            }

        })()
        
    }, [])

    return (
        <Layout style={{ flex: 1, justifyContent: 'center'}}>
            <View style={{backgroundColor: colors.primary, width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/2, position: 'absolute', top: 0 }}/>
            
            <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
                <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='control' accessoryLeft={BackIcon}/>

                <ScrollView shouldRasterizeIOS showsVerticalScrollIndicator={false}>
                    <Card disabled style={{margin: 15, borderRadius: 10}}>
                        <Button onPress={()=>navigation.navigate('Conversation', {to: doctor.email, name: doctor.firstname+' '+doctor.lastname, doctor})} style={{width: 50, alignSelf: 'flex-end'}} appearance='outline' status='primary' accessoryLeft={ChatIcon}/>
                        
                        <View  style={{justifyContent: 'center', alignItems: 'center'}}>
                            <View style={{width: 100, height: 100, marginBottom: 15}}>
                                <Image PlaceholderContent={()=><ActivityIndicator size="large" />} source={{uri: doctor.avatar}} resizeMode={'cover'} style={{width: 100, height: 100, borderRadius: 100, backgroundColor: '#5AB9AF'}}/>
                                { doctor.availability == 'Online' ? <View style={{marginStart: 5, height: 15, width: 15, borderRadius: 15, backgroundColor: 'green', bottom: 25, left: 75}}/> : null }
                            </View>
                            <Text category='h6'>{doctor.firstname} {doctor.lastname}</Text>
                            <Text category='s1' appearance={'hint'} style={{margin: 10}}>{doctor.department}</Text>
                            <Text category='s1' style={{margin: 10, alignSelf: 'flex-start'}}>About {doctor.firstname}</Text>
                            <Text category='p1' appearance={'hint'} style={{marginHorizontal: 10, marginBottom: 15, alignSelf: 'flex-start'}}>{doctor.bio}</Text>

                            <Button disabled={loading} onPress={favorite} appearance='outline' style={{flex: 1, width: '100%', marginTop: 15}} status='primary' accessoryLeft={SaveIcon}>
                                { loading ? 'Saving...' : favorites.includes(doctor.email) ? 'Remove from Account' : 'Save to Account'}
                            </Button>

                            <Button onPress={()=>navigation.navigate('Availability', { doctor })} appearance='outline' style={{flex: 1, width: '100%', marginTop: 15}} status='primary' accessoryLeft={CalendarIcon}>
                                Check Availability
                            </Button>

                        </View>
                    </Card>                
                </ScrollView>
            </SafeAreaView>

        </Layout>
    );
}

export default DoctorProfile