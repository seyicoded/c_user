import React, { useState, useEffect, useRef } from 'react';
import { formatDistanceToNow } from 'date-fns'
import * as RN from 'react-native'
import { Layout, Text, Icon, List, ListItem, Button, Divider } from '@ui-kitten/components';
import { NativeModules, View, Image, SafeAreaView, ActivityIndicator, Dimensions, FlatList } from 'react-native';
import { fetchNotifications } from '../api/notifications';
import { fetchDrugs2 } from '../api/pharmacy';
import { getLabTests } from '../api/lab';
import AsyncStorage from '../AsyncStorageCustom'

const BackIcon = (props) => (
    <Icon {...props} name='arrow-ios-back-outline'/>
);

const Notifications = ({navigation, route}) => {

    const [notifications, setNotifications] = useState([])
    const [loading, setloading] = useState(true)
    const [loading2, setloading2] = useState(false)

    const drugs = useRef([]);
    const lab = useRef([]);

    const renderItemIcon = (props) => (
        <Icon {...props} name='bell-outline'/>
    );

    const silentLoadDrugs = async ()=>{
        let found = await fetchDrugs2()
        // console.log('drugs list are')
        // console.log(found)
        drugs.current = found;
    }
    const silentLoadLab = async ()=>{
        let data = await getLabTests()
        lab.current = data
    }

    const processDrug = async (item)=>{
        setloading2(true)
        if(drugs.current == [] ){
            let found = await fetchDrugs2()
            drugs.current = found;
        }

        var drug = item.extraData1.drugs;
        var drugToSend = []
        var totalCost = 0;
        for (let i = 0; i < drug.length; i++) {
            const element = drug[i];
            
            // map through drug list to get drug price
            var price = 0;
            var drg_item = null;
            var drg_id = null;
            drugs.current.forEach(element2 => {
                if(element2.name == element.name){
                    price = element2.unitprice;
                    drg_item = element2;
                    drg_id = element2.id;
                    totalCost += parseFloat(element2.unitprice)
                }
            });

            drugToSend[i] = {
                charges: price,
                unitprice: price,
                drug: drg_item,
                qty: 1,
                quantity: 1,
                drugId: drg_id,
                name: element.name
            }
        }
        // console.log(drugToSend)
        let user = await AsyncStorage.getItem('user')
        setloading2(false)

        navigation.navigate('Cart', {cart: drugToSend, total: totalCost, user: user})
    }
    const processLab = async (item)=>{
        setloading2(true)
        if(lab.current == [] ){
            let data = await getLabTests()
            lab.current = data
        }

        var labz = item.extraData1.labs;
        // console.log(lab.current[0])
        var labToSend = []
        var totalCost = 0;
        for (let i = 0; i < labz.length; i++) {
            const element = labz[i];
            
            // map through drug list to get drug price
            var price = 0;
            var lab_item = null;
            var lab_id = null;
            lab.current.forEach(element2 => {
                if(element2.name == element.name){
                    price = element2.charges;
                    lab_item = element2;
                    lab_id = element2.id;
                    totalCost += parseFloat(element2.charges)
                }
            });

            labToSend[i] = {
                charges: price,
                unitprice: price,
                name: element.name,
                qty: 1,
                quantity: 1,
                labId: lab_id,
                id: lab_id
            }
        }

        //  other data
        var data2 = item.extraData2;
        var doctor_name = data2.doctor.firstname+' '+data2.doctor.firstname;
        var patient_name = data2.user.firstname+' '+data2.user.lastname;
        var purpose = 'Doctor Recommendation';
        var address = data2.user.address;
        var gender = data2.user.sex;
        var dob = data2.user.age;
        var schedule = (new Date()).toISOString();
        var total = totalCost;

        let account = await AsyncStorage.getItem('user')
        var user = (JSON.parse(account))

        setloading2(false)

        navigation.navigate('Payment', {
            item: {
                netTotal: ['Basic', 'Premium', 'Family'].includes(user.subscription) ? (total-(total*0.15))+5 : total+5,
                total: ['Basic', 'Premium', 'Family'].includes(user.subscription) ? total : total+5,
                totalDiscount:  ['Basic', 'Premium', 'Family'].includes(user.subscription) ? total*0.15 : 0,
            },
            data: {
                labTests: labToSend, 
                schedule: schedule, 
                doctor: doctor_name, 
                names: patient_name, 
                purpose: purpose, 
                address: address, 
                gender: gender, 
                dob: dob, 
                signature: true,
                type: 'individual'
            },
            type: 'lab'
        })

    }
    
    const renderItem = ({ item, index }) => {
        var title = item.body;
        var type = 0;
        /*
            type
            0 = normal action
            1 = drugs
            2 = labs
        */
        try {
            if(item.extraData1 != null){
                // console.log(item.extraData1)
                if(item.extraData1.drugs != null){
                    // console.log('aaa')
                    var doc_array = title.split(' ');
                    var doc = doc_array[doc_array.length - 1]
                    title = 'You have a Drug Prescription from '+ doc;
                    type = 1;
                }

                if(item.extraData1.labs != null){
                    // console.log('aaa')
                    var doc_array = title.split(' ');
                    var doc = doc_array[doc_array.length - 1]
                    title = 'Lab Request from '+ doc;
                    type = 2;
                }
            }
        } catch (error) {
            console.log(error)
        }
        
        return (
        <ListItem
            // onPress={()=> item.body.includes('appointment') ? navigation.navigate('Appointments') : item.body.includes('new message') ? navigation.navigate('Conversation', {to: item.extraData2.doctor.email, name: item.extraData2.doctor.firstname+' '+item.extraData2.doctor.lastname, doctor: item.extraData2.doctor}) : navigation.navigate('History', { type: item.body.includes('Ambulance') ? 'ambulance' : item.body.includes('Lab Request') ? 'lab' : item.body.includes('Home') ? 'homecare' : 'drug' }) }
            onPress={()=> (type == 0) ? (item.body.includes('appointment') ? navigation.navigate('Appointments') : item.body.includes('new message') ? navigation.navigate('Conversation', {to: item.extraData2.doctor.email, name: item.extraData2.doctor.firstname+' '+item.extraData2.doctor.lastname, doctor: item.extraData2.doctor}) : navigation.navigate('History', { type: item.body.includes('Ambulance') ? 'ambulance' : item.body.includes('Lab Request') ? 'lab' : item.body.includes('Home') ? 'homecare' : 'drug' })) : ( (type == 1) ? processDrug(item):processLab(item) ) }
            title={title}
            description={formatDistanceToNow(new Date(item.createDate), { addSuffix: true })}
            accessoryLeft={renderItemIcon}
        />)
    };

    useEffect(()=>{
        // lab.current = []
        // drugs.current = []

        (async()=>{
            setloading(true);
          let response = await fetchNotifications()
          setNotifications(response)
          setloading(false);
        //   silent load data
          silentLoadDrugs()
          silentLoadLab()
        //   console.log(response[0])
        })()
    
    }, [])

    return (
    <Layout style={{flex: 1}}>
        <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='basic' accessoryLeft={BackIcon}/>
                <Text category={'h4'} style={{marginEnd: 15}}>Notifications</Text>
            </View>

            <Divider/>
            { notifications.length > 0 ?
                // <List
                //     data={notifications}
                //     renderItem={renderItem}
                //     /> 

                <FlatList
                    data={notifications}
                    renderItem={renderItem}
                    initialNumToRender={50}
                    /> 
                :
                <Layout style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    {
                        loading ? 
                        <ActivityIndicator />
                        :
                        <Text category={'h5'} appearance={'hint'} style={{marginTop: 10}}>No notifications.</Text>
                    }
                </Layout>
            }
            {
            (loading2 == true) ? <RN.View style={{position: 'absolute', height: Dimensions.get('screen').height, width: Dimensions.get('screen').width, zIndex: 999, top: 0, bottom: 0, left: 0, right: 0, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator size='large' />
                </RN.View>:<></>
            }
        </SafeAreaView>
    </Layout>
    );
}

export default Notifications