import React, {useState, useEffect} from 'react'
import { View, StyleSheet, ScrollView } from 'react-native'
import { Layout, Text, Button, Icon, Card, Input } from '@ui-kitten/components';
import { FlatList } from 'react-native-gesture-handler';

export default function Search({navigation, route}) {

    // const menu = params.route;
    console.log(route)
    const BackIcon = (props) => (
        <Icon {...props} onPress={()=>navigation.goBack()} name='arrow-ios-back-outline'/>
    );

    // let's the search go through params doctor then other doctor but using get to obtain data
    
    return (
        <View style={styles.container}>
            <View style={{flexDirection: 'row'}}>
                <Input accessoryLeft={BackIcon} style={{flex: 1,}} autoFocus={true} placeholder="type to start searching" returnKeyType="search" />
            </View>
            <ScrollView style={styles.container}>

            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
