import React, { useEffect, useState } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { Layout, Text, Button, Icon, Divider, Spinner } from '@ui-kitten/components';
import { View, StyleSheet, SafeAreaView, FlatList } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Doctor } from '../utils/components';
import firebase from 'firebase'
import { fetchDoctors } from '../api/doctors';

const BackIcon = (props) => (
  <Icon {...props} name='arrow-ios-back-outline'/>
);

const SavedDoctors = ({navigation}) => {

  const [loading, setloading] = useState(true)
  const [doctors, setDoctors] = useState([])
  const [filtered, setFiltered] = useState([])

  const search = (query) => {
    setFiltered(doctors.filter(doctor=>
        doctor.firstname.toLowerCase().includes(query.toLowerCase()) ||
        doctor.lastname.toLowerCase().includes(query.toLowerCase()) ||
        doctor.department.toLowerCase().includes(query.toLowerCase()) ||
        doctor.email.toLowerCase().includes(query.toLowerCase())
    ))
  }

  useFocusEffect(
    React.useCallback(() => {
      async function load(){
        let saved = await AsyncStorage.getItem('saved')
        if(saved == null){
          return setloading(false)
        }
        saved = saved.split(',')
        let found = (await fetchDoctors()).filter(doc=>saved.includes(doc.email))
        setDoctors(found)
        setFiltered(found)
        setloading(false)
      }
      load()
    }, [])
  )

  return (
      <Layout style={{ flex: 1}}>

        <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>

          <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
            <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} accessoryLeft={BackIcon}/>
            <Text category={'h4'} style={{margin: 15}}>My Favorite Doctors</Text>
          </View>
          <Divider/>

          { loading ?
          
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Spinner/>
          </View>
          
          :
          filtered.length == 0 ?

              <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                  <Text category={'h5'} appearance={'hint'} style={{marginHorizontal: 15, textAlign: 'center'}}>
                      You have not saved any doctor yet.
                  </Text>
              </View>

          :

              <FlatList
                  data={filtered}
                  renderItem={({item, index})=>
                      <Doctor doctor={item}
                          nav={navigation}
                          navigate={()=>navigation.navigate('Doctor', {doctor: item})}/>
                  }
                  keyExtractor={item=>item.email}
                  showsVerticalScrollIndicator={false}
              />
          }
        </SafeAreaView>
      </Layout>
  );
}

export default SavedDoctors

const style = StyleSheet.create({
  top: {
    padding: 15,
  }
})