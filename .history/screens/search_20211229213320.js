import React, {useState, useEffect} from 'react'
import { View, StyleSheet, ScrollView } from 'react-native'
import { Layout, Text, Button, Icon, Card, Input } from '@ui-kitten/components';
import { FlatList } from 'react-native-gesture-handler';

const main = [
    {
        name: 'Subscribe',
        screen: 'Subscription',
        icon: require('../assets/images/subscription.png')
    },
    {
        name: 'Urgent Care',
        screen: 'Urgent',
        icon: require('../assets/images/urgent_care.png')
    },
    {
        name: 'Emergency',
        screen: 'Ambulance',
        icon: require('../assets/images/ambulance.png')
    },
    {
        name: 'Consultation',
        screen: 'Doctors',
        icon: require('../assets/images/doctor.png')
    },
    {
        name: 'Pharmacy',
        screen: 'Pharmacy',
        icon: require('../assets/images/pharmacy_.png')
    },
    {
        name: 'Laboratory',
        screen: 'Laboratory',
        icon: require('../assets/images/lab.png')
    },
    {
        name: 'Home Care',
        screen: 'Emergency',
        icon: require('../assets/images/homecare.png')
    }
]

const secondary = [
    {
        name: 'Urgent Care',
        screen: 'Urgent',
        icon: require('../assets/images/urgent_care.png')
    },
    {
        name: 'Emergency',
        screen: 'Ambulance',
        icon: require('../assets/images/ambulance.png')
    },
    {
        name: 'Consultation',
        screen: 'Doctors',
        icon: require('../assets/images/doctor.png')
    },
    {
        name: 'Pharmacy',
        screen: 'Pharmacy',
        icon: require('../assets/images/pharmacy_.png')
    },
    {
        name: 'Laboratory',
        screen: 'Laboratory',
        icon: require('../assets/images/lab.png')
    },
    {
        name: 'Home Care',
        screen: 'Emergency',
        icon: require('../assets/images/homecare.png')
    },    
    {
        name: 'Upgrade',
        screen: 'Subscription',
        icon: require('../assets/images/subscription.png')
    }
]

export default function Search({navigation}) {

    const BackIcon = (props) => (
        <Icon {...props} onPress={()=>navigation.goBack()} name='arrow-ios-back-outline'/>
    );

    // let's the search go through params doctor then other doctor but using get to obtain data
    
    return (
        <View style={styles.container}>
            <View style={{flexDirection: 'row'}}>
                <Input accessoryLeft={BackIcon} style={{flex: 1,}} autoFocus={true} placeholder="type to start searching" returnKeyType="search" />
            </View>
            <ScrollView style={styles.container}>

            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
