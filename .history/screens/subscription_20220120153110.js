import React, {useEffect, useState} from 'react';
import PagerView from 'react-native-pager-view';
import { Dimensions, SafeAreaView, StyleSheet, View } from 'react-native';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '../AsyncStorageCustom'
import { Layout, Text, Button, Icon } from '@ui-kitten/components';
import { colors } from '../utils/constants';

const BackIcon = (props) => (
  <Icon {...props} name='chevron-left-outline'/>
);

const prices = []
prices['Basic'] = 20
prices['Premium'] = 40
prices['Family'] = 100

const Subscription = ({navigation}) => {

  const [user, setUser] = useState()


  useEffect(()=>{

    (async()=>{

      let account = await AsyncStorage.getItem('user')
      setUser(JSON.parse(account))

    })()

  }, [])

  return (
    <Layout style={{ flex: 1}}>

      <View style={{backgroundColor: colors.primary, width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/3, position: 'absolute', top: 0 }}/>

      <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>

        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
          <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='control' accessoryLeft={BackIcon}/>
          <Text category={'h5'} style={{color: 'white', margin: 15}}>Swipe to select plan</Text>
        </View>
        
        <View style={{flex: 1}} shouldRasterizeIOS showsVerticalScrollIndicator={false}>

          { user ?
          <>
          <PagerView style={{ flex: 1, backgroundColor: 'transparent', marginHorizontal: 15 }} overScrollMode={'always'} initialPage={ user.subscription == 'Premium' || user.subscription == 'Premium' ? 1 : 0 }>
            { user.subscription != 'Basic' ?
            <Layout style={styles.page} key="1">
              <Layout style={styles.active}>
                <Text category={'h2'}>Basic Plan</Text>
                <Text category={'h6'} appearance={'hint'} status={'danger'}>15% discount on all services</Text>

                <View style={{marginTop: 25}}>
                  <Text category={'s1'} appearance={'default'}>168hrs open chat consult</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>1 voice call consultation</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>1 video call consultation</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited Home Care requests</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited Ambulance requests</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited Mobile Pharmacy requests</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited Mobile Lab requests</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>1 Valid subscriber</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Free prescriptions and delivery</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Health tips</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>24/7 Support</Text>
                </View>

                <Button onPress={()=>navigation.navigate('PaySub', {name: 'Basic Plan', id: '', price: 20})} style={{marginVerticalTop: 25, bottom: 25, left: 25, right: 25, position: 'absolute'}}>
                  { user.subscription == null || user.subscription == 'Normal' ? 'Subscribe' : user.subscription == 'Family' || user.subscription == 'Premium' ? 'Downgrade' : 'Upgrade'} for GHS 20/month
                </Button>
              </Layout>
            </Layout> : <></> }

            { user.subscription != 'Premium' ?
            <Layout style={styles.page} key="2">
              <Layout style={styles.active}>
                <Text category={'h2'}>Premium Plan</Text>
                <Text category={'h6'} appearance={'hint'} status={'danger'}>15% discount on all services</Text>

                <View style={{marginTop: 25}}>
                  <Text category={'s1'} appearance={'default'}>Unlimited open chat consult</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited voice call consultation</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited video call consultation</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited Home Care requests</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited Ambulance requests</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited Mobile Pharmacy requests</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited Mobile Lab requests</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>1 Valid subscriber</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Free prescriptions and delivery</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Health tips</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>24/7 Support</Text>
                </View>

                <Button onPress={()=>navigation.navigate('PaySub', {name: 'Premium Plan', id: '', price: 40})} style={{marginVerticalTop: 25, bottom: 25, left: 25, right: 25, position: 'absolute'}}>
                  { user.subscription == null || user.subscription == 'Normal' ? 'Subscribe' : user.subscription == 'Family' ? 'Downgrade' : 'Upgrade'} for GHS 40/month
                </Button>
              </Layout>
            </Layout> : <></> }

            { user.subscription != 'Family' ?
            <Layout style={styles.page} key="3">
              <Layout style={styles.active}>
                <Text category={'h2'}>Family Plan</Text>
                <Text category={'h6'} appearance={'hint'} status={'danger'}>15% discount on all services</Text>

                <View style={{marginTop: 25}}>
                  <Text category={'s1'} appearance={'default'}>Unlimited open chat consult</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited voice call consultation</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited video call consultation</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited Home Care requests</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited Ambulance requests</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited Mobile Pharmacy requests</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Unlimited Mobile Lab requests</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>1 subscriber + 10 Family Members</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Free prescriptions and delivery</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>Health tips</Text>
                  <Text category={'s1'} appearance={'default'} style={{marginTop: 10}}>24/7 Support</Text>
                </View>

                <Button onPress={()=>navigation.navigate('PaySub', {name: 'Family Plan', id: '', price: 100})} style={{marginVerticalTop: 25, bottom: 25, left: 25, right: 25, position: 'absolute'}}>
                { user.subscription == null || user.subscription == 'Normal' ? 'Subscribe' : 'Upgrade'} for GHS 100/month
                </Button>
              </Layout>
            </Layout> : <></> }
          </PagerView>

          <Button onPress={()=>{ user.subscription != null && user.subscription != 'Normal' ? navigation.navigate('PaySub', {name: `${user.subscription} Plan`, id: '', price: prices[user.subscription]}) : navigation.goBack()}} appearance={'ghost'} style={{margin: 25}}>
            {user.subscription != null && user.subscription != 'Normal' ? `Continue with ${user.subscription} Plan` : 'Try ClaronDoc free for 14 days'}
          </Button>
          </>
          : null }
        </View>
      </SafeAreaView>
    </Layout>
  );
}

export default Subscription

const styles = StyleSheet.create({
  viewPager: {
    flex: 1,
    marginHorizontal: 15,
    backgroundColor: 'transparent'
  },
  page: {
    flex: 1,
  },
  active: {
    // borderRadius: 5,
    borderWidth: 1,
    borderColor: '#5AB9AF',
    padding: 35,
    flex: 1,
  }
});