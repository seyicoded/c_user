import React, {useState} from 'react';
import { View, FlatList, SafeAreaView, ActivityIndicator, TouchableOpacity, StyleSheet, Modal, Pressable} from 'react-native';
import { Layout, TabBar, Tab, Text, Button, Card, Icon, Divider } from '@ui-kitten/components';
import { useEffect } from 'react';
import * as API from '../api/pharmacy';
import moment from 'moment';

const BackIcon = (props) => (
  <Icon {...props} name='chevron-left-outline'/>
);

const CancelIcon = (props) => (
  <Icon {...props} name='close-outline'/>
);

const History = ({navigation, route}) => {

  const { type } = route.params
  const [index, setIndex] = useState(0)
  const [bookings, setBookings] = useState([])
  const [filtered, setFiltered] = useState([])
  const [loaded, setLoaded] = useState(false)
  const [modalVisible, setModalVisible] = useState(false);
  const [modalIndex, setmodalIndex] = useState(0);
  

  // console.log(filtered)

  const filter = async(sel)=>{
    setLoaded(false);
    if(type == 'lab'){
      setFiltered([])
      if(sel == 0){
        res = await API.myLabTests('individual')
        setFiltered(res.requests)
      }else{
        res = await API.myLabTests('facility')
        setFiltered(res.requests)
      }
    }else if(type == 'ambulance'){
      sel == 0 ?
      setFiltered(bookings.filter(request=>request.status != 'Completed'))
      : setFiltered(bookings.filter(request=>request.status == 'Completed'))
    }

    setLoaded(true);
    // sel == 0 ?
    //   setFiltered(bookings.filter(booking=>moment().isBefore(new Date(booking.scheduledFor))))
    // : setFiltered(bookings.filter(booking=>moment().isAfter(new Date(booking.scheduledFor))))
  }

  useEffect(()=>{
    (async()=>{

      if(!loaded){
        try{
          let res
          
          if(type == 'lab'){
            res = await API.myLabTests('individual')
            setBookings(res.requests)
            setFiltered(res.requests)
          }else if(type == 'drug'){
            res = await API.myPharmacyOrders()
            setBookings(res.orders)
            setFiltered(res.orders)
          }else if(type == 'homecare'){
            res = await API.myHomecareRequests()
            setBookings(res.requests)
            setFiltered(res.requests.filter(request=>request.status != 'Completed'))
          }else{
            res = await API.myAmbulanceRequests()
            setBookings(res.requests)
            setFiltered(res.requests.filter(request=>request.status != 'Completed'))
          }

        }catch(e){
          console.log(e)
        }
        setLoaded(true)
      }

    })()
  })

  const renderLab = (item)=>(
    <Card style={{marginHorizontal: 15, marginVertical: 5}}>
      <View>
        <Text category='s1' style={{marginBottom: 10}}>REQUESTED TESTS</Text>
        {item.labTests.map(test=><View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
          <Text>{test.labtest}</Text>
          {console.log(test)}
          <Text appearance={'hint'}>GHS {test.charge}</Text>
          </View>)}
      </View>
      <Divider style={{marginVertical: 10}}/>
      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        <Text>Booked On: </Text>
        <Text appearance={'hint'}>{new Date(item.createDate).toString().substring(0, 21)}</Text>
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 5}}>
        <Text>Booked For: </Text>
        <Text appearance={'hint'}>{new Date(item.scheduledFor).toString().substring(0, 21)}</Text>
      </View>
    </Card>
  )

  const renderEmergency = (item)=>(
    <Card style={{marginHorizontal: 15, marginVertical: 5}}>
      <View>
        <Text category='s1' style={{marginBottom: 10}}>REQUEST DETAILS</Text>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
          <Text>Address</Text>
          <Text appearance={'hint'}>{item.pickup}</Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 5}}>
          <Text>Emergency</Text>
          <Text appearance={'hint'}>{item.conditions[0]}</Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 5}}>
          <Text>Comments</Text>
          <Text appearance={'hint'}>{item.conditions.length > 1 ? item.conditions[1] : ''}</Text>
        </View>
      </View>
      <Divider style={{marginVertical: 10}}/>
      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        <Text>Requested On: </Text>
        <Text appearance={'hint'}>{new Date(item.createDate).toString().substring(0, 21)}</Text>
      </View>
    </Card>
  )

  const renderDrugs = (item)=>(
    <Card style={{marginHorizontal: 15, marginVertical: 5}}>
      <View>
        <Text category='s1' style={{marginBottom: 10}}>PHARMACY ORDER</Text>
        {item.drugsOrdered.map(drug=><View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
          <Text>{drug.drugName} x {drug.quantity}</Text>
          <Text appearance={'hint'}>GHS {(drug.total*1).toFixed(2)}</Text>
          </View>)}
      </View>
      <Divider style={{marginVertical: 10}}/>
      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        <Text>Ordered On: </Text>
        <Text appearance={'hint'}>{new Date(item.createDate).toString().substring(0, 21)}</Text>
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 5}}>
        <Text>Order Status: </Text>
        <Text status={item.status == 'Pending' ? 'warning' : item.status == 'Cancelled' ? 'danger' : item.status == 'Completed' ? 'success' : 'info'}>{item.status}</Text>
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        <Text>Ordered Type: </Text>
        <Text appearance={'hint'}>{item.deliveryOption}</Text>
      </View>

      {item.status == 'Pending' ?
      <Button size='small' onPress={()=>navigation.navigate('Payment', {item, type: 'drug', data: {serviceId: 'orders'}})} appearance='outline' style={{marginTop: 10}}>Make Payment</Button> : <></> }

    </Card>
  )

  const renderHC = (item, index)=>(
    <Card style={{marginHorizontal: 15, marginVertical: 5}}>
      <View>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        <Text category='s1' style={[{marginBottom: 10}, styles.hc_header_left]}>{item.reasons[0]}</Text>
          <Text appearance={'hint'} style={[styles.hc_header_right, {color: (item.status == 'Pending') ? 'orange' : ((item.status == 'Success') ? 'green' : 'black') }]}>{item.status}</Text>
        </View>
      </View>
      
      <Divider style={{marginVertical: 10}}/>
      
      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        <Text style={styles.hc_content_left}>Request Id: </Text>
        <Text style={styles.hc_content_right} appearance={'hint'}>{item.id}</Text>
      </View>

      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        <Text style={styles.hc_content_left}>Schedule Date: </Text>
        <Text style={styles.hc_content_right} appearance={'hint'}>{new Date(item.scheduledFor).toString().substring(0, 21)}</Text>
      </View>

      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        <Text style={styles.hc_content_left}>Patient ({item.sex}) </Text>
        <Text style={styles.hc_content_right} appearance={'hint'}>{item.names}</Text>
      </View>

      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        <Text style={styles.hc_content_left}>Symptoms </Text>
        <Text style={styles.hc_content_right} appearance={'hint'}>{( (item.symptoms).length <= 1 && ( (item.symptoms)[0] == "" ) ) ? "N.A":(item.symptoms).map((it)=>{return (<Text>{it}, </Text>)})}</Text>
      </View>

      <Divider style={{marginVertical: 10}}/>
      
      <View style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end'}}>
        <TouchableOpacity onPress={()=>show_Modal(index)}>
          <Text style={styles.hc_vd}>View Details</Text>
        </TouchableOpacity>
      </View>
    </Card>
  )

  const show_Modal = index=>{
    console.log(index);
    setmodalIndex(index)
    setModalVisible(true)
  }

  // console.log(filtered)
  return (
      <Layout style={{ flex: 1}}>
        <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>

          <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
            <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='basic' accessoryLeft={BackIcon}/>
            <Text category={'h4'} style={{marginEnd: 15}}>{type.toUpperCase()} HISTORY</Text>
          </View>
          
          { type == 'drug' || type == 'homecare' ?
            <></> :
            <TabBar
              style={{marginTop: 10}}
              selectedIndex={index}
              appearance={'default'}
              shouldRasterizeIOS
              onSelect={(index)=>{
                setIndex(index)
                filter(index)
              }}
              >
              <Tab title={type == 'lab' ? 'Individual' : 'Pending'}/>
              <Tab title={type == 'lab' ? 'Facility' : 'Completed'}/>
            </TabBar>
          }

          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              setModalVisible(!modalVisible);
            }}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                
                {
                  ((filtered.length > 0) && (type != "lab" && type != "drug" && type != "ambulance")) ? 
                  <View>
                    <Card style={{marginHorizontal: 15, marginVertical: 5}}>
                      <View>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Text category='s1' style={[{marginBottom: 10}, styles.hc_header_left]}>{filtered[modalIndex].reasons[0]}</Text>
                          <Text appearance={'hint'} style={[styles.hc_header_right, {color: (filtered[modalIndex].status == 'Pending') ? 'orange' : ((filtered[modalIndex].status == 'Success') ? 'green' : 'black') }]}>{filtered[modalIndex].status}</Text>
                        </View>
                      </View>
                      
                      <Divider style={{marginVertical: 10}}/>
                      
                      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Text style={styles.hc_content_left}>Request Id: </Text>
                        <Text style={styles.hc_content_right} appearance={'hint'}>{filtered[modalIndex].id}</Text>
                      </View>

                      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Text style={styles.hc_content_left}>Schedule Date: </Text>
                        <Text style={styles.hc_content_right} appearance={'hint'}>{new Date(filtered[modalIndex].scheduledFor).toString().substring(0, 21)}</Text>
                      </View>

                      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Text style={styles.hc_content_left}>Patient ({filtered[modalIndex].sex}) </Text>
                        <Text style={styles.hc_content_right} appearance={'hint'}>{filtered[modalIndex].names}</Text>
                      </View>

                      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Text style={styles.hc_content_left}>Contact</Text>
                        <Text style={styles.hc_content_right} appearance={'hint'}>{filtered[modalIndex].bestContact}</Text>
                      </View>

                      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Text style={styles.hc_content_left}>Email</Text>
                        <Text style={styles.hc_content_right} appearance={'hint'}>{filtered[modalIndex].email}</Text>
                      </View>

                      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Text style={styles.hc_content_left}>Symptoms </Text>
                        <Text style={styles.hc_content_right} appearance={'hint'}>{( (filtered[modalIndex].symptoms).length <= 1 && ( (filtered[modalIndex].symptoms)[0] == "" ) ) ? "N.A":(filtered[modalIndex].symptoms).map((it)=>{return (<Text>{it}, </Text>)})}</Text>
                      </View>

                      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Text style={styles.hc_content_left}>Address</Text>
                        <Text style={styles.hc_content_right} appearance={'hint'}>{filtered[modalIndex].address}</Text>
                      </View>

                      <Divider style={{marginVertical: 10}}/>
                    </Card>
                  </View>: null
                }
                <Text></Text>
                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={() => setModalVisible(!modalVisible)}
                >
                  <Text style={styles.textStyle}>Close</Text>
                </Pressable>
              </View>
            </View>
          </Modal>

          <Divider/>

          { filtered.length > 0 ?
          <FlatList
            data={filtered}
            renderItem={({item, index})=>{
              if(type == 'lab'){
                return renderLab(item)
              }else if(type == 'drug'){
                return renderDrugs(item)
              }else if(type == 'ambulance'){
                return renderEmergency(item)
              }else{
                return renderHC(item, index)
              }
            }}
            keyExtractor={item => item.id}
            />
            :
            <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              {
                loaded ? 
                <Text appearance={'hint'} category={'h6'}>No requests to show.</Text>
                :
                <ActivityIndicator />
              }
              
            </Layout>
            }
        </SafeAreaView>
      </Layout>
  );
}

const styles = StyleSheet.create({
  hc_header_left: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  hc_header_right: {
    fontSize: 11,
    fontWeight: '800',
    opacity: 0.7
  },
  hc_content_left: {
    fontSize: 13,
    color: 'rgba(0, 0, 0, 0.6)',
    marginTop: 4

  },
  hc_content_right: {
    fontSize: 11,
  },
  hc_vd: {
    fontSize: 13,
    color: 'darkgreen',
    opacity: 0.8,
    padding: 5
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 4,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 4,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "black",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});

export default History