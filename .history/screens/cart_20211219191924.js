import React, { useState } from 'react';
import { Layout, Text, Icon, Button, Divider, CheckBox, Input, Datepicker } from '@ui-kitten/components';
import { ScrollView, View, Image, SafeAreaView } from 'react-native';

const BackIcon = (props) => (
    <Icon {...props} name='arrow-ios-back-outline'/>
);

const Cart = ({navigation, route}) => {

  const { total, cart, user, prescription } = route.params
  const [pickup, setpickup] = useState(false)
  const [date, setdate] = useState(new Date())
  const [location, setlocation] = useState('')

  console.log(prescription)

  const checkout = ()=>{
    navigation.replace('Payment', {
      item: {
          netTotal: ['Basic', 'Premium', 'Family'].includes(user.subscription) ? total : total+5,
          total: ['Basic', 'Premium', 'Family'].includes(user.subscription) ? total : total+5,
          totalDiscount: 0,
      },
      data: {
        order: cart, 
        prescription: 'not required', 
        deliveryOption: pickup ? 'Pickup' : 'Delivery', 
        deliveryLocation: location, 
        deliveryTime: date, 
        isDirectDoctorPrescription: prescription ? true : false, 
        serviceId: 'orders'
      },
      type: 'drug'
    })
  }

  return (
    <Layout style={{flex: 1}}>
      <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
          <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} accessoryLeft={BackIcon}/>
          <Text category={'h4'} style={{margin: 15}}>GHS {total.toFixed(2)}</Text>
        </View>
        <Divider/>
        <ScrollView style={{flex: 1, padding: 15, paddingBottom: 30}} showsVerticalScrollIndicator={false}>
          { cart.map(d=>(<Layout style={{flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 10}}>
            <View>
              <Text category="s1">{d.drug.name}</Text>
              <Text appearance={'hint'}>Unit Price: GHS {d.drug.unitprice.toFixed(2)}</Text>
            </View>
            <Text>x {d.quantity}</Text>
          </Layout>)) }
        </ScrollView>

        { !pickup ?
        <>
          <Input style={{marginHorizontal: 15, marginTop: 15}} value={location} onChangeText={setlocation} placeholder="Where should we deliver to?" label={'Delivery Address'}/>
          <Datepicker style={{marginTop: 15, marginHorizontal: 15}} label={'When should we deliver?'} date={date} min={new Date()} onSelect={setdate}/>
        </> : <></> }
        <CheckBox onChange={setpickup} checked={pickup} style={{margin: 15}}>I will collect the order myself</CheckBox>
        <Button onPress={checkout} style={{marginHorizontal: 15}}>Proceed</Button>
      </SafeAreaView>
    </Layout>
  );
}

export default Cart