import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity } from 'react-native'
// import {Picker} from '@react-native-picker/picker';
// import RNPickerSelect from 'react-native-picker-select';

export default function urgent_selector({navigation, route}) {

    const [selectedDoctor, setselectedDoctor] = useState('');

    const doctors = route.params.doctors;
    console.log(doctors)
    return (
        <View style={styles.container}>
            <Image source={require('../assets/icon.png')} resizeMode={'contain'} style={{height: 150, width: 150, borderRadius: 10, marginBottom: 50}}/>
            <Text>Select Doctor To Play call to</Text>
            <FlatList
                data={doctors}
                renderItem={(item)=>{
                    console.log(item)
                    return (
                        <View style={{margin: 10}}>
                            <TouchableOpacity onPress={()=>{
                                setselectedDoctor(itemValue);
                                navigation.navigate('Urgent_Main', {email: item.email});
                            }}>
                                <Text style={{borderBottomColor: 'rgba(0, 0, 0, 0.4)', borderBottomWidth: 1, borderRadius: 9}}>{`${item.firstname} ${item.lastname}`}</Text>
                            </TouchableOpacity>
                        </View>
                    )
                }}
                />
            {/* <Picker
                selectedValue={selectedDoctor}
                onValueChange={(itemValue, itemIndex) =>{
                    setselectedDoctor(itemValue);
                    navigation.navigate('Urgent_Main', {email: itemValue});}
                }>
                    {
                        doctors.map(item=>{
                            return <Picker.Item label={`${item.firstname} ${item.lastname}`} value={item.email} />
                        })
                    }
                
            </Picker> */}
            
            {/* <RNPickerSelect
                onValueChange={(itemValue) => {
                    setselectedDoctor(itemValue);
                    navigation.navigate('Urgent_Main', {email: itemValue});}
                }
                value={selectedDoctor}
                items={()=>{
                    var a = []
                    var i = 0;
                    doctors.map(item=>{
                        a[i] = {
                                label: `${item.firstname} ${item.lastname}`,
                                value: item.email
                            }
                        i++;
                    })
                    return a;
                }}
            /> */}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    }
})
