import React, {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { View, Image, Dimensions, StyleSheet, ScrollView, TouchableOpacity, Touchable, KeyboardAvoidingView, Platform, Alert, SafeAreaView } from 'react-native'
import { Layout, Text, Card, Icon, Button, List, ListItem, Input } from '@ui-kitten/components';
import { ActivityIndicator, useTheme } from 'react-native-paper';
import { PreferencesContext } from '../context';
import { initPayment, verOtp, cardPayment } from '../api/paystack';
import { colors } from '../utils/constants';
import { facilityLabRequest, individualLabRequest } from '../api/lab';
import { requestHomeCare } from '../api/homecare';
import { buyDrugs } from '../api/pharmacy';


const phoneIcon = (props) => (
    <Icon {...props} name='smartphone-outline' fill={'grey'}/>
);

const cardIcon = (props) => (
  <Icon {...props} name='credit-card-outline' fill={'grey'}/>
);
const calendarIcon = (props) => (
  <Icon {...props} name='calendar-outline' fill={'grey'}/>
);
const securityIcon = (props) => (
  <Icon {...props} name='shield-outline' fill={'grey'}/>
);

const DownIcon = (props) => (
  <Icon {...props} name='edit-2-outline'/>
);

const BackIcon = (props) => (
  <Icon {...props} name='chevron-left-outline'/>
);

const Payment = ({route, navigation}) =>{
  // console.log(route.params.data)
    const {item, type} = route.params
    const [user, setUser] = useState()
    const [card, setCard] = useState({
        number: '',
        month: '',
        year: '',
        cvv: '',
        pin: ''
    })
    const [method, setMethod] = useState('Mobile Money')
    const [price, setPrice] = useState()
    const [phone, setPhone] = useState('')
    const [loading, setLoading] = useState(false)
    const [showotp_field, setshowotp_field] = useState(false)
    const [otperror, setotperror] = useState(false)
    const [otp, setotp] = useState('')
    const [tnx_ref, settnx_ref] = useState('')
    const [button, setButton] = useState('Pay Now')
    const [reference, setReference] = useState()
    const [status, setStatus] = useState()
    const [phone_error, setPhoneError] = useState()
    const [card_error, setCardError] = useState()
    const mtns = ['024', '054', '055', '059']
    const tigos = ['027', '057', '026', '056']
    const vods = ['020', '050']

    const changeMethod = () => {
        Alert.alert('Change Payment Method', 'Please select your preferred payment method', [
        {
            text: 'Mobile Money',
            onPress: ()=>setMethod('Mobile Money')
        },
        {
            text: 'Visa/Mastercard',
            onPress: ()=>setMethod('Card')
        },
        {
          text: 'Insurance',
          onPress: ()=>setMethod('Insurance')
        },
        {
            text: 'Dismiss',
            onPress: ()=>{},
            style: 'destructive'
        }
        ])
    }

    const process_otp = async()=>{
      setLoading(true)
      setButton('Initializing Verification for Otp...')

      let init = await verOtp(tnx_ref, otp);
      // start
      if(init.status){

        if(init.data.status == 'send_otp'){
            setLoading(false)
            setshowotp_field(true)
            settnx_ref(init.data.reference)
            setButton('Pay Now')
        }else if(init.data.status == 'pay_offline'){
            setLoading(false)
            setButton('Awaiting for payment confirmation...')
        }else if(init.data.status == 'success'){

          if(type == 'lab'){
            if(route.params.data.type == 'facility'){
              await facilityLabRequest(route.params.data)
            }else{
              await individualLabRequest(route.params.data)
            }
          }else if(type == 'homecare'){
            await requestHomeCare(route.params.data)
          }else if(type == 'drug'){
            await buyDrugs(route.params.data)
          }

          setLoading(false)
          setButton('Done')
          navigation.pop()
          navigation.replace('PaymentResult', {purpose: type == 'drug' ? 'Pharmacy Order' : type == 'lab' ? 'Laboratory Test(s)' : 'Home Care Service', amount: item.netTotal, reference: init.data.reference, date: init.data.transaction_date})
        }else{
            setLoading(false)
            setButton(init.data.gateway_response)
        }

      }else{
        setLoading(false)
        setButton('Pay Now')
        setPhoneError(init.data.message)
        return
      }
      // stop
    }

    // console.log(method)

    const subscribe = async () => {

        if(method == 'Mobile Money'){

        let network

        if(phone.length < 10){
            setPhoneError('Please enter a valid number')
            return
        }

        mtns.includes(phone.substr(0, 3)) ? network = 'mtn'
        : tigos.includes(phone.substr(0, 3)) ? network = 'tgo'
        : vods.includes(phone.substr(0, 3)) ? network = 'vod'
        : network = null

        if(network == null){
            setPhoneError('Please enter a Ghanaian number')
            return
        }

        try{

            setLoading(true)
            setButton('Initializing Transaction...')
            let init = await initPayment(item.netTotal*1, phone, network)
            
            if(init.status){

              if(init.data.status == 'send_otp'){
                  setLoading(false)
                  setshowotp_field(true)
                  settnx_ref(init.data.reference)
                  setButton('Pay Now')
              }else if(init.data.status == 'pay_offline'){
                  setLoading(false)
                  setButton('Awaiting for payment confirmation...')
              }else if(init.data.status == 'success'){

                if(type == 'lab'){
                  if(route.params.data.type == 'facility'){
                    await facilityLabRequest(route.params.data)
                  }else{
                    await individualLabRequest(route.params.data)
                  }
                }else if(type == 'homecare'){
                  await requestHomeCare(route.params.data)
                }else if(type == 'drug'){
                  await buyDrugs(route.params.data)
                }

                setLoading(false)
                setButton('Done')
                navigation.pop()
                navigation.replace('PaymentResult', {purpose: type == 'drug' ? 'Pharmacy Order' : type == 'lab' ? 'Laboratory Test(s)' : 'Home Care Service', amount: item.netTotal, reference: init.data.reference, date: init.data.transaction_date})
              }else{
                  setLoading(false)
                  setButton(init.data.gateway_response)
              }

            }else{
              setLoading(false)
              setButton('Pay Now')
              setPhoneError(init.data.message)
              return
            }

        }catch(e){
          console.log(e.response.data)
            setLoading(false)
            setButton('Pay Now')
            setPhoneError(e.message)
            return
        }
      }else if(method == 'Card'){

        if(card.number.length < 10){
            setCardError('Please enter a valid card number')
            return
        }

        if(card.month.length < 2){
            setCardError('Please enter a valid expiry month')
            return
        }

        if(card.month*1 > 12){
            setCardError('Please enter a valid expiry month')
            return
        }

        if(card.year.length < 2){
            setCardError('Please enter a valid expiry year')
            return
        }

        if(card.year*1 < 21){
            setCardError('Please enter a valid expiry year')
            return
        }

        if(card.cvv.length < 3){
            setCardError('Please enter a valid card cvv')
            return
        }

        setLoading(true)
        setButton('Initializing Transaction...')
        let init = await cardPayment(card, item.netTotal*1);

        console.log(card)

        // start
        if(init.status){

          if(init.data.status == 'send_otp'){
              setLoading(false)
              setshowotp_field(true)
              settnx_ref(init.data.reference)
              setButton('Pay Now')
          }else if(init.data.status == 'pay_offline'){
              setLoading(false)
              setButton('Awaiting for payment confirmation...')
          }else if(init.data.status == 'success'){

            if(type == 'lab'){
              if(route.params.data.type == 'facility'){
                await facilityLabRequest(route.params.data)
              }else{
                await individualLabRequest(route.params.data)
              }
            }else if(type == 'homecare'){
              await requestHomeCare(route.params.data)
            }else if(type == 'drug'){
              await buyDrugs(route.params.data)
            }

            setLoading(false)
            setButton('Done')
            navigation.pop()
            navigation.replace('PaymentResult', {purpose: type == 'drug' ? 'Pharmacy Order' : type == 'lab' ? 'Laboratory Test(s)' : 'Home Care Service', amount: item.netTotal, reference: init.data.reference, date: init.data.transaction_date})
          }else{
              setLoading(false)
              setButton(init.data.gateway_response)
          }

        }else{
          setLoading(false)
          setButton('Pay Now')
          setPhoneError(init.data.message)
          Alert.alert(init.data.message)
          return
        }
        // stop

      }else{
        setLoading(false)
        setButton('payment gateway coming soon')
      }

    }

    useEffect(()=>{

        (async()=>{
        let account = await AsyncStorage.getItem('user')
        setPrice(0)
        setUser(JSON.parse(account))
        })()

    }, [])

    return (
      <Layout style={{ flex: 1}}>
        <View style={{backgroundColor: colors.primary, width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/3, position: 'absolute', top: 0 }}/>
        
        <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
            <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='control' accessoryLeft={BackIcon}/>
            <Text category={'h4'} style={{color: 'white', margin: 15}}>Make Payment</Text>
          </View>

          <Layout style={{margin: 15, flex: 1, borderRadius: 5, borderColor: '#F2F2F2', borderWidth: 1, padding: 25}}>
            <ScrollView style={{flex: 1}} shouldRasterizeIOS showsVerticalScrollIndicator={false}>


              <Text category={'h1'} style={{alignSelf: 'center', marginTop: 25}}>GHS {(item.netTotal*1).toFixed(2)}</Text>
              <Text category={'h6'} style={{alignSelf: 'center', marginTop: 15, marginBottom: 25}} appearance={'hint'}>{type == 'drug' ? 'Pharmacy' : type == 'lab' ? 'Lab Tests' : 'Home Care'}</Text>

              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 25}}>
                <Text category={'p1'} appearance={'default'}>Order Payment For</Text>
                <Text category={'p1'} appearance={'hint'}>{type == 'drug' ? 'Pharmacy' : type == 'lab' ? 'Lab Tests' : 'Home Care'}</Text>
              </View>

              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 25, marginTop: 15}}>
                <Text category={'p1'} appearance={'default'}>Payment Method</Text>
                <Button onPress={changeMethod} appearance={'outline'} size='tiny' accessoryRight={DownIcon}>
                  {method}
                </Button>
              </View>

              <Text category={'h5'} style={{alignSelf: 'center', marginTop: 15}} appearance={'hint'}>Payment Overview</Text>

              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 25}}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Text category={'p1'}>Total</Text>
                  <Text category={'p1'} appearance={'hint'}>GHS {(item.total*1).toFixed(2)}</Text>
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Text category={'p1'}>Discount</Text>
                  <Text category={'p1'} appearance={'hint'}>GHS {(item.totalDiscount*1).toFixed(2)}</Text>
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Text category={'p1'}>Net Total</Text>
                  <Text category={'p1'}>GHS {(item.netTotal*1).toFixed(2)}</Text>
                </View>

              </View>

            {method == 'Mobile Money' ?
              <View>
                <Input accessoryLeft={phoneIcon} value={phone} label={phone_error ? phone_error : ''} status={phone_error ? 'danger' : 'basic'} onChangeText={(text)=>{setPhoneError(null); setPhone(text)}} placeholder={'MoMo Number (e.g 0500000000)'} keyboardType={'phone-pad'} autoCompleteType={'tel'} maxLength={10} shouldRasterizeIOS returnKeyType={'done'} />
              </View> : 
              <></>
            }
            
            {method == 'Card' ?
              <View>
                <Input accessoryLeft={cardIcon} value={card.number} label={card_error ? card_error : ''} status={card_error ? 'danger' : 'basic'} onChangeText={(text)=>{setCardError(null); setCard({...card, ...{number: text}})}} placeholder={'Card Number'} keyboardType={'number-pad'} autoCompleteType={'cc-number'} maxLength={19} shouldRasterizeIOS returnKeyType={'done'} />
                <View style={{flexDirection: 'row', marginVertical: 15}}>
                  <Input accessoryLeft={calendarIcon} label={'Expiry Month'} value={card.month} status={card_error ? 'danger' : 'basic'} style={{flex: 1, marginEnd: 15}} onChangeText={(text)=>{setCardError(null); setCard({...card, ...{month: text}})}} placeholder={'MM'} keyboardType={'number-pad'} autoCompleteType={'cc-exp-month'} maxLength={2} shouldRasterizeIOS returnKeyType={'done'} />
                  <Input accessoryLeft={calendarIcon} label={'Expiry Year'} value={card.year} status={card_error ? 'danger' : 'basic'} style={{flex: 1}} onChangeText={(text)=>{setCardError(null); setCard({...card, ...{year: text}})}} placeholder={'YY'} keyboardType={'number-pad'} autoCompleteType={'cc-exp-year'} maxLength={2} shouldRasterizeIOS returnKeyType={'done'} />
                </View>
                <Input accessoryLeft={securityIcon} value={card.cvv} status={card_error ? 'danger' : 'basic'} onChangeText={(text)=>{setCardError(null); setCard({...card, ...{cvv: text}})}} placeholder={'CVV/CSC'} keyboardType={'number-pad'} autoCompleteType={'cc-csc'} maxLength={3} shouldRasterizeIOS returnKeyType={'done'} />
                <Input style={{marginTop: 15}} accessoryLeft={securityIcon} value={card.pin} status={card_error ? 'danger' : 'basic'} onChangeText={(text)=>{setCardError(null); setCard({...card, ...{pin: text}})}} placeholder={'Pin'} keyboardType={'number-pad'} maxLength={4} shouldRasterizeIOS returnKeyType={'done'} secureTextEntry={true} />
              </View>:<></>
            }
            {method == 'Insurance' ?
              <View>
                <Text style={{textAlign: 'center'}}>Gateway Coming Soon</Text>
              </View>:<></>
            }
              
              {
                (showotp_field && <Text />)
              }
              {
                (showotp_field && <Text>An Otp was sent</Text>)
              }
              {
                (showotp_field && <Input accessoryLeft={securityIcon} value={otp} status={otperror ? 'danger' : 'basic'} onChangeText={(text)=>{setotperror(null); setotp(text)}} placeholder={'Enter Otp'} keyboardType={'number-pad'} shouldRasterizeIOS />)
              }
              {
                (showotp_field && <Button disabled={loading} onPress={process_otp} appearance={'outline'} style={{textAlign: 'center', marginTop: 25}}>Submit Otp</Button> )
              }

              <Button disabled={loading} onPress={subscribe} appearance={'outline'} style={{textAlign: 'center', marginTop: 25}}>
                {button}
              </Button>              
              <Text category={'s2'} appearance={'hint'} style={{marginBottom: 15, marginTop: 5, alignSelf: 'center'}}>Payments processed by PayStack</Text>

              <TouchableOpacity style={{height: 40}}>
                <Text category={'s1'} style={{alignSelf: 'center'}} status={'info'}>Report a problem</Text>
              </TouchableOpacity>
            </ScrollView>
          </Layout> 
        </SafeAreaView>
    </Layout>
  );
}

export default Payment

const style = StyleSheet.create({
  container: {
    // maxHeight: 192,
  },
});