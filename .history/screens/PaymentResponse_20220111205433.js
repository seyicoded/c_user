import React, {useEffect, useState} from 'react';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import * as AsyncStorage from '../AsyncStorageCustom'

import { View, Image, Dimensions, StyleSheet, ScrollView, TouchableOpacity, Touchable, KeyboardAvoidingView, Platform, Alert, ImageBackground, SafeAreaView } from 'react-native'
import { Layout, Text, Card, Icon, Button, List, ListItem, Input } from '@ui-kitten/components';
import { ActivityIndicator, useTheme } from 'react-native-paper';
import { PreferencesContext } from '../context';
import { initPayment } from '../api/paystack';


const ForwardIcon = (props) => (
    <Icon {...props} name='arrow-ios-forward-outline' fill={'grey'}/>
);

const DownIcon = (props) => (
  <Icon {...props} name='edit-2-outline'/>
);

const BackIcon = (props) => (
  <Icon {...props} name='chevron-left-outline'/>
);

const PaymentResponse = ({route, navigation}) =>{

  const [user, setUser] = useState()

  useEffect(()=>{

    (async()=>{
      let account = await AsyncStorage.getItem('user')
      setUser(JSON.parse(account))
    })()

  }, [])

    return (
      <Layout style={{ flex: 1}}>
                        
        <ImageBackground style={{flex: 1}} source={require('../assets/images/success_bg.png')} resizeMode={'cover'}>
          <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
            <Layout style={{flex: 1, marginVertical: 50, marginHorizontal: 25, borderRadius: 10, justifyContent: 'center', alignItems: 'center'}}>
              <Image source={require('../assets/images/payment_success.png')} style={{margin: 15, height: 150, width: 100, alignSelf: 'center'}} resizeMode='contain' />
              
              <Text category='s1'>You have successfully</Text>
              <Text category='h6' style={{marginTop: 5}}>paid for {route.params.purpose.toLowerCase()}</Text>


              <Text category='h1' style={{marginVertical: 35}}>GHS {route.params.amount}</Text>

              { user ?
              <View style={{flexDirection: 'row', backgroundColor: "rgba(224, 255, 252, 0.4)", width: '70%', borderRadius: 100, alignItems: 'center', padding: 10}}>

                  <Image source={{uri: user.avatar}} style={{height: 35, width: 35, borderRadius: 35}} resizeMode='contain' />
                  <Text category={'s1'} appearance={'hint'} style={{marginStart: 15, fontSize: 20}}>{user.firstname} {user.lastname}</Text>

              </View> : null }

              <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 25, marginBottom: 5}}>
                  <Text category='s1'>Transaction done on </Text>
                  <Text category='h6'> {new Date(route.params.date).toString().substr(0,16)}</Text>
              </View>
              <Text category='s1' appearance={'hint'} style={{textAlign: 'center'}}>Your trasaction reference is {'\n'+route.params.reference}</Text>

              <Button onPress={()=>{ try{ navigation.goBack(); } catch(e){} navigation.replace('Main')}} style={{width: '80%', margin: 25}}>
                  Done
              </Button>
            </Layout>
          </SafeAreaView>
        </ImageBackground>

      </Layout>
    );
}

export default PaymentResponse

const style = StyleSheet.create({
  container: {
    // maxHeight: 192,
  },
});