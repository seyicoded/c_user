import React, {useEffect, useState} from 'react';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import { View, Image, Dimensions, StyleSheet, ScrollView, TouchableOpacity, SafeAreaView } from 'react-native'
import { Layout, Text, Card, Icon, Button, List, ListItem } from '@ui-kitten/components';
import { ActivityIndicator, useTheme } from 'react-native-paper';
import { FlatList } from 'react-native-gesture-handler';

const ForwardIcon = (props) => (
    <Icon {...props} name='arrow-ios-forward-outline' fill={'grey'}/>
);

const BellIcon = (props) => (
  <Icon {...props} name='bell-outline'/>
);

const GiftIcon = (props) => (
    <Icon {...props} name='gift-outline' style={{width: 32, height: 32}}/>
);

const History = ({route, navigation}) =>{

    return (
        <Layout style={{ flex: 1}}>
          <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
            <View style={{
              width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/3, position: 'absolute', top: 0 }}/>
            
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <Text category={'h4'} style={{margin: 15}}>Account History</Text>
              <Button onPress={()=>navigation.navigate('Notifications')} appearance='outline' style={{ width: 50, margin: 15}} accessoryLeft={BellIcon}/>
            </View>

            
            <FlatList
              data={[
                {
                  key: 'drug',
                  name: 'Pharmacy Orders',
                  image: require('../../assets/images/drug.png')
                },
                {
                  key: 'lab',
                  name: 'Laboratory Requests',
                  image: require('../../assets/images/chemistry.png')
                },
                {
                  key: 'ambulance',
                  name: 'Ambulance Requests',
                  image: require('../../assets/images/ambulance.png')
                },
                {
                  key: 'homecare',
                  name: 'Home Care Requests',
                  image: require('../../assets/images/homecare.png')
                }
              ]}
              numColumns={2}
              keyExtractor={item=>item.key}
              renderItem={({item})=>{
                return (
                  <Card onPress={()=>navigation.navigate('History', {type: item.key})} style={{width: Dimensions.get('window').width/2 - 35, marginHorizontal: 10, marginTop: 15, justifyContent: 'center', alignItems: 'center'}}>
                    <Image source={item.image} style={{width: 50, height: 50, alignSelf: 'center'}}/>
                    <Text category={'s1'} style={{marginTop: 10, textAlign: 'center'}}>{item.name}</Text>
                  </Card>
                )
              }}
              style={{
                // backgroundColor: 'white',
                marginHorizontal: 15,
                borderRadius: 5
              }}
            />
          </SafeAreaView>
        </Layout>
    );
}

export default History

const style = StyleSheet.create({
  container: {
    // maxHeight: 192,
  },
});