import React, {useEffect, useState} from 'react';
import { Layout, Text, Button, Icon, Spinner } from '@ui-kitten/components';
import { mediumPosts } from '../../api/blog';
import { ActivityIndicator, FlatList, Image, Linking, TouchableOpacity, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

const BellIcon = (props) => (
  <Icon {...props} name='bell-outline'/>
);

const Blog = ({navigation}) =>{

  const [articles, setArticles] = useState([])
  const [loading, setloading] = useState(true)

  useEffect(()=>{

    (async()=>{
      try{
        let response = await mediumPosts()
        setArticles(response)
        setloading(false)
      }catch(e){
        setloading(false)
      }
    })()

  }, [])

  return (
    <Layout style={{ flex: 1}}>
      <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>

      <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
        <Text category={'h4'} style={{margin: 15}}>Our Articles</Text>
        <Button onPress={()=>navigation.navigate('Notifications')} appearance='outline' style={{ width: 50, margin: 15}} accessoryLeft={BellIcon}/>
      </View>

      { !loading ?
       articles.length > 0 ?
        <FlatList
          removeClippedSubviews={true}
          showsVerticalScrollIndicator={false}
          data={articles}
          renderItem={({ item, index, separators }) => {
              return (
                <TouchableOpacity onPress={()=>{
                    console.log(item)
                    navigation.navigate('Web', {title: item.title, url: item.content})
                  }}>
                  <Layout level={'3'} style={{borderRadius: 5, marginHorizontal: 10, marginVertical: 5}}>
                    <Image source={{uri: item.thumbnail}} style={{height: 100, width: '100%', borderTopLeftRadius: 5, borderTopRightRadius: 5}} />
                    <View style={{padding: 10}}>
                      <Text category={'h6'}>{item.title}</Text>
                      <View style={{flexDirection: 'row', marginTop: 5}}>
                        {
                          item.categories.map((cat, index)=>{
                            return index < 4 ?
                            <Button size={'tiny'} status={'basic'} appearance={'outline'} style={{marginEnd: 5}}>{cat}</Button> : null
                          })
                        }
                      </View>
                    </View>
                  </Layout>
                </TouchableOpacity>
              )
          }}/>
          :
          <Layout style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Text category={'h5'} appearance={'hint'} style={{marginTop: 10}}>No articles to display.</Text>
          </Layout>
          :
          <Layout style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Spinner/>
          </Layout>
          }
      </SafeAreaView>
    </Layout>
  );
}

export default Blog