import React, {useEffect, useState} from 'react';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import * as AsyncStorage from '../../AsyncStorageCustom'

import { View, Image, Dimensions, StyleSheet, ScrollView, TouchableOpacity, SafeAreaView, Share } from 'react-native'
import { Layout, Text, Spinner, Icon, Button, List, ListItem, Card } from '@ui-kitten/components';
import { colors } from '../../utils/constants';

const ForwardIcon = (props) => (
    <Icon {...props} name='arrow-ios-forward-outline' fill={'grey'}/>
);

const BellIcon = (props) => (
  <Icon {...props} name='bell-outline'/>
);

const GiftIcon = (props) => (
    <Icon {...props} name='gift-outline'/>
);

const Profile = ({route, navigation}) =>{

    const [user, setUser] = useState()
    const [list, setlist] = useState([
      {
        title: 'Account',
        icon: 'person-outline',
        description: 'Manage my account',
      },
      {
        title: 'Appointments',
        icon: 'calendar-outline',
        description: 'Manage appointments',
      },
      {
        title: 'Saved Doctors',
        icon: 'heart-outline',
        description: 'Doctors you have favorited',
      },
      {
        title: 'Subscription',
        icon: 'credit-card-outline',
        description: 'Renew or cancel subscription',
      },
      {
        title: 'Settings',
        icon: 'settings-outline',
        description: 'Customize your app experience',
      },
      {
        title: 'Share App',
        icon: 'share-outline',
        description: 'Share link with friends and family',
      },
      {
        title: 'Sign Out',
        icon: 'log-out-outline',
        description: 'Close active session',
      }
    ])

    const renderItemAccessory = (props) => (
      <Button accessoryLeft={ForwardIcon} size='small' appearance={'ghost'}/>
    );
    
    const renderItem = ({ item, index }) => (
      <ListItem
        title={`${item.title}`}
        description={`${item.description}`}
        accessoryLeft={ (props) => {
          return (
            <Icon {...props} name={item.icon}/>
          );
        }}
        onPress={async()=>{
          if(item.title == 'Sign Out'){
            await AsyncStorage.removeItem('email')
            await AsyncStorage.removeItem('access-token')
            await AsyncStorage.removeItem('api-key')
            await AsyncStorage.removeItem('login-expiry')
            await AsyncStorage.removeItem('user')
            navigation.replace('Login')
          }else if(item.title == 'Share App'){
            Share.share({
              message: 'Hey! Join me on this awesome health care app and cut on trips to the hospital. http://onelink.to/clarondoc',
              url: 'http://onelink.to/clarondoc',
              title: 'ClaronDoc Patient'
            }, {
              dialogTitle: 'Share ClaronDoc'
            })
          }else{
            navigation.navigate(item.title, {subscribed: user.subscription != 'Family' && user.subscription != 'Normal' && user.subscription != null})
          }
        }}
        accessoryRight={renderItemAccessory}
      />
    );

    useEffect(()=>{

      (async()=>{
        

        try{
          let account = await AsyncStorage.getItem('user')
          setUser(JSON.parse(account))
          let plan = JSON.parse(account).subscription
          let end = new Date(JSON.parse(account).subscription_end.substring(0, 10)).toString().substring(0, 16)

          let update = list
          
          update.map(item=>{
            item.title == 'Subscription' ? item.description = `${plan} ending on ${end}` : null
          })

          console.log(update)
        }catch(e){}
      })()

    }, [])

    return (
        <Layout style={{ flex: 1, justifyContent: 'center'}}>
            <View style={{backgroundColor: colors.primary, width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/3, position: 'absolute', top: 0 }}/>
            
            <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Text category={'h4'} style={{color: 'white', margin: 15}}>Profile</Text>
                <Button onPress={()=>navigation.navigate('Notifications')} appearance='ghost' style={{ width: 50, margin: 15}} status='control' accessoryLeft={BellIcon}/>
              </View>

              { user ?
              <View shouldRasterizeIOS>
                  <Card disabled style={{margin: 15, marginBottom: 50}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Image source={{uri: user.avatar}} style={{width: 70, height: 70, borderRadius: 70}} resizeMode={'cover'}/>
                      <View style={{marginStart: 15}}>
                        <Text category={'h6'}>{user.firstname} {user.lastname}</Text>
                        <Text category={'s1'} appearance={'hint'} style={{marginTop:5}}>{user.age} yrs old</Text>
                      </View>
                    </View>

                    {user.subscription != 'Family' && user.subscription != 'Normal' && user.subscription != null ?
                    <Button onPress={()=>navigation.navigate('Subscription')} accessoryLeft={GiftIcon} appearance={'outline'} style={{textAlign: 'center', marginVertical: 25}}>
                      Upgrade Subscription
                    </Button> : <></> }

                    <List
                      style={{
                        height: Dimensions.get('screen').height*0.45
                      }}
                      data={list}
                      renderItem={renderItem}
                    />
                    
                  </Card>                
              </View>
              :
              <Layout style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Spinner size={'large'} status={'primary'}/>
              </Layout> }
            </SafeAreaView>
        </Layout>
    );
}

export default Profile

const style = StyleSheet.create({
  container: {
    // maxHeight: 192,
  },
});