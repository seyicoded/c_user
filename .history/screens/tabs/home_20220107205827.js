import React, { useEffect, useState, useRef } from 'react';
import { StyleSheet, View, Image, ScrollView, TouchableOpacity, SafeAreaView, Platform, Keyboard, ActivityIndicator } from 'react-native'
import { Layout, Text, Button, Icon, Card, Input } from '@ui-kitten/components';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { MenuItem, OnlineDoctor } from '../../utils/components';
import { FlatList } from 'react-native-gesture-handler';
import { fetchDoctors } from '../../api/doctors';
import { userDetails, downgrade } from '../../api/auth';
import { colors } from '../../utils/constants';
import moment from 'moment';

const BellIcon = (props) => (
    <Icon {...props} name='bell-outline'/>
);

const CancelIcon = ({setisSearching, setsearch_txt}) => {
    // setisSearching={setisSearching} setsearch_txt={setsearch_txt}
    setisSearching(false);
    setsearch_txt('')
    Keyboard.dismiss()
    return (
        <Icon name='bell-outline'/>
    )
};

const main = [
    {
        name: 'Subscribe',
        screen: 'Subscription',
        icon: require('../../assets/images/subscription.png')
    },
    {
        name: 'Urgent Care',
        screen: 'Urgent',
        icon: require('../../assets/images/urgent_care.png')
    },
    {
        name: 'Emergency',
        screen: 'Ambulance',
        icon: require('../../assets/images/ambulance.png')
    },
    {
        name: 'Consultation',
        screen: 'Doctors',
        icon: require('../../assets/images/doctor.png')
    },
    {
        name: 'Pharmacy',
        screen: 'Pharmacy',
        icon: require('../../assets/images/pharmacy_.png')
    },
    {
        name: 'Laboratory',
        screen: 'Laboratory',
        icon: require('../../assets/images/lab.png')
    },
    {
        name: 'Home Care',
        screen: 'Emergency',
        icon: require('../../assets/images/homecare.png')
    }
]

const secondary = [
    {
        name: 'Urgent Care',
        screen: 'Urgent',
        icon: require('../../assets/images/urgent_care.png')
    },
    {
        name: 'Emergency',
        screen: 'Ambulance',
        icon: require('../../assets/images/ambulance.png')
    },
    {
        name: 'Consultation',
        screen: 'Doctors',
        icon: require('../../assets/images/doctor.png')
    },
    {
        name: 'Pharmacy',
        screen: 'Pharmacy',
        icon: require('../../assets/images/pharmacy_.png')
    },
    {
        name: 'Laboratory',
        screen: 'Laboratory',
        icon: require('../../assets/images/lab.png')
    },
    {
        name: 'Home Care',
        screen: 'Emergency',
        icon: require('../../assets/images/homecare.png')
    },    
    {
        name: 'Upgrade',
        screen: 'Subscription',
        icon: require('../../assets/images/subscription.png')
    }
]

const basic = [
    {
        name: 'Emergency',
        screen: 'Ambulance',
        icon: require('../../assets/images/ambulance.png')
    },
    {
        name: 'Consultation',
        screen: 'Doctors',
        icon: require('../../assets/images/doctor.png')
    },
    {
        name: 'Pharmacy',
        screen: 'Pharmacy',
        icon: require('../../assets/images/pharmacy_.png')
    },
    {
        name: 'Laboratory',
        screen: 'Laboratory',
        icon: require('../../assets/images/lab.png')
    },
    {
        name: 'Home Care',
        screen: 'Emergency',
        icon: require('../../assets/images/homecare.png')
    },    
    {
        name: 'Upgrade',
        screen: 'Subscription',
        icon: require('../../assets/images/subscription.png')
    }
]

const Home = ({navigation}) => {

    const [menu, setMenu] = useState([])
    const [user, setUser] = useState({})
    const [expiry, setexpiry] = useState()
    const [isLoading, setisLoading] = useState(true)
    const [isSearching, setisSearching] = useState(false)
    const [doctors, setDoctors] = useState([])

    // because of search integration
    const [search_txt, setsearch_txt] = useState('');
    const unfilter_menu = useRef(null)
    const unfilter_doctors = useRef(null)

    const checkSubscription = async(date)=>{

        if(date == null){
            return
        }

        const days = moment().diff(date, 'days')
        const left = moment(date).add(1, 'day').fromNow()

        if(days == 0){
            setexpiry('Your subscription ends today. Renew now to avoid losing access to services')
        }else if (days < 0 && days > -11){
            setexpiry(`Your subscription ends in ${left}. Renew now to avoid losing access to services`)
        }else if(days > 0 && days < 10){
            setexpiry(`Your subscription ended ${left}. Renew now to avoid losing access to services`)
        }else if(days > 10){
            let res = await downgrade()
            console.info(res)
            // setexpiry(`Your subscription ended ${left}. Renew now to avoid losing access to services`)
        }
    }

    const entry_for_menu = (data) => {
        unfilter_menu.current = data
    }

    useEffect(()=>{
        
        (async()=>{
            setisLoading(true)
            let account = await AsyncStorage.getItem('user')
            let token = await AsyncStorage.getItem('access-token')
            let key = await AsyncStorage.getItem('api-key');
            setUser(JSON.parse(account))

            let online = await fetchDoctors()
            setDoctors(online.filter(doctor=>doctor.availability == 'Online'))
            unfilter_doctors.current = online;

            userDetails(JSON.parse(account).email, key, token).then(data=>{
                setUser(data)
            }).catch(e=>{
                console.log('Error: ', e)
            })

            try{
                checkSubscription(JSON.parse(account).subscription_end);
            }catch(e){}

            ['Family', 'Premium'].includes(JSON.parse(account).subscription) ?
                setMenu(secondary) :
            ['Basic'].includes(JSON.parse(account).subscription) ? setMenu(basic) : setMenu(main)

            // update menu ref for unfiltered
            ['Family', 'Premium'].includes(JSON.parse(account).subscription) ?
                entry_for_menu(secondary) :
            ['Basic'].includes(JSON.parse(account).subscription) ? entry_for_menu(basic) : entry_for_menu(main)
            
            setisLoading(false)
        })()

    }, [])

    return (
        
        <Layout style={{ flex: 1 }}>
            {
                isLoading && (<View style={{position: 'absolute', zIndex: 99, height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator color={"green"}/>
                </View>)
            }
            <View style={[style.top, {backgroundColor: colors.primary}]}>
                <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0 }}>

                    {
                        (isSearching == true) ? <></>:
                        <View style={{flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                                <TouchableOpacity onPress={()=>navigation.navigate('Profile')}>
                                    <Image source={{uri: user ? user.avatar : ''}} style={{height: 50, width: 50, borderRadius: 75}} resizeMode={'cover'}/>
                                </TouchableOpacity>
                                <Text category={'h6'} style={{color: '#f2f2f2', fontSize: 18, marginStart: 10}}>{user ? user.firstname : ''}</Text>
                            </View>

                            <Button onPress={()=>navigation.navigate('Notifications')} appearance='ghost' style={{ width: 50 }} status='control' accessoryLeft={BellIcon}/>
                            
                        </View>
                    }
                    
                    <Text category={'h4'} style={{marginTop: 15, color: 'white'}}>Find Health Services</Text>
                    <Input 
                        size={'large'}
                        placeholder={'Search...'}
                        placeholderTextColor={'grey'}
                        value={search_txt}
                        onFocus={()=>{
                            setisSearching(true)
                            // navigation.navigate('Search', {menu: menu, user: user, doctors: doctors})
                            // Keyboard.dismiss()
                        }}
                        style={{
                            marginTop: 25,
                            marginBottom: 15
                        }}
                        accessoryRight={(isSearching == true) ? <CancelIcon setisSearching={setisSearching} setsearch_txt={setsearch_txt} />: <></>}/>
                </SafeAreaView>
            </View>

            <ScrollView shouldRasterizeIOS showsVerticalScrollIndicator={false}>
                { expiry ?
                <Card level={'3'} style={{marginTop: 15, marginHorizontal: 15, borderRadius: 8}}>
                    <Text category={'s1'} status={'danger'}>{expiry}</Text>
                    <Button onPress={()=>navigation.navigate('Subscription')} appearance='outline' style={{ marginTop: 10 }}>Renew Subscription</Button>
                </Card>
                : <></> }

                <FlatList
                    data={ menu }
                    renderItem={item=>
                    <MenuItem 
                        option={item}
                        subscribed={'subscription' in user ? user.subscription != 'Normal' && user.subscription != null : false}
                        navigate={()=>navigation.navigate(item.item.screen, {doctors: doctors})}/>}
                        keyExtractor={item=>item.name}
                        showsHorizontalScrollIndicator={false}
                        horizontal
                        style={{marginVertical: 15, marginStart: 10}}
                    />

                {  doctors.length > 0 ?
                <View style={{marginHorizontal: 15, flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{marginStart: 5, height: 15, width: 15, borderRadius: 15, backgroundColor: 'green', marginEnd: 10}}/>
                    <Text category={'h4'}>
                        {
                            (isSearching == true) ? 'Doctors':'Doctors Online'
                        }
                        </Text>
                </View> : <></> }

                <FlatList
                    data={doctors}
                    renderItem={({item, index})=><OnlineDoctor navigate={()=>navigation.navigate('Doctor', {doctor: item})} doctor={item}/>}
                    keyExtractor={item=>item.email}
                    showsHorizontalScrollIndicator={false}
                    horizontal
                    style={{marginBottom: 15, marginStart: 10}}
                    />
            </ScrollView>

        </Layout>
    );
}

export default Home

const style = StyleSheet.create({
    top: {
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        padding: 15
    }
})