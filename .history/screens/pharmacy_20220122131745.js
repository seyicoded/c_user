import React, { useEffect, useState, useRef } from 'react';
import { Layout, Text, Button, Icon, Modal, Card, Input, IndexPath } from '@ui-kitten/components';
import { View, StyleSheet, Image, FlatList, Alert, SafeAreaView, Dimensions, ActivityIndicator } from 'react-native'
import { fetchDrugs } from '../api/pharmacy';
import { Drug } from '../utils/components';
import { colors } from '../utils/constants';
import DocumentPicker from 'react-native-document-picker'
import { RNCamera } from 'react-native-camera';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '../AsyncStorageCustom'

import {RNS3} from 'react-native-aws3';

var bag = {}

const BackIcon = (props) => (
  <Icon {...props} name='arrow-ios-back-outline'/>
);

const CartIcon = (props) => (
  <Icon {...props} name='shopping-cart-outline'/>
);

const PrescriptionIcon = (props)=><Icon {...props} name="clipboard-outline"/>

const options = [
  'Delivery',
  'Pickup'
]

const Pharmacy = ({navigation}) => {

  let camera = useRef()
  const [drugs, setDrugs] = useState([])
  const [filtered, setFiltered] = useState([])
  const [cart, setCart] = useState([])
  const [user, setuser] = useState()
  const [total, setTotal] = useState(0)
  const [capture, setcapture] = useState(false)
  const [prescription, setprescription] = useState()
  const [prescriptiona, setprescriptiona] = useState(false)
  const [loadinga, setloadinga] = useState(false)
  const [loadingaaa, setloadingaaa] = useState(true)

  const search = (query) => {
    setFiltered(drugs.filter(drug=>
      drug.name.toLowerCase().includes(query.toLowerCase()) ||
      drug.description.toLowerCase().includes(query.toLowerCase())
    ))
  }
  
  const fromDevice = async()=>{
    try{
      let document = await DocumentPicker.pick({
          type: [DocumentPicker.types.images],
          allowMultiSelection: false,
      })

      let attached_files = []

      document.map(attached=>{
          if(attached.size/1024/1024 > 25){
              seterror(`${attached.name} cannot be attached. Exceed the maximum upload size (25MB).`)
          }else{
              attached_files.push(attached)
          }
      })
      setprescription(attached_files[0])
      setprescriptiona(true)
    }catch(e){
        console.log(e)
    }
  }

  const fromCamera = async()=>{
    setcapture(true)
  }

  const upload = async()=>{
    setloadinga(true)

    const options = {
      keyPrefix: '/', // Ex. myuploads/ arn:aws:s3:::imageuploads01
      bucket: 'imageuploads01', // Ex. aboutreact
      region: 'us-east-1', // Ex. ap-south-1
      accessKey: 'AKIA24CPCGBT22ID7H5V',
      // Ex. AKIH73GS7S7C53M46OQ
      secretKey: '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',
      // Ex. Pt/2hdyro977ejd/h2u8n939nh89nfdnf8hd8f8fd
      successActionStatus: 201,
    }

    console.log(prescription)

    try {
      
      const resp = await RNS3.put({
        // `uri` can also be a file system path (i.e. file://)
        uri: prescription.uri,
        name: prescription.fileName,
        type: prescription.type,
      }, options)

      console.log(resp)
      const response = resp;

      if (response.status !== 201){
        Alert.alert('Failed to upload image to S3');
        return setloadinga(false)
      }
          
        console.log(response.body);
        // setFilePath('');
        let {
          bucket,
          etag,
          key,
          location
        } = response.body.postResponse;

        setprescription(location)
        setprescriptiona(false)
        Alert.alert('uploaded prescription to server');
    } catch (error) {
      
    }

    setloadinga(false)
  }

  const takepicture = async ()=>{

    try{
      if(camera == null){
        return
      }
      let res = await camera.current.takePictureAsync({
        base64: true,
        quality: 0.5
      })
      setcapture(false)
      setprescription(res.uri)
      setprescriptiona(true)
    }catch(e){
      Alert.alert('Error', e.message)
    }

  }

  const nototc = ()=>{
    Alert.alert('Buy Prescribed Drugs', 'How would you like to upload your prescription?', [
      {
        text: 'Take Picture',
        onPress: fromCamera
      },
      {
        text: 'Upload from Phone',
        onPress: fromDevice
      },
      {
        text: 'Consult with Pharmacist',
        onPress: ()=>{navigation.replace('Doctors')}
      },
      {
        text: 'Dismiss',
        style: 'cancel',
        onPress: ()=>{}
      }
    ])
  }

  useEffect(()=>{

    (async()=>{
      setloadingaaa(true);
      let found = await fetchDrugs(false);
      setDrugs(found);
      setFiltered(found);

      let account = await AsyncStorage.getItem('user');
      setuser(JSON.parse(account));
      setloadingaaa(false);
    })()

  }, [])

  let arr = []

  const addToCart = (item)=>{

    arr = cart

    if(arr.filter(d=>d.drugId == item.id).length == 0){
      arr.push({
        drug: item,
        quantity: 1,
        drugId: item.id,
      })
    }else{
      arr[arr.indexOf(arr.filter(d=>d.drugId == item.id)[0])].quantity += 1
    }

    setCart(arr)
    AsyncStorage.setItem('cart', arr.toString())
    setTotal((total+item.unitprice))
  }

  const removeFromCart = (item)=>{

    arr = cart

    if(arr.filter(d=>d.drugId == item.id).length > 0){
      if(arr[arr.indexOf(arr.filter(d=>d.drugId == item.id)[0])].quantity > 1){
        arr[arr.indexOf(arr.filter(d=>d.drugId == item.id)[0])].quantity -= 1
      }else{
        delete arr[arr.indexOf(arr.filter(d=>d.drugId == item.id)[0])]
      }
      setTotal((total-item.unitprice))
    }

    setCart(arr)
    AsyncStorage.setItem('cart', arr.toString())
  }

  const checkout = ()=>{

    let otc = true
    let count = 0

    cart.forEach(item=>{
      if(item.drug.requiresPrescription){
        otc = false
        count++
      }
    })

    if(otc){
      navigation.navigate('Cart', {cart, total, user})
    }else{
      if(prescription){
        navigation.navigate('Cart', {cart, total, user, prescription})
      }else{
        Alert.alert('Missing Prescription', `${count} of the drugs on your cart require a doctor's prescription. Please upload one before your procced`)
      }
    }
  }

  return (
    <Layout style={{ flex: 1}}>

      {/* camera modal */}
      <Modal
        visible={capture}
        onBackdropPress={()=>setcapture(false)}
        backdropStyle={{
          backgroundColor: 'rgba(0, 0, 0, 0.3)'
        }}>
        <Card disabled style={{width: Dimensions.get('screen').width-30}}>
          <RNCamera
            ref={camera}
            autoFocus={'on'}
            pauseAfterCapture
            captureAudio={false}
            playSoundOnCapture={false}
            useCamera2Api
            ratio="1:1"
            style={{height: 300, width: Dimensions.get('screen').width-80, marginBottom: 15, alignSelf: 'center'}}
            type={RNCamera.Constants.Type.back}
            flashMode={RNCamera.Constants.FlashMode.auto}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            androidRecordAudioPermissionOptions={{
              title: 'Permission to use audio recording',
              message: 'We need your permission to use your audio',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
          />
          <Button onPress={takepicture}>Take Picture</Button>
        </Card>
      </Modal>

      {/* preview modal */}
      <Modal
        visible={prescriptiona}
        backdropStyle={{
          backgroundColor: 'rgba(0, 0, 0, 0.3)'
        }}>
        <Card disabled style={{width: Dimensions.get('screen').width-30}}>
          <Image source={{uri: prescription ? prescription.uri : ''}} style={{width: '100%', height: 300}} resizeMode="contain" />
          <Button disabled={loadinga} onPress={upload}>Proceed</Button>
          <Button onPress={()=>setprescription(null)} appearance={'ghost'} status="basic">Cancel</Button>
        </Card>
      </Modal>

      <View style={[style.top, {backgroundColor: colors.primary}]}>
        <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50}} status='control' accessoryLeft={BackIcon}/>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Button onPress={nototc} status={'control'} accessoryLeft={PrescriptionIcon} appearance={'outline'}>Presciption</Button>
              </View>
          </View>

          <Text category={'h4'} style={{marginTop: 15, color: 'white'}}>Buy your drugs</Text>
          <Input 
              placeholder={'Search...'}
              placeholderTextColor={'grey'}
              size="large"
              onChangeText={search}
              style={{
                  marginTop: 25,
                  marginBottom: 15
              }}/>
        </SafeAreaView>
      </View>

      {
        loadingaaa ? 
        <View style={{position: 'absolute', justifyContent: 'center', alignItems: 'center', flex: 1, zIndex: 9999, height: '100%', width: '100%'}}>
          <ActivityIndicator />
        </View> : <></>
      }
      
      <FlatList
        data={filtered}
        numColumns={2}
        renderItem={({item, index})=> <Drug drug={item} remove={removeFromCart} cart={addToCart}/>}
        keyExtractor={item=>item.id}
        showsVerticalScrollIndicator={false}
        />
        { total > 0 ?
        <Button onPress={checkout} style={{minHeight: 65}} accessoryLeft={CartIcon}>Checkout {'GHS '+total.toFixed(2)}</Button> : <></> }
    </Layout>
  );
}

export default Pharmacy

const style = StyleSheet.create({
  top: {
      // borderBottomLeftRadius: 15,
      // borderBottomRightRadius: 15,
      padding: 15
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
})