import React, {useEffect, useState} from 'react';
import { View, TouchableOpacity, Platform, StyleSheet, Image, Alert, Modal } from 'react-native';
import { LoginManager, Profile, Settings } from "react-native-fbsdk-next";
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';  
import { styles } from '../utils/style';
import { login, sociallogin, resetpassword, checkotp, changePasswordd } from '../api/auth';
import { Layout, Input, Button, Text } from '@ui-kitten/components';
import { colors } from '../utils/constants';
import { SignInWithAppleButton } from 'react-native-apple-authentication'

GoogleSignin.configure({
    scopes: ['https://www.googleapis.com/auth/user.gender.read'], // [Android] what API you want to access on behalf of the user, default is email and profile
    webClientId: '77071010064-u7nlds7si6bh93bmvs2q5dkb8er0qkaa.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
    offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
    forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
    iosClientId: '77071010064-ejb2e2j3v5s2gmj3o3vgf1ai838un9fs.apps.googleusercontent.com', // [iOS] if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    profileImageSize: 120, // [iOS] The desired height (and width) of the profile image. Defaults to 120px
});  

function Login({navigation}) {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState()
    const [emailError, setEmailError] = useState(false)
    const [passwordError, setPasswordError] = useState(false)
    const [loading, setLoading] = useState(false)
    const [show_forget_p, setshow_forget_p] = useState(false)
    const [forget_p_text, setforget_p_text] = useState('')
    const [forget_p_text_holder, setforget_p_text_holder] = useState('Resent Password')
    const [forget_p_error, setforget_p_error] = useState(false)
    const [forget_p_page, setforget_p_page] = useState(1)
    const [reset_code, setreset_code] = useState('')
    const [reset_passworda, setreset_passworda] = useState('')
    const [forget_p_error1, setforget_p_error1] = useState(false)
    const [forget_p_error2, setforget_p_error2] = useState(false)

    const reset_password_confirm = async()=>{
        setLoading(true)

        try {
            // send otp to server to get code
            // use code to reset connection

            const response = await checkotp(reset_code);
            if(response.success){
                var token = response.token;

                const respon = await changePasswordd({password: reset_passworda}, token);

                if(respon.success){
                    setforget_p_error(false)
                    setforget_p_text_holder('Password Changed');
                    Alert.alert('Password Changed');
                }else{
                    setforget_p_error(false)
                    setforget_p_text_holder('Password Error')
                }
            }else{
                setforget_p_error(false)
                setforget_p_text_holder('Code MisMatch')
            }
        } catch (e) {
            setforget_p_error(false)
                setforget_p_text_holder('Network error')
        }

        setLoading(false)
    }
    
    const reset_password = async()=>{
        setError()
        setEmailError(false)
        setPasswordError(false)

        if(forget_p_text.length < 6){
            setforget_p_error(true)
            setshow_forget_p(false)
            setError('Please provide a valid email address.')
            return
        }

        setLoading(true)

        try{
            const response = await resetpassword(forget_p_text)

            if(response.success){
                setforget_p_error(false)
                setforget_p_text_holder('Change Password')
                setforget_p_page(2)
            }else{
                setforget_p_error(false)
                setforget_p_text_holder('Account not found')
            }
        }catch(e){
            setforget_p_error(false)
            setshow_forget_p(false)
            setError(e.message)
        }

        setLoading(false)
    }

    const authenticate = async ()=>{

        setError()
        setEmailError(false)
        setPasswordError(false)

        if(email.length < 6){
            setEmailError(true)
            setError('Please provide a valid email address.')
            return
        }

        if(password.length < 6){
            setPasswordError(true)
            setError('Password minimum length is 6 characters.')
            return
        }

        setLoading(true)

        try{
            const response = await login(email, password)

            if(response.success){
                setLoading(false)
                navigation.replace('Main');
            }else{
                setError(response.message)
                setLoading(false)
            }
        }catch(e){
            setError(e.message)
            setLoading(false)
        }

    }

    const google = async () => {
        if(loading){
            return
        }
        try {
            setLoading(true)
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            const response = await sociallogin(userInfo.user.email)

            if(response.success){
                setLoading(false)
                navigation.replace('Main');
            }else{
                setError(response.message)
                setLoading(false)
            }
        } catch (error) {
            setLoading(false)
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
                Alert.alert('Unavailable', 'Your device does not have play services or play services have been disabled')
            } else {
                // some other error happened
                setError(error)
            }
        }
    };

    const facebook = async ()=>{

        if(loading){
            return
        }

        setLoading(true)
        try{
            if( Platform.OS == 'ios'){
                Settings.initializeSDK();
            }
            Settings.setAppID('709847706663241')
            
            // Settings.initializeSDK()
        LoginManager.logInWithPermissions(["public_profile", "email"]).then(
            async function(result) {
                setLoading(false)
                if (result.isCancelled) {
                    console.log("Login cancelled");
                } else {
                    console.log(
                    "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );

                    let user = await Profile.getCurrentProfile()

                    if(user!=null){
                        try{
                            const response = await sociallogin(user.email)

                            if(response.success){
                                setLoading(false)
                                navigation.replace('Main');
                            }else{
                                setError(response.message)
                                setLoading(false)
                            }
                        }catch(e){
                            setLoading(false)
                            setError(e.message)
                        }
                    }
                    
                }
            },
            function(error) {
                setLoading(false)
                console.log(error)
                setError('Facebook Refuse Connection#1')
            }
          );
        }catch(e){
            console.log(e.message)
            setError('Facebook Refuse Connection#0')
            setLoading(false)
        }

    }

    return (
        <Layout style={{ flex: 1, justifyContent: 'center' }}>

            <Image source={require('../assets/images/logo.png')} shouldRasterizeIOS style={{width: 200, height: 150, alignSelf: 'center'}} resizeMode={'contain'} /> 

            <Text status={'danger'} style={{ textAlign: 'center'}}>{error ? error : ''}</Text>
            
            <Modal
                visible={show_forget_p}
                transparent={true}>
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <View style={style.modalContainer}>
                            <View style={{flex: 1, padding: 14}}>

                                {
                                    (forget_p_page == 1) ? 
                                    <View>
                                        <Text style={{}}>We would send a reset link to your email provided</Text>

                                        <Input 
                                            mode={'outlined'} 
                                            style={{marginVertical: 15}} 
                                            placeholder={'Email'} 
                                            label={'Email'} 
                                            appearance={'default'}
                                            size={'large'}
                                            value={forget_p_text}
                                            error={forget_p_error}
                                            onChangeText={text=>{
                                                setforget_p_error(false)
                                                setError()
                                                setforget_p_text(text)
                                            }}
                                            keyboardType={'email-address'} 
                                            shouldRasterizeIOS 
                                            textContentType={'emailAddress'}/>
                                            
                                            <Button disabled={loading} onPress={()=>reset_password()} style={{margin: 15}}>{ loading ? 'Please wait...' : forget_p_text_holder}</Button>
                                    </View>: <></>
                                }

{
                                    (forget_p_page == 2) ? 
                                    <View>
                                        <Text style={{}}>We have sent a reset code to your provided email address</Text>

                                        <Input 
                                            mode={'outlined'} 
                                            placeholder={'Enter the reset code'} 
                                            label={'Reset Code'} 
                                            appearance={'default'}
                                            size={'large'}
                                            value={reset_code}
                                            error={forget_p_error1}
                                            onChangeText={text=>{
                                                setforget_p_error1(false)
                                                setError()
                                                setreset_code(text)
                                            }}
                                            shouldRasterizeIOS />

                                        <Input 
                                            mode={'outlined'} 
                                            placeholder={'Enter new Password'} 
                                            label={'New Password'} 
                                            appearance={'default'}
                                            size={'large'}
                                            value={reset_passworda}
                                            error={forget_p_error2}
                                            onChangeText={text=>{
                                                setforget_p_error2(false)
                                                setError()
                                                setreset_passworda(text)
                                            }}
                                            shouldRasterizeIOS />
                                            
                                            <Button disabled={loading} onPress={()=>reset_password_confirm()} style={{margin: 15}}>{ loading ? 'Please wait...' : forget_p_text_holder}</Button>
                                    </View>: <></>
                                }
                                    
                            </View>

                            <Button style={{backgroundColor: 'black'}} onPress={()=> setshow_forget_p(false)}>CLOSE</Button>

                        </View>
                    </View>
            </Modal>

            <Input 
                mode={'outlined'}
                appearance={'default'}
                size={'large'}
                style={{margin: 15}} 
                placeholder={'Email address'} 
                label={'Email address'} 
                value={email}
                onChangeText={text=>{
                    setEmailError(false)
                    setError()
                    setEmail(text)
                }}
                keyboardType={'email-address'} 
                autoCapitalize={'none'} 
                shouldRasterizeIOS
                error={emailError}
                textContentType={'emailAddress'} 
                autoCompleteType={'email'}/>

            <Input 
                mode={'outlined'} 
                style={{marginHorizontal: 15}} 
                placeholder={'Password'} 
                label={'Password'} 
                appearance={'default'}
                size={'large'}
                value={password}
                error={passwordError}
                onChangeText={text=>{
                    setPasswordError(false)
                    setError()
                    setPassword(text)
                }}
                secureTextEntry={true} keyboardType={'default'} 
                shouldRasterizeIOS 
                textContentType={'password'}/>

            <TouchableOpacity style={{alignSelf: 'flex-end',marginHorizontal: 15, marginVertical: 10}} onPress={()=> { setforget_p_page(1); setshow_forget_p(true)}}>
                <Text style={{color: colors.primary}}>Forgot Password?</Text>
            </TouchableOpacity>

            <Button disabled={loading} onPress={authenticate} style={{margin: 15}}>{ loading ? 'Please wait...' : 'Login'}</Button>

            {/* <TouchableRipple 
                onPress={authenticate}
                style={
                    [
                        styles.button, 
                        {
                            backgroundColor: colors.primary, 
                            marginVertical: 15
                        }
                    ]
                }>

                <Text 
                    style={styles.button_text}>
                    { loading ? 'Please wait...' : 'Login'}
                </Text>
                
            </TouchableRipple> */}

            <View style={{flexDirection: 'row', marginTop: 15}}>

                <TouchableOpacity 
                    onPress={google}
                    style={
                        [
                            styles.button, 
                            style.button,
                            {
                                marginBottom: 15,
                                flex: 1
                            }
                        ]
                    }>

                    <>
                        <Image source={require('../assets/images/google.png')} shouldRasterizeIOS style={{width: 25, height:25, marginEnd:15}} resizeMode={'contain'} />

                        <Text 
                            style={{color: '#141414'}}>
                            Google
                        </Text>
                    </>
                    
                </TouchableOpacity>

                <TouchableOpacity 
                    onPress={facebook}
                    style={
                        [
                            styles.button, 
                            style.button,
                            {
                                backgroundColor: '#3B5998',
                                marginBottom: 15,
                                marginStart: 0,
                                flex: 1
                            }
                        ]
                    }>

                    <>
                    <Image source={require('../assets/images/facebook.png')} shouldRasterizeIOS style={{width: 25, height:25, marginEnd:15}} resizeMode={'contain'} />

                    <Text 
                        style={styles.button_text}>
                        Facebook
                    </Text>
                    </>
                    
                </TouchableOpacity>
            </View>

            <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row', backgroundColor: 'black', width: '80%'}}>
            { (Platform.OS == 'ios') && SignInWithAppleButton({
                buttonStyle: {
                }, 
                callBack: (result)=>{
                    (async()=>{
                        var email = result.email;

                        try{
                            const response = await sociallogin(email)

                            if(response.success){
                                setLoading(false)
                                navigation.replace('Main');
                            }else{
                                setError(response.message)
                                setLoading(false)
                            }
                        }catch(e){
                            setLoading(false)
                            setError('can\'t process login, try again or navigate to >> ios settings >> app and security and remove claronDoc to reset login')
                        }
                    })()
                },
                buttonText: "Sign In With Apple",
            })}
            </View>
            
            <TouchableOpacity onPress={()=>navigation.navigate('Register')} style={{
                margin: 15
                }}>

                <Text style={{
                    color: colors.primary,
                    textAlign: 'center',
                    fontSize: 16
                }}>
                    I don't have an account
                </Text>
            </TouchableOpacity>

        </Layout>
    );
}

export default Login

const style = StyleSheet.create({
    button: {
        backgroundColor: '#ffffff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3,
        elevation: 3,
        flexDirection: 'row'
    },
    modalContainer: {
        width: '70%',
        marginHorizontal: '15%',
        height: '46%',
        backgroundColor: 'white',
        borderRadius: 12,
        elevation: 99,
        zIndex: 99,
        borderColor: 'rgba(0, 0, 0, 0.2)',
        borderWidth: 1,
    }
})