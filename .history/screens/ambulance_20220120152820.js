import React, {useEffect, useState} from 'react';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import { View, Image, Dimensions, StyleSheet, ScrollView, TouchableOpacity, Alert, SafeAreaView } from 'react-native'
import { Layout, Text, Card, Icon, Button, Input, IndexPath, Select, SelectItem } from '@ui-kitten/components';
import { ActivityIndicator, useTheme } from 'react-native-paper';
import { colors } from '../utils/constants';
import { requestAmbulance } from '../api/pharmacy';

const BackIcon = (props) => (
  <Icon {...props} name='chevron-left-outline'/>
);

const Ambulance = ({route, navigation}) =>{

  const [selectedIndex, setSelectedIndex] = React.useState(new IndexPath(0));
  const [address, setaddress] = useState('')
  const [comments, setcomments] = useState('')
  const [error, seterror] = useState()
  const [loading, setloading] = useState(false)
  const cases = [
    'Fatal Accident',
    'Asthma Attack',
    'Going into Labour',
    'Other'
  ]

  const submitRequest = async()=>{
    try{
      setloading(true)
      await requestAmbulance(address, cases[selectedIndex-1], comments)
      Alert.alert('Request Sent', 'We have received your ambulance request, an ambulance will be dispatched to your address soon.')
      setloading(false)
    }catch(e){
      setloading(false)
      Alert.alert('Something went wrong', 'Your request could not be sent at the moment, please try again or dial 911')
    }
  }

  return (
      <Layout style={{ flex: 1}}>
          <View style={{backgroundColor: colors.primary, width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/3, position: 'absolute', top: 0 }}/>
          
          <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0 }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
            <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='control' accessoryLeft={BackIcon}/>
            <Text category={'h4'} style={{color: 'white', margin: 15}}>Ambulance</Text>
          </View>

          <View shouldRasterizeIOS showsVerticalScrollIndicator={false}>
              <Layout style={{margin: 15, borderRadius: 5, padding: 25, borderWidth: 1, borderColor: '#f2f2f2'}}>
                
                <Text
                  category={'p1'}
                  appearance={'hint'}
                  style={{
                    marginVertical: 15
                  }}>
                    It is not necessary to answer the question below. You may call an ambulance right away.
                </Text>

                <Text
                  category={'label'}
                  appearance={'hint'}
                  style={{
                    marginTop: 15,
                    marginBottom: 5
                  }}
                  >Confirm Your Address</Text>
                <Input 
                  mode='outlined'
                  size={'large'}
                  placeholder={'Your address'}/>

                <Text
                  category={'label'}
                  appearance={'hint'}
                  style={{
                    marginTop: 15,
                    marginBottom: 5
                  }}
                  >What is the emergency?</Text>
                <Select
                  selectedIndex={selectedIndex}
                  value={cases[selectedIndex-1]}
                  onSelect={index => setSelectedIndex(index)}>
                  <SelectItem title='Fatal Accident'/>
                  <SelectItem title='Asthma Attack'/>
                  <SelectItem title='Going into Labour'/>
                  <SelectItem title='Other'/>
                </Select>
                
                <Text
                  category={'label'}
                  appearance={'hint'}
                  style={{
                    marginTop: 15,
                    marginBottom: 5
                  }}
                  >Your comments</Text>
                <Input 
                  mode='outlined'
                  size={'large'}
                  numberOfLines={3}
                  returnKeyType={'done'}
                  placeholder={'Comment'}/>

                <Button disabled={loading} onPress={submitRequest} appearance={'filled'} size={'large'} style={{textAlign: 'center', marginVertical: 25}}>
                  { loading ? 'Please wait...' : 'Call An Ambulance' }
                </Button>
                
              </Layout>                
          </View>
        </SafeAreaView>
      </Layout>
  );
}

export default Ambulance

const style = StyleSheet.create({
  container: {
    // maxHeight: 192,
  },
});