import React, { useState, useEffect, useRef } from 'react';
import { formatDistanceToNow } from 'date-fns'
import * as RN from 'react-native'
import { Layout, Text, Icon, List, ListItem, Button, Divider } from '@ui-kitten/components';
import { NativeModules, View, Image, SafeAreaView, ActivityIndicator, Dimensions } from 'react-native';
import { fetchNotifications } from '../api/notifications';

const BackIcon = (props) => (
    <Icon {...props} name='arrow-ios-back-outline'/>
);

const Notifications = ({navigation, route}) => {

    const [notifications, setNotifications] = useState([])
    const [loading, setloading] = useState(true)
    const [loading2, setloading2] = useState(true)

    const renderItemIcon = (props) => (
        <Icon {...props} name='bell-outline'/>
    );
    
    const renderItem = ({ item, index }) => (
        <ListItem
            onPress={()=> item.body.includes('appointment') ? navigation.navigate('Appointments') : item.body.includes('new message') ? navigation.navigate('Conversation', {to: item.extraData2.doctor.email, name: item.extraData2.doctor.firstname+' '+item.extraData2.doctor.lastname, doctor: item.extraData2.doctor}) : navigation.navigate('History', { type: item.body.includes('Ambulance') ? 'ambulance' : item.body.includes('Lab Request') ? 'lab' : item.body.includes('Home') ? 'homecare' : 'drug' }) }
            title={item.body}
            description={formatDistanceToNow(new Date(item.createDate), { addSuffix: true })}
            accessoryLeft={renderItemIcon}
        />
    );

    useEffect(()=>{

        (async()=>{
            setloading(true);
          let response = await fetchNotifications()
          setNotifications(response)
          setloading(false);
        //   console.log(response[0])
        })()
    
    }, [])

    return (
    <Layout style={{flex: 1}}>
        <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='basic' accessoryLeft={BackIcon}/>
                <Text category={'h4'} style={{marginEnd: 15}}>Notifications</Text>
            </View>

            <Divider/>
            { notifications.length > 0 ?
                <List
                    data={notifications}
                    renderItem={renderItem}
                    /> 
                :
                <Layout style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    {
                        loading ? 
                        <ActivityIndicator />
                        :
                        <Text category={'h5'} appearance={'hint'} style={{marginTop: 10}}>No notifications.</Text>
                    }
                </Layout>
            }
            {
            (loading2) ? <RN.View style={{position: 'absolute', height: Dimensions.get('screen').height, width: Dimensions.get('screen').width, zIndex: 999, top: 0, bottom: 0, left: 0, right: 0, flex: 1}}>
                    <ActivityIndicator />
                </RN.View>:<></>
            }
        </SafeAreaView>
    </Layout>
    );
}

export default Notifications