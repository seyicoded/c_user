import { Button, Icon, Layout, Text } from '@ui-kitten/components';
import axios from 'axios';
import React, {useEffect, useState, useRef} from 'react';
import {
  SafeAreaView,
  PermissionsAndroid,
  StatusBar,
  StyleSheet,
  useColorScheme,
  Platform,
  View,
  TouchableOpacity
} from 'react-native';
import RtcEngine, {
  ChannelProfile,
  ClientRole,
  RtcEngineContext,
  RtcLocalView,
  RtcRemoteView,
  VideoRenderMode
} from 'react-native-agora';
import firebase from 'firebase';
import AsyncStorage from '../AsyncStorageCustom'

const SpeakerIcon = (props)=><Icon {...props} name="volume-up-outline"/>
const EndIcon = (props)=><Icon {...props} name="close-outline"/>
const MuteIcon = (props)=><Icon {...props} name="mic-off-outline"/>
const VideoIcon = (props)=><Icon {...props} name="video-outline"/>

const VideoCall = ({navigation, route}) => {
  var call_data = route.params.urgent;
  
  const isDarkMode = useColorScheme() === 'dark';
  const [joined, setjoined] = useState(false)
  const [engine, setengine] = useState()
  const [muted, setmuted] = useState(false)
  const [video, setvideo] = useState(true)
  const [speaker, setspeaker] = useState(false)
  const [remoteid, setremoteid] = useState()

  if((call_data == undefined) || (call_data == null) ){
    (async()=>{
      console.log('reached for logger')
      call_data = JSON.parse(await AsyncStorage.getItem('call_d'))
    })()
  }

  const engine_r = useRef(null);
  engine_r.current = null;
  let _engine

  const _init = async (call_data) => {
    // _engine = await RtcEngine.create('0742c8affa02429b9622956bac0d67d0')
    _engine = await RtcEngine.createWithContext(
      new RtcEngineContext('0742c8affa02429b9622956bac0d67d0')
    )


    console.log(_engine)
    setengine(_engine)
    engine_r.current = _engine;

    requestCameraAndAudioPermission()

    _addListeners()

    await _engine.enableVideo()
    await _engine.startPreview()
    await _engine.enableLocalVideo(true)
    await _engine.startPreview()
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting)
    await _engine.setClientRole(ClientRole.Broadcaster)
    try{
      await _engine.joinChannel(call_data.token, call_data.channel, null, 0)
    }catch(e){
      console.log('Join Error: ', e)
    }
  }

  const requestCameraAndAudioPermission = async () =>{
    try {
        const granted = await PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
            PermissionsAndroid.PERMISSIONS.CAMERA,
        ])
        if (
            granted['android.permission.RECORD_AUDIO'] === PermissionsAndroid.RESULTS.GRANTED
        ) {
            console.log('You can use the mic')
            console.log(granted)
        } else {
            console.log('Permission denied')
        }
    } catch (err) {
        console.warn(err)
    }
  }

  // engine.startPreview()
  const _addListeners = () => {
    _engine.addListener('JoinChannelSuccess', (channel, uid, elapsed) => {
      console.info('JoinChannelSuccess', channel, uid)
      setjoined(true)
    })

    _engine.addListener('LeaveChannel', (stats) => {
      console.info('LeaveChannel', stats)
      setjoined(false)
      try{
        navigation.goBack()
      }catch(e){

      }
    })

    _engine.addListener('UserJoined', (uid, elapsed) => {
      console.info('UserJoined', uid, elapsed);
      setremoteid(uid)
      _engine.stopPreview()
    })

    _engine.addListener('Error', (e) => {
      console.info('Main Error', e);
    })

    _engine?.addListener('UserOffline', (uid, reason) => {
      console.info('UserOffline', uid, reason);
      setremoteid(null)
      try{
        navigation.goBack()
      }catch(e){

      }
    })

    // _engine.addListener('JoinChannelError', (e)=>{
    //   console.log(e)
    // })
  }

  const _onChangeRecordingVolume = (value) => {
    engine.adjustRecordingSignalVolume(value * 400)
  }

  const _onChangePlaybackVolume = (value) => {
    engine.adjustPlaybackSignalVolume(value * 400)
  }

  const _toggleInEarMonitoring = (isEnabled) => {
    engine.enableInEarMonitoring(isEnabled)
  }

  const _onChangeInEarMonitoringVolume = (value) => {
    engine.setInEarMonitoringVolume(value * 400)
  }

  const _leaveChannel = async () => {
    try{
      await engine.leaveChannel();
    }catch(e){
      console.log('Leave Error: ', e)
    }
    navigation.goBack()
  }

  const _switchCamera =async()=>{
    try{
      await engine.enableLocalVideo(!video)
      await engine.startPreview()
      setvideo(!video)
    }catch(e){
      console.log(e)
    }
  }

  const _switchMicrophone = async() => {
    try{
      await engine.enableLocalAudio(!muted)
      setmuted(!muted)
    }catch(e){
      console.log(e)
    }
  };

  const _switchSpeakerphone = () => {
    try{
      engine.setEnableSpeakerphone(!speaker)
      setspeaker(!speaker)
      console.log('speaker is:', !speaker)
    }catch(e){
      console.log(e)
    }
  }

  useEffect(() => {

    (async()=>{
      try{
        _init(call_data)
      }catch(e){
        console.log(e)
      }
    })()

    return () => {
      engine.destroy()
    }
  }, [call_data])

  return (
    <Layout style={{flex: 1}}>
      <SafeAreaView style={{flex: 1}}>
        <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />

        { joined ?
        <Layout style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          
          {remoteid != null ?
          <RtcRemoteView.SurfaceView
          style={{flex: 1, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, zIndex: -1}}
            uid={remoteid}
            zOrderMediaOverlay={false}
          /> : <Text style={{zIndex: 9999, color: 'green'}} category={'h5'}>Connecting...</Text> }
          
          <RtcLocalView.SurfaceView style={remoteid == null ? styles.localConnecting : styles.localConnected} />
        </Layout>
        :
        // <Layout style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        //   <Text category={'h5'}>Connecting...</Text>
        // </Layout>
        <RtcLocalView.SurfaceView style={styles.localConnecting} />
        }
        <View style={{position: 'absolute', bottom: 35, left: 15, right: 15, alignItems: 'center', justifyContent: 'space-around', flexDirection: 'row'}}>
        <Button onPress={_switchCamera} status={'basic'} style={{height: 65, width: 65, borderRadius: 65}} accessoryLeft={VideoIcon} appearance={video ? 'filled' : 'outline'}></Button>
          <Button onPress={_switchMicrophone} status={'basic'} style={{height: 65, width: 65, borderRadius: 65}} accessoryLeft={MuteIcon} appearance={!muted ? 'filled' : 'outline'}></Button>
          <Button onPress={_leaveChannel} status={'danger'} style={{height: 75, width: 75, borderRadius: 75}} accessoryLeft={EndIcon} appearance={'filled'}></Button>
          <Button onPress={_switchSpeakerphone} status={'basic'} style={{height: 65, width: 65, borderRadius: 65}} accessoryLeft={SpeakerIcon} appearance={speaker ? 'filled' : 'outline'}></Button>
        </View>
      </SafeAreaView>
    </Layout>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  localConnecting: {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    position: 'absolute',
    zIndex: 99
  },
  localConnected: {
    height: 200,
    width: 150,
    borderRadius: 5,
    backgroundColor: '#141414',
    position: 'absolute',
    top: 50,
    right: 15
  }
});

export default VideoCall;