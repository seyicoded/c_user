import React, {useState} from 'react';
import { View, FlatList, SafeAreaView, ActivityIndicator} from 'react-native';
import { Layout, TabBar, Tab, Text, Button, Card, Icon, Divider } from '@ui-kitten/components';
import { useEffect } from 'react';
import { myBookings, DeleteBooking } from '../api/doctors';
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import firebase from 'firebase';

const BackIcon = (props) => (
  <Icon {...props} name='chevron-left-outline'/>
);

const CancelIcon = (props) => (
  <Icon {...props} name='close-outline'/>
);

const Appointments = ({navigation}) => {

  const [index, setIndex] = useState(0)
  const [bookings, setBookings] = useState([]);
  const [filtered, setFiltered] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [showData, setshowData] = useState(false);
  const [loadinga, setloadinga] = useState(true);

  // console.log(filtered)

  const filter = (sel)=>{
    sel == 0 ?
      setFiltered(bookings.filter(booking=>moment().isBefore(new Date(booking.scheduledFor))))
    : setFiltered(bookings.filter(booking=>moment().isAfter(new Date(booking.scheduledFor))))
  }

  useEffect(()=>{
    // setloadinga(true)
    (async()=>{

      if(!loaded){
        try{
          loadData()
          // let res = await myBookings()
          // setBookings(res.requests)
          // setFiltered(bookings.filter(booking=>moment().isBefore(new Date(booking.scheduledFor))))
        }catch(e){
          console.log(e)
        }
        setLoaded(true)
        setloadinga(false)
      }

    })()
  })

  const cancelAppointment = async (id) =>{
    // console.log(filtered);
    try{
      setloadinga(true)
      const email = await AsyncStorage.getItem('email');
      await firebase.firestore().collection('deletedAppointment').doc(email).collection('list').add({id: id});
      // let resa = await DeleteBooking(id)
      // console.log(resa.data)
      loadData();
    }catch(e){
      console.log(e)
      setloadinga(false)
    }

  }

  const loadData = async()=>{
    setloadinga(true)
    setshowData(false)
    let res = await myBookings()
    setBookings(res.requests)
    setFiltered(bookings.filter(booking=>moment().isBefore(new Date(booking.scheduledFor))))
    
    // get firebase cancelled appointment
    const email = await AsyncStorage.getItem('email');
    firebase.firestore().collection('deletedAppointment').doc(email).collection('list').onSnapshot(snapshot=>{
      var r = snapshot.docs.map(doc =>{
          return (doc.data())
            
          // return {
          //     id: doc.id,
          //     data: (doc.data())
          //   }
        // check if data is thesame as uid
      //   if(my_data.uid != doc.id){
          
      //   }
        
      });

      console.log(r)
      setloadinga(false)
    }, error=>{
      console.log(error)
    });
    
  }

  return (
      <Layout style={{ flex: 1}}>
        <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>

        {
          loadinga ? 
          <View style={{flex: 1, position: 'absolute', zIndex: 99, justifyContent: 'center', alignItems: 'center', width: '100%', height: '100%'}}>
            <ActivityIndicator color="green"/>
          </View>:<></>
        }
        
        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
          <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='basic' accessoryLeft={BackIcon}/>
          <Text category={'h4'} style={{marginEnd: 15}}>My Appointments</Text>
        </View>
        
        <TabBar
          style={{marginTop: 10}}
          selectedIndex={index}
          appearance={'default'}
          shouldRasterizeIOS
          onSelect={(index)=>{
            setIndex(index)
            filter(index)
          }}
          >
          <Tab title='Upcoming'/>
          <Tab title='Past'/>
        </TabBar>

        <Divider/>

        { (filtered.length > 0 && showData) ?
        <FlatList
          data={filtered}
          renderItem={({item})=>{
            return(
              <Card style={{marginHorizontal: 15, marginVertical: 5}}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',}}>
                  <View>
                    <Text category={'h6'}>{item.physician.fullName}</Text>
                    <Text category={'s1'} appearance={'hint'} style={{marginTop: 5}}>{ index == 0 ? 'Scheduled for' : 'Was on' } {'\n'}{new Date(item.scheduledFor).toString().substring(0, 21)}</Text>
                  </View>
                  <View>
                    { item.status == 'Pending' && index == 0 ?
                    <Button onPress={()=>{cancelAppointment(item.id)}} appearance='outline' style={{ width: 50, margin: 15}} status='danger' accessoryLeft={CancelIcon}/>
                    : <></>
                    }
                  </View>
                </View>
              </Card>
            )
          }}/>
          :
          <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text appearance={'hint'} category={'h6'}>No appointments to show.</Text>
          </Layout>
          }
        </SafeAreaView>
      </Layout>
  );
}

export default Appointments