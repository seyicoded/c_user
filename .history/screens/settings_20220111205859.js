import React, {useEffect, useState} from 'react';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import * as AsyncStorage from '../AsyncStorageCustom'

import { View, Dimensions, StyleSheet, Linking, SafeAreaView } from 'react-native'
import { Layout, Text, Card, Icon, Button, List, ListItem, Spinner } from '@ui-kitten/components';
import { PreferencesContext } from '../context';
import { colors } from '../utils/constants';

const ForwardIcon = (props) => (
    <Icon {...props} name='arrow-ios-forward-outline' fill={'grey'}/>
);

const GiftIcon = (props) => (
    <Icon {...props} name='gift-outline'/>
);

const BackIcon = (props) => (
  <Icon {...props} name='chevron-left-outline'/>
);

const Settings = ({route, navigation}) =>{

    const [user, setUser] = useState()
    const themeContext = React.useContext(PreferencesContext);
    const [dark, setDark] = React.useState(themeContext.theme == 'dark')

    const list = [
      {
        title: 'App Theme',
        icon: 'smartphone-outline',
        description: 'Switch dark theme',
      },
      {
        title: 'Security',
        icon: 'shield-outline',
        description: 'Enable app lock',
      },
      {
        title: 'Notifications',
        icon: 'bell-outline',
        description: 'How you want to be notified',
      },
      {
        title: 'Terms & Conditions',
        icon: 'file-text-outline',
        description: 'Terms of using this software',
        link: 'http://clarondoc.com/terms'
      },
      {
        title: 'About',
        icon: 'info-outline',
        description: 'About ClaronDoc',
        link: 'http://clarondoc.com/about'
      }
    ]

    const changeTheme = ()=>{
      setDark(!dark);
      themeContext.toggleTheme()
    }

    const renderItem = ({ item, index }) => (
      <ListItem
        title={`${item.title}`}
        description={`${item.description}`}
        accessoryLeft={ (props) => {
          return (
            <Icon {...props} name={item.icon}/>
          );
        }}
        onPress={()=>{
          item.title == 'App Theme' ? changeTheme()
          : item.title == 'Security' ? ()=>{}
          : item.title == 'Notifications' ? navigation.navigate('NotificationSettings')
          : navigation.navigate('Web', {title: item.title, url: item.link});
        }}
        // accessoryRight={renderItemAccessory}
      />
    );

    useEffect(()=>{

      (async()=>{
        let account = await AsyncStorage.getItem('user')
        setUser(JSON.parse(account))
      })()

    }, [])

    return (
        <Layout style={{ flex: 1}}>
            <View style={{backgroundColor: colors.primary, width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/3, position: 'absolute', top: 0 }}/>
            
            <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0 }}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='control' accessoryLeft={BackIcon}/>
                <Text category={'h4'} style={{color: 'white', margin: 15}}>Settings</Text>
              </View>
            </SafeAreaView>

            { user ?
            <View shouldRasterizeIOS showsVerticalScrollIndicator={false}>
              <Card disabled style={{margin: 15}}>
                <List
                  // style={style.container}
                  data={list}
                  renderItem={renderItem}
                />

                { route.params.subscribed ?
                  <Button onPress={()=>navigation.navigate('Subscription')} accessoryLeft={GiftIcon} appearance={'outline'} style={{textAlign: 'center', marginVertical: 25}}>
                    Upgrade Subscription
                  </Button> : <></> }
                
              </Card>
            </View>
            :
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Spinner size={'medium'}/>
            </View> }

        </Layout>
    );
}

export default Settings

const style = StyleSheet.create({
  container: {
    // maxHeight: 192,
  },
});