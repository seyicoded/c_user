import React, {useState, useEffect} from 'react'
import { View, StyleSheet, ScrollView, FlatList } from 'react-native'
import { Layout, Text, Button, Icon, Card, Input } from '@ui-kitten/components';
import { MenuItem, OnlineDoctor } from '../utils/components';

export default function Search({navigation, route}) {

    const unfilter_menu = route.params.menu;
    const unfilter_doctors = route.params.doctors;
    const user = route.params.user;

    var menu = unfilter_menu;
    var doctors = unfilter_doctors;
    
    // console.log('a')
    // console.log(route)
    const BackIcon = (props) => (
        <Icon {...props} onPress={()=>navigation.goBack()} name='arrow-ios-back-outline'/>
    );

    // let's the search go through params doctor then other doctor but using get to obtain data
    
    return (
        <View style={styles.container}>
            <View style={{flexDirection: 'row'}}>
                <Input accessoryLeft={BackIcon} style={{flex: 1,}} autoFocus={true} placeholder="type to start searching" returnKeyType="search" />
            </View>
            <ScrollView style={styles.container}>
                <FlatList
                    data={ menu }
                    renderItem={item=>
                    <MenuItem 
                        option={item}
                        subscribed={'subscription' in user ? user.subscription != 'Normal' && user.subscription != null : false}
                        navigate={()=>navigation.navigate(item.item.screen)}/>}
                        keyExtractor={item=>item.name}
                        showsHorizontalScrollIndicator={false}
                        horizontal
                        style={{marginVertical: 15, marginStart: 10}}
                    />
                {  doctors.length > 0 ?
                    <View style={{marginHorizontal: 15, flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{marginStart: 5, height: 15, width: 15, borderRadius: 15, backgroundColor: 'green', marginEnd: 10}}/>
                        <Text category={'h4'}>Doctors</Text>
                    </View> : <></> }

                <FlatList
                    data={doctors}
                    renderItem={({item, index})=><OnlineDoctor navigate={()=>navigation.navigate('Doctor', {doctor: item})} doctor={item}/>}
                    keyExtractor={item=>item.email}
                    showsVerticalScrollIndicator={false}
                    style={{marginBottom: 15, marginStart: 10}}
                    />                

            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
