import React, {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { View, Dimensions, StyleSheet, ScrollView, TouchableOpacity, Platform, Alert, SafeAreaView } from 'react-native'
import { Layout, Text, Icon, Button, Input } from '@ui-kitten/components';
import { initPayment } from '../api/paystack';
import { colors } from '../utils/constants';


const phoneIcon = (props) => (
    <Icon {...props} name='smartphone-outline' fill={'grey'}/>
);

const cardIcon = (props) => (
  <Icon {...props} name='credit-card-outline' fill={'grey'}/>
);
const calendarIcon = (props) => (
  <Icon {...props} name='calendar-outline' fill={'grey'}/>
);
const securityIcon = (props) => (
  <Icon {...props} name='shield-outline' fill={'grey'}/>
);

const DownIcon = (props) => (
  <Icon {...props} name='edit-2-outline'/>
);

const BackIcon = (props) => (
  <Icon {...props} name='chevron-left-outline'/>
);

const SubscriptionSummary = ({route, navigation}) =>{

  const [user, setUser] = useState()
  const [name, setName] = useState()
  const [id, setId] = useState()
  const [card, setCard] = useState({
    number: '',
    month: '',
    year: '',
    cvv: ''
  })
  const [method, setMethod] = useState('Mobile Money')
  const [price, setPrice] = useState(0)
  const [phone, setPhone] = useState('')
  const [loading, setLoading] = useState(false)
  const [button, setButton] = useState('Subscribe Now')
  const [reference, setReference] = useState()
  const [status, setStatus] = useState()
  const [phone_error, setPhoneError] = useState()
  const [card_error, setCardError] = useState()
  const mtns = ['024', '054', '055', '059']
  const tigos = ['027', '057', '026', '056']
  const vods = ['020', '050']

  const changeMethod = () => {
    Alert.alert('Change Payment Method', 'Please select your preferred payment method', [
      {
        text: 'Mobile Money',
        onPress: ()=>setMethod('Mobile Money')
      },
      {
        text: 'Visa/Mastercard',
        onPress: ()=>setMethod('Card')
      },
      {
        text: 'Dismiss',
        onPress: ()=>{},
        style: 'destructive'
      }
    ])
  }

  const subscribe = async () => {

    if(method == 'Mobile Money'){

      let network

      if(phone.length < 10){
        setPhoneError('Please enter a valid number')
        return
      }

      mtns.includes(phone.substr(0, 3)) ? network = 'mtn'
      : tigos.includes(phone.substr(0, 3)) ? network = 'tgo'
      : vods.includes(phone.substr(0, 3)) ? network = 'vod'
      : network = null

      if(network == null){
        setPhoneError('Please enter a Ghanaian number')
        return
      }

      try{

        setLoading(true)
        setButton('Initializing Transaction...')
        let init = await initPayment(price, phone, network)
        
        console.log(init)

        if(init.status){

          if(init.data.status == 'send_otp'){
            setLoading(false)
            setButton('Submit OTP')
          }else if(init.data.status == 'pay_offline'){
            setLoading(false)
            setButton('Awaiting for payment confirmation...')
          }else if(init.data.status == 'success'){
            setLoading(false)
            setButton('Done')
            navigation.pop()
            navigation.replace('PaymentResult', {purpose: price == 20 ? 'Basic Plan Subscription' : price == 20 ? 'Premium Plan Subscription' : 'Family Plan Subscription', amount: price, reference, date: init.data.transaction_date})
          }else{
            setLoading(false)
            setButton(init.data.gateway_response)
          }

        }else{
          setLoading(false)
          setButton('Subscribe Now')
          setPhoneError(init.data.message)
          return
        }

      }catch(e){
        setLoading(false)
        setButton('Subscribe Now')
        setPhoneError(e.message)
        return
      }
    }else{

      if(card.number.length < 16){
        setCardError('Please enter a valid card number')
        return
      }

      if(card.month.length < 2){
        setCardError('Please enter a valid expiry month')
        return
      }

      if(card.month*1 > 12){
        setCardError('Please enter a valid expiry month')
        return
      }

      if(card.year.length < 2){
        setCardError('Please enter a valid expiry year')
        return
      }

      if(card.year*1 < 21){
        setCardError('Please enter a valid expiry year')
        return
      }

      if(card.cvv.length < 3){
        setCardError('Please enter a valid card cvv')
        return
      }

    }

  }

  useEffect(()=>{

    (async()=>{
      let account = await AsyncStorage.getItem('user')
      const {name, id, price} = route.params
      setId(id)
      setName(name)
      setPrice(price)
      setUser(JSON.parse(account))
    })()

  }, [])

    return (
      <Layout style={{ flex: 1}}>
        <View style={{backgroundColor: colors.primary, width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/3, position: 'absolute', top: 0 }}/>
          
        <SafeAreaView style={{paddingTop: Platform.OS == 'android' ? 35 : 0, flex: 1 }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
            <Button onPress={()=>navigation.goBack()} appearance='outline' style={{ width: 50, margin: 15}} status='control' accessoryLeft={BackIcon}/>
            <Text category={'h4'} style={{color: 'white', margin: 15}}>Confirm Subscription</Text>
          </View>

          <Layout style={{margin: 15, flex: 1, borderRadius: 5, borderColor: '#F2F2F2', borderWidth: 1, padding: 25}}>
            <ScrollView style={{flex: 1}} shouldRasterizeIOS showsVerticalScrollIndicator={false}>

              <Text category={'h1'} style={{alignSelf: 'center', marginTop: 25}}>GHS {price.toFixed(2)}</Text>
              <Text category={'h6'} style={{alignSelf: 'center', marginTop: 15, marginBottom: 25}} appearance={'hint'}>{name}</Text>

              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 25}}>
                <Text category={'p1'} appearance={'default'}>Subscription Start Date</Text>
                <Text category={'p1'} appearance={'hint'}>{new Date().toString().substr(0, 16)}</Text>
              </View>

              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 25, marginTop: 15}}>
                <Text category={'p1'} appearance={'default'}>Payment Method</Text>
                <Button onPress={changeMethod} appearance={'outline'} size='tiny' accessoryRight={DownIcon}>
                  {method}
                </Button>
                {/* <View style={{flexDirection: 'row'}}>
                  <Text category={'p1'} appearance={'hint'}>{method}</Text>
                  <Icon fill={'#ffffff'} style={{width: 15, height: 15}} name='edit-2-outline' />
                </View> */}
              </View>

              <Text category={'h5'} style={{alignSelf: 'center', marginTop: 15}} appearance={'hint'}>Subscription Overview</Text>

              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 25}}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Text category={'p1'}>Next Payment</Text>
                  <Text category={'p1'} appearance={'hint'}>{new Date().toString().substr(0, 16)}</Text>
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Text category={'p1'}>Renewal Amount</Text>
                  <Text category={'p1'} appearance={'hint'}>GHS {price.toFixed(2)}</Text>
                </View>

              </View>

              {method == 'Mobile Money' ?
              <View>
                <Input accessoryLeft={phoneIcon} value={phone} label={phone_error ? phone_error : ''} status={phone_error ? 'danger' : 'basic'} onChangeText={(text)=>{setPhoneError(null); setPhone(text)}} placeholder={'MoMo Number (e.g 0500000000)'} keyboardType={'phone-pad'} autoCompleteType={'tel'} maxLength={10} shouldRasterizeIOS returnKeyType={'done'} />
              </View> : 
              <View>
                <Input accessoryLeft={cardIcon} value={card.number} label={card_error ? card_error : ''} status={card_error ? 'danger' : 'basic'} onChangeText={(text)=>{setCardError(null); setCard({...card, ...{number: text}})}} placeholder={'Card Number'} keyboardType={'number-pad'} autoCompleteType={'cc-number'} maxLength={16} shouldRasterizeIOS returnKeyType={'done'} />
                <View style={{flexDirection: 'row', marginVertical: 15}}>
                  <Input accessoryLeft={calendarIcon} label={'Expiry Month'} value={card.month} status={card_error ? 'danger' : 'basic'} style={{flex: 1, marginEnd: 15}} onChangeText={(text)=>{setCardError(null); setCard({...card, ...{month: text}})}} placeholder={'MM'} keyboardType={'number-pad'} autoCompleteType={'cc-exp-month'} maxLength={2} shouldRasterizeIOS returnKeyType={'done'} />
                  <Input accessoryLeft={calendarIcon} label={'Expiry Year'} value={card.year} status={card_error ? 'danger' : 'basic'} style={{flex: 1}} onChangeText={(text)=>{setCardError(null); setCard({...card, ...{year: text}})}} placeholder={'YY'} keyboardType={'number-pad'} autoCompleteType={'cc-exp-year'} maxLength={2} shouldRasterizeIOS returnKeyType={'done'} />
                </View>
                <Input accessoryLeft={securityIcon} value={card.cvv} status={card_error ? 'danger' : 'basic'} onChangeText={(text)=>{setCardError(null); setCard({...card, ...{cvv: text}})}} placeholder={'CVV/CSC'} keyboardType={'number-pad'} autoCompleteType={'cc-csc'} maxLength={3} shouldRasterizeIOS returnKeyType={'done'} />
                {/* <Input value={phone} label={phone_error ? phone_error : ''} status={phone_error ? 'danger' : 'basic'} onChangeText={(text)=>{setPhoneError(null); setPhone(text)}} placeholder={'MoMo Number (e.g 0500000000)'} keyboardType={'phone-pad'} autoCompleteType={'tel'} maxLength={10} shouldRasterizeIOS returnKeyType={'done'} /> */}
              </View>
              }


              <Button disabled={loading} onPress={subscribe} appearance={'outline'} style={{textAlign: 'center', marginTop: 25}}>
                {button}
              </Button>              
              <Text category={'s2'} appearance={'hint'} style={{marginBottom: 15, marginTop: 5, alignSelf: 'center'}}>Payments processed by PayStack</Text>

              <TouchableOpacity style={{height: 40}}>
                <Text category={'s1'} style={{alignSelf: 'center'}} status={'info'}>Report a problem</Text>
              </TouchableOpacity>
          </ScrollView>
        </Layout>
      </SafeAreaView>
    </Layout>
  );
}

export default SubscriptionSummary

const style = StyleSheet.create({
  container: {
    // maxHeight: 192,
  },
});