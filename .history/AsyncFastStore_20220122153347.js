import React from 'react'
import FastStorage from "react-native-fast-storage";

// const storage = new MMKV()

export const getItem = async (key)=>{
    const val = await FastStorage.getItem(key)
    return ((val != undefined) ? val: null)
}

export const setItem = async (key, value)=>{
    console.log(key+' '+value)
    await FastStorage.setItem((key).toString(), (value).toString())
    // storage.set((key).toString(), (value).toString())
    return true;
}

export const getAllKeys = async ()=>{
    // storage.set('test', 'tt1')
    // storage.set('tes2', 'tt1')
    // storage.set('tes3', 'tt1')
    // return (storage.getAllKeys()).toString()
    return '';
}

export const clear = async ()=>{
    // storage.clearAll()
    await FastStorage.clearStore()
    return true
}

export const removeItem = async (key)=>{
    // storage.delete(key)
    FastStorage.removeItem(key)
    return true
}
