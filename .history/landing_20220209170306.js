import React, {useEffect, useState} from 'react';
import { Dimensions, StyleSheet, View, Image } from 'react-native';
import { Layout, Text, ViewPager, Button } from '@ui-kitten/components';
import { default as theme } from './theme.json';

const Landing = ({navigation}) => {
    
    const [index, setIndex] = useState(0)

    useEffect(()=>{
        if(index == 3){
            setTimeout(() => {
                setIndex(3)
            }, 400);
        }
    }, [index])

    return (
        <Layout style={{ flex: 1}}>
            <ViewPager 
                style={{flex: 1}}
                selectedIndex={index}
                onSelect={setIndex}>

                <Layout
                    style={styles.tab}
                    level='1'>
                        <Image source={require('./assets/images/landing_1.jpg')} style={{width: 300, height: 300}} />
                </Layout>
                
                <Layout
                    style={styles.tab}
                    level='1'>
                        <Image source={require('./assets/images/landing_2.jpg')} style={{width: 300, height: 300}} />
                </Layout>

                <Layout
                    style={styles.tab}
                    level='1'>
                        <Image source={require('./assets/images/landing_3.jpg')} style={{width: 300, height: 300}} />
                </Layout>

                <Layout
                    style={styles.tab}
                    level='1'>
                        <Image source={require('./assets/images/landing_4.jpg')} style={{width: 300, height: 300}} />
                </Layout>
            </ViewPager>

            <View style={{ backgroundColor: '#5AB9AF', borderTopRightRadius: 25, borderTopLeftRadius: 25 }}>

                <Text category='h5' style={{marginHorizontal: 25, marginTop: 25, textAlign: 'center'}} status={'control'}>
                    {
                        index == 0 ?
                            'Find Trusted Doctor'
                        : index == 1 ?
                            'Mobile Laboratory'
                        : index == 2 ?
                            'Mobile Pharmacy'
                        : 'Health Diary'
                    }
                </Text>
                <Text category='s1' style={{marginHorizontal: 50, marginTop: 15, textAlign: 'center'}} status={'control'}>
                    {
                        index == 0 ?
                            'Social Distance is no hinderance to seeing the doctor.'
                        : index == 1 ?
                            'Get your lab results analyzed and shared with you from ClaronDoc.'
                        : index == 2 ?
                            'Save trips to the pharmacy and order your drugs from the app.'
                        : 'Keep your health in check with reminders and a personal medical diary.'
                    }
                </Text>

                <View style={{flexDirection: 'row', justifyContent: 'center', margin: 15, marginVertical: 35}}>
                    <View style={{height: 10, width: index == 0 ? 30 : 10, borderRadius: 10, backgroundColor: '#F2F2F2', marginEnd: 10}}/>
                    <View style={{height: 10, width: index == 1 ? 30 : 10, borderRadius: 10, backgroundColor: '#F2F2F2', marginEnd: 10}}/>
                    <View style={{height: 10, width: index == 2 ? 30 : 10, borderRadius: 10, backgroundColor: '#F2F2F2', marginEnd: 10}}/>
                    <View style={{height: 10, width: index == 3 ? 30 : 10, borderRadius: 10, backgroundColor: '#F2F2F2', marginEnd: 10}}/>
                </View>

                <Button onPress={()=>{index < 3 ? setIndex(index+1) : navigation.replace('Login')}} size={'large'} status={'control'} appearance={'outline'} style={{marginHorizontal: 15, marginBottom: 15}}>
                    { index < 3 ? 'Continue' : 'Get Started' }
                </Button>

            </View>
        </Layout>
    )
}

export default Landing

const styles = StyleSheet.create({
  viewPager: {
    flex: 1,
  },
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  active: {
    margin: 15,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#F2F2F2',
    padding: 25,
    width: Dimensions.get('window').width-60,
    height: Dimensions.get('window').height-200
  }
});