// console.log('app start -1')
import 'react-native-gesture-handler';
import React, { useState, useEffect, useRef } from 'react';
import {AppRegistry, Alert} from 'react-native';
import {name as appName} from './app.json';
import App from './App';
import RNCallKeep from 'react-native-callkeep';
import messaging from '@react-native-firebase/messaging';
import uuid from 'react-native-uuid';
import AsyncStorage from './AsyncStorageCustom'
import {createNavigationContainerRef} from '@react-navigation/native'

console.log('app start 0');

const navigation = createNavigationContainerRef();

// Register background handler
messaging().setBackgroundMessageHandler(async message => {
    console.log('Message handled in the background!', message);
    if(message.data.hasOwnProperty('call')){
        AsyncStorage.setItem('call_d',JSON.stringify(message.data.call));

        try{
            console.log('notification: ', message.data.call)
            call_id = uuid.v4();
            let calls = await RNCallKeep.getCalls()
            if(calls.length == 0){
                RNCallKeep.addEventListener('answerCall', ({ callUUID }) => {
                    // Do your normal `Answering` actions here.
                    RNCallKeep.endAllCalls();
                    navigation.navigate('Video', {urgent: message.data.call});
                });

                RNCallKeep.displayIncomingCall(call_id, 'Urgent Care', localizedCallerName = message.data.call.name, handleType = 'generic')
            }
            return;
        }catch(e){
            console.log('display incoming call error: ', e)
        }
    }
});

console.log('app start 1');

const HeadlessCheck = ({isHeadless}) =>{
    if(isHeadless){
        // return null
    }

    return <App />;
}

AppRegistry.registerComponent(appName, () => HeadlessCheck);


// {
//     "to": "fXP0HvwZ8UqbvfjiP73wYO:APA91bHjgkBwfvsbHYh9QDX60Gra6H0svRi_Y9A84KMDtZd7aDwCD6bTzR7ujTjw_syRI54-ajWsoGycPTJ45XLa3HF2CtBDJ6BFfKpp8LEgjHkmey8HqDeJqY5n5zJa-9blxKhAE-i9",
//     "notification": {
//         "title":"Test",
//         "body":"Test",
//         "icon": "https://imageuploads01.s3.amazonaws.com/1607082935102-icon_inv.jpg",
//         "sound": "call"
//     },
//     "data": {
//         "name": "hellworld"
//     }
// }