import 'react-native-gesture-handler';
import * as React from 'react';
import { ActivityIndicator} from 'react-native-paper';
import { KeyboardAvoidingView, Platform, Alert, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from './AsyncStorageCustom'
// const AsyncStorage = AsyncStorag.default
console.log(AsyncStorage)
import {requestMultiple, PERMISSIONS} from 'react-native-permissions';

import { PreferencesContext } from './context';
import Login from './screens/login';
import Register from './screens/register';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import * as eva from '@eva-design/eva';
import { default as claron } from './theme.json';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { UserMain } from './screens/main';
import DoctorProfile from './screens/doctor_profile';
import Conversation from './screens/conversation';
import Doctors from './screens/doctors';
import Pharmacy from './screens/pharmacy';
import Account from './screens/account';
import Appointments from './screens/appointments';
import Subscription from './screens/subscription';
import Settings from './screens/settings';
import Call from './screens/call';
import Emergency from './screens/homecare';
import HomeCare from './screens/homecare';
import Ambulance from './screens/ambulance';
import Landing from './landing';
import firebase from 'firebase'
import Availability from './screens/availability';
import SubscriptionSummary from './screens/subscription_summary';
import PaymentResponse from './screens/PaymentResponse';
import Laboratory from './screens/lab';
import SavedDoctors from './screens/saved_doctors';
import NotificationSettings from './screens/notification_settings';
import UrgentCare2 from './screens/urgent';
import UrgentCare from './screens/urgent_selector';
import Notifications from './screens/notifications';
import VideoCall from './screens/video';
import History from './screens/history';
import Payment from './screens/payment';
import Web from './screens/web';
import OnDemand from './screens/ondemand';
import Cart from './screens/cart';
import Password from './screens/change_password';
import Search from './screens/search';
import VoipPushNotification from 'react-native-voip-push-notification';
import messaging from '@react-native-firebase/messaging';
import CodePush from 'react-native-code-push'
import * as API from './api/noti'

let codePushOptions = { checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,
  installMode: CodePush.InstallMode.IMMEDIATE,
  mandatoryInstallMode: CodePush.InstallMode.IMMEDIATE,
  updateDialog: {
    appendReleaseDescription: true,
    title: "a new update is available!"
  }
};

// (async()=>{
//   console.log(await AsyncStorage.getAllKeys() )
// })()

const { Navigator, Screen } = createStackNavigator();
const moment = require('moment'); 

firebase.apps.length == 0 ? firebase.initializeApp({
  apiKey: "AIzaSyA07_A7At-J9Mu6NMXBpoLVYcrKWR3ezy4",
  authDomain: "fcm-notify-db9b8.firebaseapp.com",
  databaseURL: "https://fcm-notify-db9b8.firebaseio.com",
  projectId: "fcm-notify-db9b8",
  storageBucket: "fcm-notify-db9b8.appspot.com",
  messagingSenderId: "77071010064",
  appId: "1:77071010064:web:e693b1fa22167a00e27d95",
  measurementId: "G-VWCS7XBQC3"
}) : null;

function App() {

  const [valid, setValid] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [loadingText, setLoadingText] = React.useState('loading');
  const [user, setUser] = React.useState();
  const [theme, setTheme] = React.useState('light');

  const toggleTheme = () => {
    const nextTheme = theme === 'light' ? 'dark' : 'light';
    setTheme(nextTheme);
  };

  const registerForPushNotificationsAsync = async () => {
    let token = await messaging().getToken()
    console.log('token: ', token)
    // return 
    await API.updateFCMToken(token)
    await messaging().requestPermission();
    await messaging().registerDeviceForRemoteMessages()

    try{
      messaging().onTokenRefresh(token=>{
        console.log(token)
      })

      let initial = await messaging().getInitialNotification()
      if(initial != null){
        console.log('initial notification: ', initial.data)
      }else{
        console.log('no initial notification')
      }

    }catch(e){
      console.log('error registering fcm token: ', e)
    }

    if(Platform.OS == 'ios'){

      try{
        VoipPushNotification.registerVoipToken()
        
        VoipPushNotification.addEventListener('register', (token) => {
          // --- send token to your apn provider server
          console.log('VOIP TOKEN: ', token)
        });
        
        VoipPushNotification.addEventListener('notification', (notification) => {
          // --- optionally, if you `addCompletionHandler` from the native side, once you have done the js jobs to initiate a call, call `completion()`
          VoipPushNotification.onVoipNotificationCompleted(notification.uuid);
        });

        // ===== Step 3: subscribe `didLoadWithEvents` event =====
        VoipPushNotification.addEventListener('didLoadWithEvents', (events) => {
          // --- this will fire when there are events occured before js bridge initialized
          // --- use this event to execute your event handler manually by event type

          if (!events || !Array.isArray(events) || events.length < 1) {
              return;
          }
          for (let voipPushEvent of events) {
              let { name, data } = voipPushEvent;
              console.log('VOIP EVENTS: ', voipPushEvent)
              if (name === VoipPushNotification.RNVoipPushRemoteNotificationsRegisteredEvent) {
                  console.log('Voip Registration: ', data)
              } else if (name === VoipPushNotification.RNVoipPushRemoteNotificationReceivedEvent) {
                  console.log('Voip Event: ', data);
              }
          }
        });

        // ===== Step 4: register =====
        // --- it will be no-op if you have subscribed before (like in native side)
        // --- but will fire `register` event if we have latest cahced voip token ( it may be empty if no token at all )
        // VoipPushNotification.registerVoipToken(); // --- register token
      }catch(e){
        console.log('Error initializing VOIP Notification', e)
      }
    }
  };

  React.useEffect(()=>{
    (async()=>{
      if(Platform.OS == 'ios'){
        if(true){
          requestMultiple([PERMISSIONS.IOS.CAMERA, PERMISSIONS.IOS.MICROPHONE]).then((statuses) => {
            console.log('Camera', statuses[PERMISSIONS.IOS.CAMERA]);
            console.log('FaceID', statuses[PERMISSIONS.IOS.MICROPHONE]);
          });
        }
      }
    })()
  }, [])

  React.useEffect(()=>{

    console.log('app start 2');

    (async()=>{
      let expiry = await AsyncStorage.getItem('login-expiry')
      // console.log(expiry)
      // return
      if( !(moment().isBefore(new Date(expiry))) ){
        setValid(moment().isBefore(new Date(expiry)))
        setLoading(false);
        await registerForPushNotificationsAsync();
        return ;
      }
      setLoadingText('Encrypting to server, this might take a while but it\'s for your security')
      let u = await AsyncStorage.getItem('user')
      setLoadingText('Finalizing')
      setUser(JSON.parse(u))
      setValid(moment().isBefore(new Date(expiry)))
      setLoading(false);
      await registerForPushNotificationsAsync();
    })()

  }, [])

  const HomeNavigator = ({user, valid}) => (
    <PreferencesContext.Provider value={{ theme, toggleTheme }}>
      <ApplicationProvider {...eva} theme={{ ...eva[theme], ...claron }}>      
      <NavigationContainer>
        { loading ?
          <SafeAreaView style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size={'small'} color={'#5AB9AF'}/>
            <Text style={{textAlign: 'center', width: '70%'}}>{loadingText}</Text>
          </SafeAreaView>
          :

          <SafeAreaProvider>
          {/* <SafeAreaView style={{flex: 1}}> */}
            <KeyboardAvoidingView style={{flex: 1}} behavior={Platform.OS == 'ios' ? 'padding' : null}>
              <Navigator initialRouteName={user && valid ? 'Main' : 'Landing'}>
                <Screen name="Login" component={Login} options={{ headerShown: false }} />
                <Screen name="Register" component={Register} options={{ headerShown: false }} />
                <Screen name="Landing" component={Landing} options={{ headerShown: false }} />
                <Screen name="Main" component={UserMain} options={{ headerShown: false }} />
                <Screen name="Doctor" component={DoctorProfile} options={{ headerShown: false }} />
                <Screen name="Conversation" component={Conversation} options={{ headerShown: false }} />
                <Screen name="Doctors" component={Doctors} options={{ headerShown: false }} />
                <Screen name="Search" component={Search} options={{ headerShown: false }} />
                <Screen name="Pharmacy" component={Pharmacy} options={{ headerShown: false }} />
                <Screen name="Laboratory" component={Laboratory} options={{ headerShown: false }} />
                <Screen name="Account" component={Account} options={{ headerShown: false }} />
                <Screen name="Appointments" component={Appointments} options={{ headerShown: false }} />
                <Screen name="Subscription" component={Subscription} options={{ headerShown: false }} />
                <Screen name="Saved Doctors" component={SavedDoctors} options={{ headerShown: false }} />
                <Screen name="Settings" component={Settings} options={{ headerShown: false }} />
                <Screen name="Call" component={Call} options={{ headerShown: false }} />
                <Screen name="Video" component={VideoCall} options={{ headerShown: false }} />
                <Screen name="Urgent_Main" component={UrgentCare} options={{ headerShown: false }} />
                <Screen name="Urgent" component={UrgentCare2} options={{ headerShown: false }} />
                <Screen name="Emergency" component={Emergency} options={{ headerShown: false }} />
                <Screen name="Home Care" component={HomeCare} options={{ headerShown: false }} />
                <Screen name="Ambulance" component={Ambulance} options={{ headerShown: false }} />
                <Screen name="Availability" component={Availability} options={{ headerShown: false }} />
                <Screen name="Notifications" component={Notifications} options={{ headerShown: false }} />
                <Screen name="History" component={History} options={{ headerShown: false }} />
                <Screen name="Payment" component={Payment} options={{ headerShown: false }} />
                <Screen name="PaySub" component={SubscriptionSummary} options={{ headerShown: false }} />
                <Screen name="PaymentResult" component={PaymentResponse} options={{ headerShown: false }} />
                <Screen name="Web" component={Web} options={{ headerShown: false }} />
                <Screen name="Demand" component={OnDemand} options={{ headerShown: false }} />
                <Screen name="Cart" options={{ presentation: 'modal', headerShown: false }} component={Cart} />
                <Screen name="Password" options={{ presentation: 'modal', headerShown: false }} component={Password} />
                <Screen name="NotificationSettings" component={NotificationSettings} options={{ headerShown: false }} />
              </Navigator>
            </KeyboardAvoidingView>
          {/* </SafeAreaView> */}
          </SafeAreaProvider>
        }
      </NavigationContainer>
    </ApplicationProvider>
    </PreferencesContext.Provider>
  );

  return (
    <>
      <IconRegistry icons={EvaIconsPack}/>
      <ApplicationProvider {...eva} theme={{ ...eva[theme], ...claron }}>      
        <HomeNavigator user={user} valid={valid}/>
      </ApplicationProvider>
    </>
  );
}

export default CodePush(codePushOptions)(App);
