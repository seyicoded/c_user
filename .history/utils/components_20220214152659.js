import React, {useState, useRef} from 'react';
import { StyleSheet, View, Dimensions, Linking, Alert, ActivityIndicator } from 'react-native'
import { format, formatDistanceToNow, formatRelative, subDays } from 'date-fns'
import { Layout, Text, Card, Icon, Button } from '@ui-kitten/components';
import {Image} from 'react-native-elements'
import Clipboard from '@react-native-clipboard/clipboard';
import AsyncStorage from '../AsyncStorageCustom'

const ChatIcon = (props) => (
    <Icon {...props} name='message-circle-outline'/>
);

const VideoIcon = (props) => (
    <Icon {...props} name='video-outline'/>
);

const CallIcon = (props) => (
    <Icon {...props} name='calendar-outline'/>
);

const AddIcon = (props) => (
    <Icon {...props} name='plus-square-outline'/>
);

const RemoveIcon = (props) => (
    <Icon {...props} name='minus-square-outline'/>
);

export const MenuItem = ({option, navigate, subscribed}) => {

    return(
        <Card onPress={()=>navigate()} style={style.menu_card}>
            <Image source={option.item.icon} style={{width: 75, height: 75, marginBottom: 10, alignSelf: 'center'}} resizeMode={'contain'}/>
            <Text category={'h6'}>{option.item.name == 'Subscribe' ? subscribed ? 'Upgrade' : 'Subscribe' : option.item.name  }</Text>
        </Card>
    )
}

export const OnlineDoctor = ({doctor, navigate}) => {

    return(
        <Card style={style.doctor_card} onPress={()=>navigate()}>
            <Image PlaceholderContent={<ActivityIndicator size="large" />} source={{uri: doctor.avatar}} style={{width: 150, height: 180, marginBottom: 10, alignSelf: 'center'}} resizeMode={'cover'}/>
            <Text category={'h6'} style={{alignSelf: 'center'}}>{doctor.firstname}</Text>
            <Text category={'s1'} style={{alignSelf: 'center'}} appearance={'hint'} >{doctor.department}</Text>
        </Card>
    )
}

export const OnlineDoctor2 = ({doctor, navigate}) => {

    return(
        <Card style={style.doctor_card2} onPress={()=>navigate()}>
            <View style={{flex: 1, flexDirection: 'row',}}>
                <Image PlaceholderContent={<ActivityIndicator size="large" />} source={{uri: doctor.avatar}} style={{width: 60, height: 60, marginBottom: 10, alignSelf: 'center'}} resizeMode={'cover'}/>
                <View style={{marginLeft: 16}}>
                    <Text category={'h6'}>{doctor.firstname}</Text>
                    <Text category={'s1'} appearance={'hint'} >{doctor.department}</Text>    
                </View>
            </View>
            
        </Card>
    )
}

export const Drug = ({drug, cart, remove}) => {

    return(
        <Layout style={{flexDirection: 'column', width: (width/2)-20, margin: 10, borderRadius: 5, borderColor: '#f2f2f2', borderWidth: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Image source={{uri: drug.avatar}} style={{width: (width/2)-20, height: (width/2)-50, marginBottom: 10, alignSelf: 'center'}} resizeMode={'cover'}/>
            <Text category={'s1'} style={{alignSelf: 'center'}} appearance={'hint'}>{drug.name}</Text>
            <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 10}}>
                <Button onPress={()=>remove(drug)} style={{width: 50, alignSelf: 'flex-end'}} appearance='ghost' status='danger' accessoryLeft={RemoveIcon}/>
                <Text category={'h6'} style={{alignSelf: 'center'}} >GHS {drug.unitprice}</Text>
                <Button onPress={()=>cart(drug)} style={{width: 50, alignSelf: 'flex-end'}} appearance='ghost' status='primary' accessoryLeft={AddIcon}/>
            </View>
        </Layout>
    )
}

export const Doctor = ({doctor, navigate, nav, subscribed, alt}) => {
    // console.log(subscribed)
    console.log(`doctor ${doctor.firstname} has profile image: ${doctor.avatar}`)
    return(
        <Card style={{marginHorizontal: 15, marginVertical: 10}} onPress={()=>navigate()}>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>

                <View style={{width: 75, height: 75, marginBottom: 15}}>
                    <Image source={{uri: doctor.avatar}} resizeMode={'cover'} style={{width: 75, height: 75, borderRadius: 75}}/>
                    { doctor.availability == 'Online' ? <View style={{marginStart: 5, height: 15, width: 15, borderRadius: 15, backgroundColor: 'green', bottom: 18.75, left: 56.25}}/> : null }
                </View>

                <View style={{marginStart: 15}}>
                    <Text category={'h6'} adjustsFontSizeToFit lineBreakMode='tail'>{doctor.firstname} {doctor.lastname}</Text>
                    <Text category={'s1'} appearance={'hint'} >{doctor.department}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                        <Button onPress={()=>{
                                (async()=>{
                                    const day = await AsyncStorage.getItem('subscription_exp_day');
                                    const sub_type = await AsyncStorage.getItem('subscription');
                                    if( (sub_type == 'Normal' || (sub_type == 'Basic' && day < 14))){
                                        Alert.alert('Must be on a Subscription to access such features');
                                        nav.navigate('Subscription', {doctors: doctor});
                                        return false;
                                    }
                                    nav.navigate('Availability', {doctor})
                                })()

                                
                            }} style={{width: 50, alignSelf: 'flex-end', marginEnd: 15}} appearance='outline' status='primary' accessoryLeft={CallIcon}/>
                        {/* <Button onPress={()=>nav.navigate('Availability', {doctor})} style={{width: 50, alignSelf: 'flex-end', marginEnd: 15}} appearance='outline' status='primary' accessoryLeft={VideoIcon}/> */}
                        <Button onPress={()=>{ 
                            (async()=>{
                                const day = await AsyncStorage.getItem('subscription_exp_day');
                                const sub_type = await AsyncStorage.getItem('subscription');
                                if( (sub_type == 'Normal' || (sub_type == 'Basic' && day < 14))){
                                    Alert.alert('Must be on a Subscription to access such features');
                                    nav.navigate('Subscription', {doctors: doctor});
                                    return false;
                                }
                                nav.navigate('Conversation', {to: doctor.email, name: doctor.firstname+' '+doctor.lastname, doctor})
                            })()

                            
                            }} style={{width: 50, alignSelf: 'flex-end'}} appearance='outline' status='primary' accessoryLeft={ChatIcon}/>
                    </View>
                </View>
            </View>
        </Card>
    )
}

export const ChatBubble = ({chat, me}) => {
    const [failedLoad, setfailedLoad] = useState(false);
    // console.log(new Date(chat.createDate))
    // console.log((new Date((new Date()).toString()).toString()))
    // console.log(me)
    // (async()=>{
    //     me = await AsyncStorage.getItem('email');
    // })()
    
    // console.log(b)
    if(chat.attachment != null){
        // console.log(chat.attachment+':');
    }

    // console.log(new Date(chat.createDate)

    // console.log('ath: '+chat.attachment)
    
    return(
        <Card onPress={()=>{
            console.log('clicked');
            try{
                if((chat.attachment == null || chat.attachment == 'undefined') || (chat.attachment).length < 3 ){
                    Alert.alert("Opening Attachment", "about opening a third party app to process attachments", [
                        {
                          text: 'Cancel',
                          onPress: () => console.log('Cancel Pressed'),
                          style: 'cancel'
                        },
                        { text: 'Only Copy Text', onPress: () => Clipboard.setString(chat.message) }
                      ]);
                }else{
                    Alert.alert("Opening Attachment", "about opening a third party app to process attachment.", [
                        {
                          text: 'Cancel',
                          onPress: () => console.log('Cancel Pressed'),
                          style: 'cancel'
                        },
                        { text: 'Proceed To Attachment', onPress: () => Linking.openURL(chat.attachment) },
                        { text: 'Only Copy Text', onPress: () => Clipboard.setString(chat.message) }
                      ]);
                }
                
            }catch(e){}


            chat.attachment != null && chat.file_type.includes('image') ?
            <View style={{flex: 1}}>
                <Image onError={()=>{console.log('e')}} PlaceholderContent={<ActivityIndicator size="large"/>} source={{uri: chat.attachment}} resizeMode={'contain'}/>
            </View>
            : null
        }} style={ chat.sender == me ? style.bubble_right : style.bubble_left}>
            {!(failedLoad) && chat.attachment != null && chat.file_type.includes('image') ? <Image source={{uri: chat.attachment}} onError={(e)=>{
                
                setfailedLoad(true)
            }} PlaceholderContent={<ActivityIndicator size="large"/>}  style={{width: width-150, height: 180, marginBottom: 10}} resizeMode={'cover'}/> : null }
            {((chat.attachment != null && chat.attachment != 'undefined') && (chat.attachment).length > 3) && !(chat.file_type.includes('image')) ? <Image source={require('../assets/imga.png')} onError={()=>{console.log('e')}} style={{width: width-150, height: 180, marginBottom: 10}} resizeMode={'cover'}/> : null }
            {(failedLoad) ? <Image source={require('../assets/failed.png')} style={{width: width-150, height: 180, marginBottom: 10}} resizeMode={'contain'}/> : null }
            <Text category={'p1'}>{chat.message}</Text>
            <Text category={'p2'} style={{ alignSelf: chat.sender == me ? 'flex-end' : 'flex-start' }} appearance={'hint'} >{formatDistanceToNow(new Date(chat.createDate), { addSuffix: true }) }</Text>
        </Card>
    )
}

const width = Dimensions.get('screen').width

const style = StyleSheet.create({
    menu_card: {
        marginVertical: 15,
        marginHorizontal: 10,
        width: 180,
        height: 180,
        alignItems: 'center',
        justifyContent: 'center'
    },
    doctor_card: {
        marginVertical: 15,
        marginHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center'
        // paddingVertical: 15
    },
    doctor_card2: {
        marginVertical: 5,
        marginHorizontal: 10,
        // paddingVertical: 15
    },
    bubble_right: {
        width: width-100,
        alignSelf: 'flex-end',
        marginHorizontal: 15,
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 0,
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        marginVertical: 10,
        backgroundColor: '#f2f2f2'
    },
    bubble_left: {
        width: width-100,
        alignSelf: 'flex-start',
        marginHorizontal: 15,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 25,
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        marginVertical: 10
    }
})