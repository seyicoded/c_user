// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '../AsyncStorageCustom'

const axios = require('axios');

export const fetchConversation = async (from, to) => {

    const key = await apiKey()
    const auth = await AsyncStorage.getItem('access-token');

    try{
        const response = await axios({
            method: 'GET',
            url: `https://api.clarondoc.com/chats/conversations/between/${from}/and/${to}?limit=10000`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${auth}`,
                'x-api-key': key
            }
        })

        if(response.data.success){
            return response.data.chats
        }else{
            return []
        }
    }catch(e){
        return []
    }
}

export const sendMessage = async (data)=>{
    try{

        let key = await apiKey()
        let token = await AsyncStorage.getItem('access-token')
        let res = await axios.post(`https://api.clarondoc.com/chats`, data, {
            headers: {
                'x-api-key': key,
                'Authorization': `Bearer ${token}`
            }
        })

        return res.data.success

    }catch(e){
        return false
    }
}

export const apiKey = async (data) => {
    let key = await AsyncStorage.getItem('api-key');

    if(key != null){
        return key
    }else{
        const response = await axios({
            method: 'POST',
            url: 'https://api.clarondoc.com/getAPIKey',
            data: {
                email: 'developer@clarondoc.com',
                password: 'Basket012Ball'
            },
            headers: {
                'Content-Type': 'application/json',
            }
        })

        key = response.data.apiKey

        await AsyncStorage.setItem('api-key', key)

        return key
    }
}