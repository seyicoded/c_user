import { DefaultTheme, DarkTheme } from '@react-navigation/native';
import { DefaultTheme as Default, DarkTheme as DarkPaper  } from 'react-native-paper';

export const Light = {
    dark: false,
    ...DefaultTheme,
    ...Default,
    colors: {
      primary: '#5AB9AF',
      background: 'rgb(242, 242, 242)',
      card: 'rgb(255, 255, 255)',
      text: 'rgb(28, 28, 30)',
      border: 'rgb(199, 199, 204)',
      notification: 'rgb(255, 69, 58)',
      error: '#FF5F47',
      warning: '#FFDC16',
      info: '#2DB0FC',
      success: '#63BC3A'
    },
};

export const Dark = {
    dark: true,
    ...DarkTheme,
    ...DarkPaper,
    colors: {
      primary: '#5AB9AF',
      background: '#485D72',
      card: '#7A83A1',
      text: '#f2f2f2',
      border: 'rgb(199, 199, 204)',
      notification: 'rgb(255, 69, 58)',
      error: '#FF5F47',
      warning: '#FFDC16',
      info: '#2DB0FC',
      success: '#63BC3A'
    },
};