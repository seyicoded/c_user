export const colors = {
    primary: "#5AB9AF"
}

export const specialists = [
    'Dental Services',
    'Fitness/Certication Services',
    'Laboratory Services',
    'Nursing/Private Doctor Services',
    'Optometry Services',
    'Vaccination Services',
    'Diabetic Clinic Services',
    'Neurologic Clinic Services',
    'Pediatric Clinic Services',
    'Obstetrics & Gynecolgy Services',
    'Surgical Services',
    'Dermatology/Geriatric Services',
    'Antenatal Clinic Services',
    'Diagnostic Services',
    'Minor Procedures Services',
    'Accommodation Service'
]