import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    button: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        marginHorizontal: 15
    },
    button_text: {
        color: 'white'
    }
})